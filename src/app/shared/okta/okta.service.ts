import { Injectable } from'@angular/core';
import OktaSignIn from'@okta/okta-signin-widget';
import { environment } from'../../../environments/environment';
@Injectable({
  providedIn:'root'
})
export class Okta {
  
    widget;
 
    constructor() {
        // this.widget = new OktaSignIn({
        //     logo: '//logo.clearbit.com/certainteed.com', 
        //     baseUrl: 'https://dev-34408392.okta.com', 
        //     clientId: '0oaavecfyuB7SI1no5d7',
        //      redirectUri: 'http://localhost:4200'
        //     redirectUri: 'https://digital-team-805wb.certainteed.com/Productcustomization'
        // });
        this.widget = new OktaSignIn({
            logo: '//logo.clearbit.com/certainteed.com',
            baseUrl: 'https://loginpreview.certainteed.com', 
            clientId: '0oa7y2776dJxBFYK70x7',
            redirectUri: 'https://digital-team-805wb.certainteed.com/Productcustomization'
          });
    }

    getWidget() {
        return this.widget;
    }

    logout(): void {
        this.widget.authClient.signOut({
        //    postLogoutRedirectUri: 'http://localhost:4200',
          postLogoutRedirectUri: 'https://digital-team-805wb.certainteed.com/Productcustomization',
        });
    
        sessionStorage.removeItem('SGID');
        sessionStorage.clear();
        localStorage.clear();
    }
}