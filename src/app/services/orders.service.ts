import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { environment } from'../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  totalOrders: number = 0;
  completeCount: number = 0;
  private OrderID!: string;

  private apiUrl = environment.Api;

  constructor(private http: HttpClient) { }

  // FOR OKTA LOGIN ON THE LANDING PAGE -> CERTAINTEED CUSTOMERS SIGIN IN
  sendOktaUserId(userId: string): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    // debugger;
    return this.http.post<any>(this.apiUrl + "/api/Login/OktaCustomerLogin/"+ userId, { headers });
  }

  setOrderId(OrderID: string) {
    this.OrderID = OrderID;
  }

  getOrderId(): string {
    return this.OrderID;
  }

  setCompleteCount(count: number) {
    this.completeCount = count;
  }

  getCompleteCount() {
    return this.completeCount;
  }

  // GET LOGS FOR ACCOUNT CHANGE HISTORY -> DOES NOT INCLUDE ORDERID
  getAccountChangeHistory(userId: number): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/Orders/GetAccountHistory/${userId}`);
  }

  // GET LOGS FOR ORDERS HISTORY -> INCLUDES ORDERID
  getOrdersHistorywithOrderID(userId: number): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/Orders/GetOrdersHistorywithOrderID/${userId}`);
  }

  // GET data for Annually Order Details -> FOR THE BAR CHART IN REPORTS PAGE
  getAnnuallyOrderDetails(userId: number): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/Orders/GetAnnuallyOrderDetails/${userId}`);
  }

  // GET data for Monthly Order Details -> FOR THE BAR CHART IN REPORTS PAGE
  getMonthlyOrderDetails(userId: number): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/Orders/GetMonthlyOrderDetails/${userId}`);
  }

  // GET data for last 60 Days Order Details -> FOR THE BAR CHART IN REPORTS PAGE
  getOrdersInLast60Days(userId: number): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/Orders/GetOrdersInLast60Days/${userId}`);
  }  

  // GET data for Product Logs
  getProductLogs(userId: number): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/Orders/GetProductLogs/${userId}`);
  }

  // GET ALL COUNTRIES DATA, GOING TO USE THIS IN DROPDOWN
  getAllCountries(): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/Orders/GetAllCountries`);
  }

  // GET ALL STATES DATA, GOING TO USE THIS IN DROPDOWN
  getAllStates(): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/Orders/GetAllStates`);
  }

  // GET SITE DETAIL BY ORDER ID ---- FOR VIEW ORDER PAGE
  getSelectedOrderSite(OrderID: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/Orders/ViewSelectedOrderSite/${OrderID}`);
  }
  
  // FOR REPEAT ORDER //
  // REPEAT ORDER START
  getOrdersInfoByOrderNumber(CTOrderNumber: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/Orders/GetOrdersInfoByOrderNumber/${CTOrderNumber}`);
  }
  getOrderNumberFromUserID(UserID: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/Orders/GetOrderNumberFromUserID/${UserID}`);
  }
  // REPEAT ORDER END //

  // GET USER DETAILS FROM USERID
  getUserInfoByUserID(UserID: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/Orders/GetUserInfoByUserID/${UserID}`);
  }
  UpdateOrderDetailsByOrderID(NewProductCode: any,ReferenceNo:any,ProxyId:any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/Orders/UpdateOrderDetailsByOrderID/${NewProductCode}/${ReferenceNo}/${ProxyId}`);
  }
  UpdateAnticipatedShipDate(OrderId: any,AnticipatedShipDate:any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/Orders/UpdateAnticipatedShipDate/${OrderId}/${AnticipatedShipDate}`);
  }
  CompleteOrder(OrderId: any,ActualShipDate:any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/Orders/CompleteOrder/${OrderId}/${ActualShipDate}`);
  }
  updateUserInfoByUserID(userData: any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.post<any>(`${this.apiUrl}/api/Orders/updateUserInfoByUserID`, userData, { headers });
  }

  getSecurityLevelByUserId(UserID: number): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/Orders/GetSecurityLevelByUserId/${UserID}`);
  }

  getCompletedOrders(StatusId: number): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/Orders/GetCompletedOrders/${StatusId}`);
  }
  
  getGetOrderDetailsByOrderID(OrderID: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/Orders/GetOrderDetailsByOrderID/${OrderID}`);
  }
  
  // in view orders page
  getFullNameAndEmailAddressByOrderID(OrderID: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/Orders/GetFullNameAndEmailAddressByOrderID/${OrderID}`);
  }

  getPMSColor(OrderID: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/Orders/GetPMSColor/${OrderID}`);
  }
 
  // GET ALL ORDER DETAILS BY ORDER ID ---- FOR VIEW ORDER PAGE
  getOrderDetails(OrderID: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/Orders/GetOrderDetails/${OrderID}`);
  } 

  getCurrentOrderStatus(OrderID: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/Orders/currentOrderStatus/${OrderID}`);
  }

  getAllSites(UserID:any): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/Orders/GetSites/${UserID}`);
  }
  getAllUsers(): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/Orders/GetUsers`);
  }
  GetUserswithId(UserId:any): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/Orders/GetUserswithId?UserID=`+UserId);
  }
  getAllSecurityLevels(): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/Orders/GetAllSecurityLevels`); 
  }

  getCustomerEmails(SiteId:any): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/Orders/GetCustomerEmails?SiteId=`+SiteId);
  }

  getQuickActionOrders(SiteName:any): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/Orders/GetQuickActionOrders?SiteName=`+SiteName);
  }

  getQuickActionEndUserOrders(UserID:any): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/Orders/GetQuickActionEndUserOrders?UserId=`+UserID);
  }

  getAllOrders(SecurityLevel:any,UserId:any,Country:any,SiteName:any): Observable<any> {
    // debugger;
    return this.http.get(`${this.apiUrl}/api/Orders/GetOrders/${SecurityLevel}/${UserId}/${Country}/${SiteName}`);
  }

  filterOrdersByStatus(): Observable<any[]> {
    return this.http.get<any[]>('api/allOrders').pipe(
      map((orders: any[]) => orders.filter(order => order.StatusId === 5))
    );
  }

  getAllOrderStatus(): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/Orders/GetOrderStatus`);
  }

  // Method to check if the CT Order Number is available
  checkCTOrderNumberAvailability(CTOrderNumber: string): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/Orders/CheckCTOrderNumber?orderNumber=${CTOrderNumber}`);
  }
  
  // CREATE NEW ORDER SECTION
  createNewOrder(orderData: any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.post<any>(`${this.apiUrl}/api/Orders/CreateOrder`, orderData, { headers });
  }


  updateOrder(orderData: any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    // Include the OrderID along with the order data

    return this.http.post<any>(`${this.apiUrl}/api/Orders/UpdateOrder`, orderData, { headers });
  }
  CheckUser(userData: any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.post<any>(`${this.apiUrl}/api/Orders/CheckUser`, userData, { headers });
  }

  // CREATE NEW USER SECTION
  createUser(userData: any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.post<any>(`${this.apiUrl}/api/Orders/CreateUser`, userData, { headers });
  }

  UpdateUser(userData: any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.post<any>(`${this.apiUrl}/api/Orders/UpdateUser`, userData, { headers });
  }

  // PRODUCT DETAILS SECTION
  createProductDetails(productDetailsData: any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    debugger;
    return this.http.post<any>(`${this.apiUrl}/api/Orders/InserProductDetails`, productDetailsData, { headers });
  }
  
  // UPDATE PRODUCT DETAILS SECTION
  updateProductDetails(productDetailsData: any[]): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    debugger;

    return this.http.post<any>(`${this.apiUrl}/api/Orders/UpdateProductDetails`, productDetailsData, { headers });
  }


  // ASSIGN CUSTOMERS SECTION
  assignCustomers(customersData: any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    debugger;
    return this.http.post<any>(`${this.apiUrl}/api/Orders/InsertUpdateCustomerEmails`, customersData, { headers });
  }

  updateOrderStatus(orderID: string, statusID: number): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    
    return this.http.post<any>(`${this.apiUrl}/api/Orders/UpdateOrderStatus`, { orderID, statusID }, { headers });
  }

  deleteOrder(orderId: any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.delete<any>(`${this.apiUrl}/api/Orders/DeleteOrder/${orderId}`, { headers });
  }

  // GET ALL ORDER DETAILS BY ORDER ID ---- FOR VIEW ORDER PAGE
  getAllLogsDetails(): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/Orders/GetAllLogsDetails`);
  } 

  // CREATE NEW ORDER LOG
  insertLog(logData: any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.post<any>(`${this.apiUrl}/api/Orders/InsertLog`, logData, { headers });
  }

  getNewOrderLog(OrderID: any): Observable<any> {
    return this.http.get<any>(`${this.apiUrl}/api/Orders/GetNewOrderLog/${OrderID}`);
  }
  
}
