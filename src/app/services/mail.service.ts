import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from'../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MailService {

  private apiUrl = environment.Api;

  constructor(private http: HttpClient) { }

  NewOrderMail(Template:any,Servername:any,ExtraUrl:any,FullName:any,OrderNo:any,Site:any,ToEmail:any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    debugger;
    return this.http.post<any>(this.apiUrl+"/api/Orders/SendEmail/"+Template+"/"+Servername+"/"+ExtraUrl+"/"+FullName+"/"+OrderNo+"/"+Site+"/"+ToEmail , { headers });
  }

  RepeatOrder(Template:any,Servername:any,ExtraUrl:any,FullName:any,OrderNo:any,Site:any,ToEmail:any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    debugger;
    return this.http.post<any>(this.apiUrl+"/api/Orders/RepeatOrder/"+Template+"/"+Servername+"/"+ExtraUrl+"/"+FullName+"/"+OrderNo+"/"+Site+"/"+ToEmail , { headers });
  }


  SendUploadArtworkEmail(Template:any,Servername:any,ExtraUrl:any,OrderNo:any,Site:any,varExistingFileID:any,PMSCOLORS:any, ArtworkID: any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
   debugger;
    return this.http.post<any>(this.apiUrl+"/api/Orders/SendUploadArtworkEmail/"+Template+"/"+Servername+"/"+ExtraUrl+"/"+OrderNo+"/"+Site+"/"+varExistingFileID+"/"+PMSCOLORS+"/"+ArtworkID , { headers });
  }

  SendUploadProofEmail(Template:any,Servername:any,ExtraUrl:any,OrderNo:any,Site:any,varUserId:any,Fullname:any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
   
    return this.http.post<any>(this.apiUrl+"/api/Orders/SendUploadProofEmail/"+Template+"/"+Servername+"/"+ExtraUrl+"/"+OrderNo+"/"+Site+"/"+varUserId+"/"+Fullname , { headers });
 
 
  }
  
  SendArtworkRejectedByVendorEmail(Template:any,Servername:any,ExtraUrl:any,OrderNo:any,Site:any,varUserId:any,Fullname:any, varExistingFileID:any, REJECTREASON:any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
   debugger;
    return this.http.post<any>(this.apiUrl+"/api/Orders/SendArtworkRejectedByVendorEmail/"+Template+"/"+Servername+"/"+ExtraUrl+"/"+OrderNo+"/"+Site+"/"+varUserId+"/"+Fullname +"/"+varExistingFileID+"/"+REJECTREASON , { headers });
 
 
  }

  SendProofApprovedContinueOrderEmail(Template:any,Servername:any,ExtraUrl:any,OrderNo:any,Site:any, varExistingFileID:any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
   debugger;
    return this.http.post<any>(this.apiUrl+"/api/Orders/SendProofApprovedContinueOrderEmail/"+Template+"/"+Servername+"/"+ExtraUrl+"/"+OrderNo+"/"+Site +"/"+varExistingFileID , { headers });
 
 
  }

  SendProofRejectedByCustomerEmail(Template:any,Servername:any,ExtraUrl:any,OrderNo:any,Site:any, varExistingFileID:any, REJECTREASON:any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
   
    return this.http.post<any>(this.apiUrl+"/api/Orders/SendProofRejectedByCustomerEmail/"+Template+"/"+Servername+"/"+ExtraUrl+"/"+OrderNo+"/"+Site +"/"+varExistingFileID+"/"+REJECTREASON , { headers });
 
 
  }

  SendUpdateOrderStatusEmail(Template:any,Servername:any,ExtraUrl:any,OrderNo:any,Site:any, MESSAGE:any,): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
   
    return this.http.post<any>(this.apiUrl+"/api/Orders/SendUpdateOrderStatusEmail/"+Template+"/"+Servername+"/"+ExtraUrl+"/"+OrderNo+"/"+Site +"/"+MESSAGE , { headers });
 
 
  }

}
