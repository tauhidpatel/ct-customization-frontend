import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { environment } from'../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class SsoService {

  //  private ssoUrl = 'https://localhost:7049/api/sso1';
  private ssoUrl = 'https://digital-team-805wb.certainteed.com/ProductCTAPI/api/sso1';
  
  constructor(private http: HttpClient) { }

  navigateToSsoPage(): void {
    // window.location.href = 'https://uat.websso.saint-gobain.com/cas/login' +
    //   '?service=' + encodeURIComponent('http://localhost:54100/api/sso1');
      // window.location.href = "https://uat.websso.saint-gobain.com/cas/login"+ "?service=" + "http://localhost:54100/" +"api/sso1/" ;
      // window.location.href = "https://uat.websso.saint-gobain.com/cas/login"+ "?service=" + "https://localhost:7049" +"/api/Login/api/sso1/" ;
      window.location.href = "https://uat.websso.saint-gobain.com/cas/login"+ "?service=" + environment.Api +"/api/Login/api/sso1/" ;
  }

  handleSsoCallback(): Observable<string> {
    // Make a request to your backend to retrieve SGID
    return this.http.get<string>(this.ssoUrl);
  }

  storeSgidInSession(sgid: string): void {
    sessionStorage.setItem('sgid', sgid);
      //  window.location.href = "https://uat.websso.saint-gobain.com/cas/login"+ "?service=" + "https://localhost:7049" +"/api/Login/api/sso1/" ;
      window.location.href = "https://uat.websso.saint-gobain.com/cas/login"+ "?service=" + "https://digital-team-805wb.certainteed.com/ProductCTAPI" +"/api/Login/api/sso1/" ;
  }
  
}
