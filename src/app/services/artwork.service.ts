import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { environment } from'../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ArtworkService {

  private apiUrl = environment.Api;

  constructor(private http: HttpClient) { }

   // UPLOAD ARTWORK
  uploadArtwork(artworkData: any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.post<any>(`${this.apiUrl}/api/Orders/InsertArtwork`, artworkData, { headers });
  }

  updateOrderWithArtworkID(orderID: string, artworkID: string): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const body = { orderID, artworkID };
    debugger;


    return this.http.post<any>(`${this.apiUrl}/api/Orders/UpdateOrderWithArtworkID`, body, { headers });
  } 

  uploadArtworkFile(orderId: string, userId: string, pmsColors: string, file: File) {
    const formData: FormData = new FormData();
    formData.append('uploadNewFile', file);
    formData.append('orderId', orderId);
    formData.append('userId', userId);
    formData.append('pmsColors', pmsColors);
  
    return this.http.post<any>(`${this.apiUrl}/api/Orders/InsertArtworkFile`, formData);
  }

  getArtworkToDownload(orderId: string): Observable<any>  {
    return this.http.get<any>(`${this.apiUrl}/api/Orders/GetArtworkToDownload?OrderID=${orderId}}`);
  }

  // downloadArtworkFile(orderId: string, fileName: string): Observable<any> {
  //   return this.http.get<any>(`${this.apiUrl}/api/Orders/DownloadArtworkFile/${orderId}/${fileName}`);
  // }  

  // downloadArtworkFile(orderId: string, fileName: string): Observable<Blob> {
  //   return this.http.get(`${this.apiUrl}/api/Orders/DownloadArtworkFile/${orderId}/${fileName}`, {
  //     responseType: 'blob'
  //   });
  // }

  downloadArtworkFile(orderId: string, fileName: string): Observable<Blob> {
    const encodedFileName = encodeURIComponent(fileName);
    const modifiedFileName = `${orderId}/${encodedFileName}`;
;
    
    // return this.http.get(`${this.apiUrl}/api/Orders/DownloadArtworkFile/${modifiedFileName}`, {
    //   responseType: 'blob'
    // });

    return this.http.get(`${this.apiUrl}/api/Orders/DownloadArtworkFile/${modifiedFileName}`, {
      responseType: 'blob',
      observe: 'response' // Ensure full response is captured
    }).pipe(
      map((response: any) => new Blob([response.body], { type: 'application/octet-stream' }))
    );
  }
  

  ArtworkReject(OrderId:any,UserID:any,IPAddress:any,varProofID:any,RejectReason:any): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/Orders/ArtworkReject/${OrderId}/${UserID}/${IPAddress}/${varProofID}/${RejectReason}`);
  }


  

}