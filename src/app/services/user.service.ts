import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private apiUrl = environment.Api;

  constructor(private http: HttpClient) { }

  createuser(FirstName:any,LastName:any,Email:any,groupids:any,mobilePhone:any,organization:any): Observable<any> {

    const formData: FormData = new FormData();
    formData.append('FirstName', FirstName);
    formData.append('LastName', LastName);
    formData.append('Email', Email);
    formData.append('groupids', groupids);
    formData.append('mobilePhone', mobilePhone);
    formData.append('organization', organization);
   
    return this.http.post(`${this.apiUrl}/api/User/createuser`,formData);
  }
  UpdateUser(userid:any,FirstName:any,LastName:any,Email:any,organization:any): Observable<any> {

    const formData: FormData = new FormData();
    formData.append('userid', userid);
    formData.append('FirstName', FirstName);
    formData.append('LastName', LastName);
    formData.append('Email', Email);
    formData.append('organization', organization);
   
    return this.http.post(`${this.apiUrl}/api/User/UpdateUser`,formData);
  }

  Send(FirstName:any,LastName:any,Email:any,Token:any,User:any,WebsiteURL:any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.post(this.apiUrl+"/api/User/Send/"+FirstName+"/"+LastName+"/"+Email+"/"+Token+"/"+User+"/"+WebsiteURL,headers);
  }

  ForgotPasswordSend(FirstName:any,LastName:any,Email:any,User:any,WebsiteURL:any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.post(this.apiUrl+"/api/User/ForgotPasswordSend/"+FirstName+"/"+LastName+"/"+Email+"/"+User+"/"+WebsiteURL,headers);
  }
  GetActivationToken(userId:any): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/User/GetActivationToken/${userId}`);
  }

  CheckUserEmail(Email:any): Observable<any> { 
    return this.http.get(`${this.apiUrl}/api/User/CheckUserEmail/${Email}`);
  }

  Activateuser(Userid:any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
   
    return this.http.post(this.apiUrl+"/api/User/Activateuser/"+Userid,headers);
  }
  AddUserGroup(groupid:any,userid:any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post(this.apiUrl+"/api/User/AddUserGroup/"+groupid+"/"+userid,headers);
  }
  SetPassword(User:any,Password:any): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/User/SetPassword/${User}/${Password}`);
  }
  VerifyToken(Token:any): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/User/VerifyToken/${Token}`);
  }
}
