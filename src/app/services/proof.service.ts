import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { environment } from'../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProofService {

  private apiUrl = environment.Api;

  constructor(private http: HttpClient) { }

  uploadProof(proofData: any): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.post<any>(`${this.apiUrl}/api/Orders/InsertProof`, proofData, { headers });
  }
  ProofApprove(OrderId:any,UserID:any,IPAddress:any,varProofID:any): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/Orders/ProofApprove/${OrderId}/${UserID}/${IPAddress}/${varProofID}`);
  }
  ProofReject(OrderId:any,UserID:any,IPAddress:any,varProofID:any,RejectReason:any): Observable<any> {
    return this.http.get(`${this.apiUrl}/api/Orders/ProofReject/${OrderId}/${UserID}/${IPAddress}/${varProofID}/${RejectReason}`);
  }

  uploadProofFile(orderId: string, userId: string, file: File) {
    const formData: FormData = new FormData();
    formData.append('uploadNewFile', file);
    formData.append('orderId', orderId);
    formData.append('userId', userId);
  
    return this.http.post<any>(`${this.apiUrl}/api/Orders/InsertProofFile`, formData);
  }

  getProofToDownload(orderId: string): Observable<any>  {
    return this.http.get<any>(`${this.apiUrl}/api/Orders/GetProofToDownload?OrderID=${orderId}}`);
  }

  downloadProofFile(orderId: string, fileName: string): Observable<Blob> {
    const encodedFileName = encodeURIComponent(fileName);
    const modifiedFileName = `${orderId}/${encodedFileName}`;


    return this.http.get(`${this.apiUrl}/api/Orders/DownloadProofFile/${modifiedFileName}`, {
      responseType: 'blob',
      observe: 'response' // Ensure full response is captured
    }).pipe(
      map((response: any) => new Blob([response.body], { type: 'application/octet-stream' }))
    );

  }


}
