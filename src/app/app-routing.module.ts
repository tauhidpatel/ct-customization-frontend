import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { EmailverifyComponent } from './components/emailverify/emailverify.component';
import { VerifyotpComponent } from './components/verifyotp/verifyotp.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { LandingComponent } from './components/landing/landing.component';
import { SsoComponent } from './components/sso/sso.component';
import { AuthorizeComponent } from './components/authorize/authorize.component';
import { UserprofileComponent } from './components/userprofile/userprofile.component';
import { CreateNewOrderComponent } from './components/create-new-order/create-new-order.component';
import { RepeatOrderComponent } from './components/repeat-order/repeat-order.component';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { RoleAccessComponent } from './components/role-access/role-access.component';
import { ViewOrderComponent } from './components/view-order/view-order.component';
import { ReportsComponent } from './components/reports/reports.component';
import { AllOrdersComponent } from './components/all-orders/all-orders.component';
import { UploadProofComponent } from './components/upload-proof/upload-proof.component';
import { ReviewProofComponent } from './components/review-proof/review-proof.component';
import { UploadArtworkComponent } from './components/upload-artwork/upload-artwork.component';
import { ResetOrderStatusComponent } from './components/reset-order-status/reset-order-status.component';
import { UsersComponent } from './components/users/users.component';
import { EditOrderComponent } from './components/edit-order/edit-order.component';
import { ProcessedOrdersComponent } from './components/processed-orders/processed-orders.component';
import { UpdateOrderStatusComponent } from './components/update-order-status/update-order-status.component';
import { MonthlyOrdersComponent } from './components/monthly-orders/monthly-orders.component';
import { AnnuallyOrdersComponent } from './components/annually-orders/annually-orders.component';
import { OrdersInLast60DaysComponent } from './components/orders-in-last60-days/orders-in-last60-days.component';
import { UserAccountActivityComponent } from './components/user-account-activity/user-account-activity.component';
import { UserOrdersActivityComponent } from './components/user-orders-activity/user-orders-activity.component';
import { ProductLogsComponent } from './components/product-logs/product-logs.component';
import { ReviewArtworkComponent } from './components/review-artwork/review-artwork.component';
import { PrintOrderComponent } from './components/print-order/print-order.component';
import { ChangepasswordComponent } from './components/changepassword/changepassword.component';
import { LoginoktaComponent } from './components/loginokta/loginokta.component';
import { ResetpasswordComponent } from './components/resetpassword/resetpassword.component';
import { ForgotpasswordComponent } from './components/forgotpassword/forgotpassword.component';

const routes: Routes = [
  { path: '', redirectTo: '/landing', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'loginokta', component: LoginoktaComponent },
  { path: 'verifyemail', component: EmailverifyComponent },
  { path: 'verifyotp', component: VerifyotpComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'landing', component: LandingComponent },
  { path: 'sso', component: SsoComponent },
  { path: 'authorize', component: AuthorizeComponent },
  { path: 'userprofile', component: UserprofileComponent },
  { path: 'createNewOrder', component: CreateNewOrderComponent },
  { path: 'repeatOrder', component: RepeatOrderComponent },
  { path: 'repeatOrder/:OrderID', component: RepeatOrderComponent },
  { path: 'createUser', component: CreateUserComponent },
  { path: 'createUser/:id', component: CreateUserComponent },
  { path: 'roleAccess/:OrderID', component: RoleAccessComponent },
  { path: 'viewOrder/:OrderID', component: ViewOrderComponent},
  { path: 'editOrder/:OrderID', component: EditOrderComponent},
  { path: 'allOrders', component: AllOrdersComponent },
  { path: 'allOrders/:OrderID', component: AllOrdersComponent },
  { path: 'uploadProof/:OrderID', component: UploadProofComponent },
  { path: 'reviewProof/:OrderID/:ProofID', component: ReviewProofComponent },
  { path: 'uploadArtwork/:OrderID', component: UploadArtworkComponent },
  { path: 'reviewArtwork/:OrderID/:ArtworkID', component: ReviewArtworkComponent },
  { path: 'resetOrderStatus/:OrderID', component: ResetOrderStatusComponent },
  { path: 'updateOrderStatus/:OrderID', component: UpdateOrderStatusComponent },
  { path: 'printOrder/:OrderID', component: PrintOrderComponent },
  { path: 'ChangePassword/:Token/:UserId', component: ChangepasswordComponent },
  { path: 'Resetpassword/:UserId', component: ResetpasswordComponent },
  { path: 'User', component: UsersComponent },
  { path: 'Forgotpassword', component: ForgotpasswordComponent },
  { path: 'processedOrders', component: ProcessedOrdersComponent },
  { path: 'reports', component: ReportsComponent },
  { path: 'monthlyOrders', component: MonthlyOrdersComponent },
  { path: 'annuallyOrders', component: AnnuallyOrdersComponent },
  { path: 'ordersInLast60Days', component: OrdersInLast60DaysComponent },
  { path: 'userAccountActivity', component: UserAccountActivityComponent },
  { path: 'userOrdersActivity', component: UserOrdersActivityComponent },
  { path: 'productLogs', component: ProductLogsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
