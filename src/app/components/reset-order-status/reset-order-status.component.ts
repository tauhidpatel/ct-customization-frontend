import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrdersService } from 'src/app/services/orders.service';
import { MailService } from 'src/app/services/mail.service';
import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-reset-order-status',
  templateUrl: './reset-order-status.component.html',
  styleUrls: ['./reset-order-status.component.css']
})
export class ResetOrderStatusComponent {

  UserID!: string;
  FirstName!: string;
  LastName!: string;
  EmailAddress!: any;

  selectedSite: any;

  allOrderStatus: any[] = [];
  OrderID!: string;
  currentOrderStatus!: string;
  selectedStatusId: any;

  userIPAdress!: string;
  browserName!: string;

  constructor (
    private orderService: OrdersService, 
    private route: ActivatedRoute,
    private router: Router,
    private MailService:MailService,
    private http: HttpClient,
    private _location: Location
  ) { 
    this.UserID = sessionStorage.getItem("UserID") ?? '';
    this.FirstName = sessionStorage.getItem("FirstName") ?? '';
    this.LastName = sessionStorage.getItem("LastName") ?? '';
    this.EmailAddress = sessionStorage.getItem("EmailAddress") ?? '';
  }

  goBack() {
    this._location.back();
  }

  ngOnInit(): void {
    this.getOrderStatus(); // Call getOrderStatus after fetching OrderID
    this.getAllOrderStatus();
    this.getIPInfo();
    this.getSelectedOrderSite();

    this.OrderID = this.route.snapshot.params['OrderID'];
  }

  getAllOrderStatus() {
    this.orderService.getAllOrderStatus().subscribe(
    (orderResponse) => {
      this.allOrderStatus = orderResponse;
    },
    (orderError) => {
      console.log(orderError)
    }
    )
  }

  resetOrderStatus(): void {
    debugger;
    this.orderService.updateOrderStatus(this.OrderID, this.selectedStatusId).subscribe(
      (response) => {
        console.log('Order status updated successfully:', response);
        
        this.browserName = (navigator as any).userAgentData.brands[0].brand;
        const logData = {
          UserID: this.UserID,
          IPAddress: this.userIPAdress, 
          OrderID: this.OrderID,
          ActionType: 'Status Reset',
          ActionDescription: `Admin Order status has been reset by ${this.EmailAddress}`,
          BrowserInfo: this.browserName, 
          Comments: '---', 
          Status: '' 
        };
      
        // Call your service method to insert log
        this.orderService.insertLog(logData).subscribe(
          (response) => {
            console.log('Log inserted successfully:', response);
          },
          (error) => {
            console.error('Error inserting log:', error);
          }
        );

        this.SendUpdateOrderStatusEmail(this.OrderID, this.selectedSite, "Order Status has been reset");

        // Redirect to '/allOrders' upon successful order creation
        const navigationExtras: NavigationExtras = {
          queryParams: { successAdminResetStatus: 'true' },
          fragment: 'top' // Scroll to top when navigating
        };
        this.router.navigate(['/allOrders'], navigationExtras);

      },
      (error) => {
        console.error('Error updating order status:', error);
      }
    );
    
  }

  SendUpdateOrderStatusEmail(OrderNo: any, Siteid:any, MESSAGE:any)
  {
    debugger;
    const Template="GeneralTemplate";
    // const Servername="http:%2F%2Flocalhost:4200%2F";
    // const ExtraUrl="null";

    const Servername = encodeURIComponent("https://digital-team-805wb.certainteed.com");
    const ExtraUrl = encodeURIComponent("Productcustomization");
  
    this.MailService.SendUpdateOrderStatusEmail( Template, Servername, ExtraUrl, OrderNo, this.selectedSite, MESSAGE).subscribe(
      (orderResponse) => {
        console.log('Order created successfully:', orderResponse);
      });
  }

  getOrderStatus() {
    const orderId = this.route.snapshot.params['OrderID']; // Retrieve OrderID from URL params
    
    if (orderId) {
      this.orderService.getCurrentOrderStatus(orderId).subscribe(
        (response: any) => {
          console.log(response);
          this.currentOrderStatus = response[0].Status;
          console.log(this.currentOrderStatus); 
        },
        (error) => {
          console.error('Error fetching current order status:', error);
        }
      );
    } else {
      console.warn('OrderID is undefined');
    }
  }
  
  // This method logs the StatusId of the selected value in the dropdown
  // onStatusSelectionChange(event: any) {
  //   console.log('Selected StatusId:', this.selectedStatusId);
  // }
  
  onStatusSelectionChange(event: any) {
    // Call the updateOrderStatus method with the selected StatusId
    console.log('Selected StatusId:', this.selectedStatusId);
    // this.orderService.updateOrderStatus(this.OrderID, this.selectedStatusId).subscribe(
    //   (response) => {
    //     console.log('Order status updated successfully:', response);
    //   },
    //   (error) => {
    //     console.error('Error updating order status:', error);
    //   }
    // );
  }

  getSelectedOrderSite() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.orderService.getSelectedOrderSite(orderId).subscribe(
      (response) => {
        console.log(response[0].SiteName);
        this.selectedSite = response[0].SiteName;
      },
      (error) => {
        console.log('error fetching order details', error);
      }
    )
  }

  getIPInfo() {
    this.http.get<any>('https://ipinfo.io').subscribe(
      (response) => {
        this.userIPAdress = response.ip;
      },
      (error) => {
        console.log('Error fetching IP info', error);
      }
    );
  }
  

}