import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetOrderStatusComponent } from './reset-order-status.component';

describe('ResetOrderStatusComponent', () => {
  let component: ResetOrderStatusComponent;
  let fixture: ComponentFixture<ResetOrderStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResetOrderStatusComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResetOrderStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
