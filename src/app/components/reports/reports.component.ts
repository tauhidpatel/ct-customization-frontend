import { Component } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent {

  constructor(private _location: Location ) { }

  goBack() {
    this._location.back();
  }

  ngOnInit(): void { }

}
