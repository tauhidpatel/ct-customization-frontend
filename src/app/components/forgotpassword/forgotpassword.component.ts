import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent {

  user: any = {
    Email: '',
    }
    showSuccessAlert: boolean = false;
    showErrorAlert1: boolean = false;
    showErrorAlert2: boolean = false;
  constructor(private UserService:UserService, private router: Router,private route:ActivatedRoute) {
    // this.UserID = sessionStorage.getItem("UserID") ?? '';
    }
 

  ForgotPassword()
  {
    debugger;
    var formdata = new FormData();
    formdata.append("email", this.user.Email);


    fetch("https://digital-h-1-uat.certainteed.com/OktaAPI/user/CheckUserEmail", {
      method: 'POST',
   
      body: formdata,
      redirect: 'follow'
  })
        .then(response => response.text())
        .then(result => {
            result = result.substring(result.indexOf("(") + 1, result.lastIndexOf(")"));
         var   result1 = JSON.parse(result);
            if (result1[0] != "[]") {
         // this.router.navigate(['/Resetpassword', data[0].id]);
        //  var data= JSON.parse(data[0]);
          this.UserService.ForgotPasswordSend(result1[0].profile.firstName,result1[0].profile.lastName,this.user.Email,result1[0].id,"https:%2F%2Fdigital-team-805wb.certainteed.com%2FProductcustomization").subscribe((data: any) => {
            debugger;
               console.log(data);
               this.showSuccessAlert=true;
           });
         }
         else
         {
this.showErrorAlert2=true;
         }
        });

  }
}
