import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrdersService } from 'src/app/services/orders.service';
import { Okta } from 'src/app/shared/okta/okta.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  loading: boolean = false; // loading spinner
  showIcons: boolean = true;
  showSuperRoles: boolean = false;
  user: any;
  oktaSignIn: any;

  userIPAdress!: string;
  browserName!: string;

  constructor(
    private okta: Okta, 
    private changeDetectorRef: ChangeDetectorRef,
    private router: Router,
    private zone: NgZone,
    private http: HttpClient,
    private route: ActivatedRoute,
    private ordersService: OrdersService
  ) 
  {
    this.oktaSignIn = okta.getWidget();
    // debugger;
  } 

  showLogin(): void {
    this.oktaSignIn?.remove();
    this.oktaSignIn.renderEl({el: '#okta-login-container'}, (response: any) => {
      if (response.status === 'SUCCESS') {
        debugger;
        console.log(response);
        this.oktaSignIn.authClient.tokenManager.add('idToken', response.tokens.idToken);
        this.oktaSignIn.authClient.tokenManager.add('accessToken', response.tokens.accessToken);
        this.user = response.tokens.idToken.claims.email;
        sessionStorage.setItem("userId",response.tokens.idToken.claims.sub);

        sessionStorage.setItem("oktaEmail",response.tokens.idToken.claims.email);
        this.oktaSignIn.remove();

        const userOktaId = response.tokens.idToken.claims.sub;
        this.ordersService.sendOktaUserId(userOktaId).subscribe(
          (data: any) => {
            debugger;
            console.log(data);
            
            this.zone.run(() => {
              this.router.navigate(['/login'], { queryParams: { 
                SGID: data[0].SGID,
                UserID: data[0].UserID,
                FirstName:  data[0].FirstName,
                LastName: data[0].LastName,
                EmailAddress: data[0].EmailAddress,
                CompanyName: data[0].CompanyName,
                Country: data[0].Country,
                SecurityLevel: data[0].SecurityLevel,
                SiteName: data[0].SiteName,
              }}); 
          });
        });
      } 
      return;
    });
    return;
  }

  // async ngOnInit(): Promise<void> {
  //   this.getIPInfo();
  // }
  async ngAfterViewInit() {
    console.log('in ngAfterViewInit');
    try {
      debugger;
      if (this.route.snapshot.queryParams['FirstName']) {
        this.route.queryParams.subscribe(params => {
        
          console.log(params);
          
          sessionStorage.setItem("SGID", params["SGID"]);
          sessionStorage.setItem("UserID", params["UserID"]);
          sessionStorage.setItem("FirstName", params["FirstName"]);
          sessionStorage.setItem("LastName", params["LastName"]);
          sessionStorage.setItem("EmailAddress", params["EmailAddress"]);
          sessionStorage.setItem("CompanyName", params["CompanyName"]);
          sessionStorage.setItem("Country", params["Country"]);
          sessionStorage.setItem("SecurityLevel", params["SecurityLevel"]);
          sessionStorage.setItem("SiteName", params["SiteName"]);
  
          debugger;
          console.log("UserID:", sessionStorage.getItem("UserID"));
          console.log("Country:", sessionStorage.getItem("Country"));
          console.log("FirstName:", sessionStorage.getItem("FirstName"));
          console.log("LastName:", sessionStorage.getItem("LastName"));
  
         
          
          
          window.location.href = "http://localhost:4200/welcome";
         // window.location.href = "https://digital-team-805wb.certainteed.com/Productcustomization/welcome";
        // window.location.href = "https://digital-team-805wb.certainteed.com/Productcustomization/welcome";
          });

          this.browserName = (navigator as any).userAgentData.brands[0].brand;
          const logData = {
            UserID: response.userId,
            IPAddress: this.userIPAdress, // retrieve the IP address if needed
            OrderID: '',
            ActionType: 'Login',
            ActionDescription: 'user has logged in successfully',
            BrowserInfo: this.browserName, 
            Comments: '---', // User comments for new order
            Status: '' // You may add status if needed
          };
        
          // Call your service method to insert log
          this.ordersService.insertLog(logData).subscribe(
            (response) => {
              console.log('Log inserted successfully:', response);
            },
            (error) => {
              console.error('Error inserting log:', error);
            }
          );
  
          
  
            window.location.href = "http://localhost:4200/welcome";
         // window.location.href = "https://digital-team-805wb.certainteed.com/Productcustomization/welcome";
  
          
          // .subscribe(
          //   (response) => {
          //     console.log(response);
          //   },
          //   (error) => { console.log(error); }
          // );
          this.loading = true;
      }
      else{
      var response = await this.oktaSignIn.authClient.session.get();
      // console.log(response);
   

      if (response.status !== "INACTIVE") {
        sessionStorage.setItem("userId",response.userId);
  
        debugger;
        const usersOktaId = response.userId;
        this.ordersService.sendOktaUserId(usersOktaId).subscribe(
          (data: any) => {
            debugger;
            console.log(data);
            var SiteName="";
            if(data.length>=1)
              {
for(var i=0;i<data.length;i++)
  {
    SiteName=SiteName+data[i].SiteName+",";
  }
        }
              SiteName = SiteName.substring(0, SiteName.length - 1);
            this.zone.run(() => {
              this.router.navigate(['/login'], { queryParams: { 
                SGID: data[0].SGID,
                UserID: data[0].UserID,
                FirstName:  data[0].FirstName,
                LastName: data[0].LastName,
                EmailAddress: data[0].EmailAddress,
                CompanyName: data[0].CompanyName,
                Country: data[0].Country,
                SecurityLevel: data[0].SecurityLevel,
                SiteName: SiteName,
              }}); 
          });
        });


  
       
      }
    
      else
      {
        this.showLogin();
      }
    }
    } catch (error) {
    }
}
  async ngOnInit() {
    this.getIPInfo();
   
  }

  getIPInfo() {
    this.http.get<any>('https://ipinfo.io').subscribe(
      (response) => {
        this.userIPAdress = response.ip;
      },
      (error) => {
        console.log('Error fetching IP info', error);
      }
    );
  }


}
