import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SsoService } from 'src/app/services/sso.service';

@Component({
  selector: 'app-sso',
  templateUrl: './sso.component.html',
  styleUrls: ['./sso.component.css']
})
export class SsoComponent {

  loading = true;
  message!: string;

  constructor(private ssoService: SsoService, private router: Router) {}

  // ngOnInit() {
  //   const sgid = 'T3776844';
  //   this.ssoService.performSso(sgid).subscribe(
  //     (response) => {
  //       this.message = response.message; // Extract the message from the response
  //       this.loading = false;
  //       // Redirect to the dashboard page after a short delay
  //       setTimeout(() => this.router.navigate(['/dashboard']), 5000);
  //     },
  //     (error) => {
  //       console.error('SSO error:', error);
  //       this.message = 'SSO validation failed.';
  //       this.loading = false;
  //     }
  //   );
  // }



}
