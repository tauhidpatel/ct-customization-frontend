import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrdersService } from 'src/app/services/orders.service';
import { UserService } from 'src/app/services/user.service';
import { UsersComponent } from '../users/users.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent {

  showSuccessAlert: boolean = false;
  showErrorAlert: boolean = false;
  showUpdateAlert: boolean = false;
  allSites: any[] = [];
  securityLevels: any[] = [];
  selected=0;

  user: any = {
    Userid:0,
    SiteId: '',
		EmailAddress: '',
		FirstName: '',
		LastName: '',
		SGID: '',
		CompanyName: '',
		Address: '',
		City: '',
		State: '',
		ZipCode: '',
		Country: '',
		Phone: '',
		securityLevel: '',

    // not passed
    DateCreated: '',
    DateUpdated: '',
    Status: '',
    oktauserid: ''
  };

  UserID: string;
  groupid!: string;
  EmailAddress!: string;

  constructor(private ordersService: OrdersService,private UserService:UserService, private router: Router,private route:ActivatedRoute, private _location: Location ) {
    this.UserID = sessionStorage.getItem("UserID") ?? ''
    this.EmailAddress = sessionStorage.getItem("EmailAddress") ?? ''
   }

   goBack() {
    this._location.back();
    console.log( 'goBack()...' );
  }
  userEmailContainsSaintGobain(): boolean {
    const emailAddress = sessionStorage.getItem("EmailAddress") ?? '';
    return emailAddress.toLowerCase().includes('saint-gobain');
  }
  UpdateoktaUser()
  {
    // debugger;
    // this.user.Userid=this.route.snapshot.params['id'];
    // this.UserService.UpdateUser(this.user.oktauserid,this.user.FirstName,this.user.LastName,this.user.EmailAddress,this.user.CompanyName).subscribe(
    //   (response) => {
    //   });
    var formdata = new FormData();
    formdata.append("userid", this.user.oktauserid)
    formdata.append("FirstName", this.user.FirstName)
    formdata.append("LastName", this.user.LastName)
    formdata.append("Email", this.user.EmailAddress)
    formdata.append("mobilePhone", this.user.Phone)
    formdata.append("organization", this.user.CompanyName)
    fetch("https://digital-h-1-uat.certainteed.com/OktaAPI/user/updateuser", {
      method: 'POST',
      body: formdata,
      redirect: 'follow'
  })
        .then(response => response.text())
        .then(result => {
            result = result.substring(result.indexOf("(") + 1, result.lastIndexOf(")"));
            result = JSON.parse(result);
        });
  }
  CreateOktaUser1()
  {
    if(this.user.SiteId==1)
      {
        this.groupid="00g4trsomioxEo1HE0x7";
      }
      else if(this.user.SiteId==2)
        {
          this.groupid="00g5nd54w6moyZXGS0x7"
        }
        else
        {
          this.groupid="00g5nd6phtUY2tJy80x7"
        }
    this.UserService.CheckUserEmail(this.user.EmailAddress).subscribe((data: any) => {
      debugger;
         console.log(data);
         if (data.length == 0) {
          if (this.user.EmailAddress.includes("@abcsupply.com") == false) {
    debugger;
    this.UserService.createuser(this.user.FirstName,this.user.LastName,this.user.EmailAddress,this.groupid,this.user.Phone,this.user.CompanyName).subscribe((data: any) => {
   debugger;
      console.log(data);
      
      if(data[1]=="BadRequest")
        {
          
          data=JSON.parse(data[0]);
          this.UserService.AddUserGroup(data.id,this.groupid).subscribe((data: any) => {
            debugger;
               console.log(data);
          });

        }
        else
        {
          
          data=JSON.parse(data[0]);
          this.user.oktauserid=data.id;
          this.UserService.Activateuser(data.id).subscribe(async (data: any) => {
            debugger;
               console.log(data);
               data=JSON.parse(data[0]);
               var activationToken=data.activationToken;
               this.ordersService.createUser(this.user).subscribe(
                (response) => {
                  console.log('User created successfully:', response);
                  console.log('Selected Security Level:', this.user.SecurityLevel);
                 
                  this.showSuccessAlert = true;
                },
                (error) => {
                  console.error('Error creating user:', error);
                }
              );
              // this.router.navigate(['/ChangePassword', activationToken,this.user.oktauserid]);
                   console.log(data);
                   this.UserService.Send(this.user.FirstName,this.user.LastName,this.user.EmailAddress,activationToken,this.user.oktauserid,"https:%2F%2Fdigital-team-805wb.certainteed.com%2FProductcustomization").subscribe((data: any) => {
                    debugger;
                       console.log(data);
                   });
               
          });
        }
      });
      }
    }
    else
    {
      
          this.UserService.AddUserGroup(this.groupid,data[0].id).subscribe((data: any) => {
            debugger;
               console.log(data);
          });
    }

    });
  }

  CreateOktaUser()
  {
    if(this.user.SiteId==1)
      {
        this.groupid="00g4trsomioxEo1HE0x7";
      }
      else if(this.user.SiteId==2)
        {
          this.groupid="00g5nd54w6moyZXGS0x7"
        }
        else
        {
          this.groupid="00g5nd6phtUY2tJy80x7"
        }
    var formdata = new FormData();
formdata.append("userid", this.UserID)
 


fetch("https://digital-h-1-uat.certainteed.com/OktaAPI/user/GetToken", {
  method: 'POST',
  body: formdata,

  redirect: 'follow' })
.then(response => response.text())
 .then(result=> {
  debugger;
localStorage.setItem("JWTToken", result);
var token = "Bearer " + result;



var Userid = this.UserID;

var myHeaders1 = new Headers();
myHeaders1.append("Content-Type", "application/json");

var raw = JSON.stringify({
    "token": result
});

var requestOptions = {
    method: 'POST',
    headers: myHeaders1,
    body: raw,
    redirect: 'follow'
};

fetch("https://digital-h-1-uat.certainteed.com/OktaAPI/user/ValidateToken", {
  method: 'POST',
  headers: myHeaders1,
  body: raw,
  redirect: 'follow'
})
    .then(response => response.text())
    .then(result => {
        console.log(result);
        if (result == Userid) {
            var myHeaders = new Headers();
            myHeaders.append("Authorization", token);

            var formdata = new FormData();
            formdata.append("email", this.user.EmailAddress);

            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: formdata,
                redirect: 'follow'
            };

            fetch("https://digital-h-1-uat.certainteed.com/OktaAPI/user/CheckUser", {
              method: 'POST',
              headers: myHeaders,
              body: formdata,
              redirect: 'follow'
          })
                .then(response => response.text())
                .then(result => {
                    result = result.substring(result.indexOf("(") + 1, result.lastIndexOf(")"));
                    result = JSON.parse(result);
                    if (result[0] == "[]") {
                        if (this.user.EmailAddress.includes("@abcsupply.com") == false) {
                            var formdata = new FormData();
                            formdata.append("FirstName", this.user.FirstName)
                            formdata.append("LastName", this.user.LastName)
                            formdata.append("Email", this.user.EmailAddress)
                            formdata.append("groupids", this.groupid)
                            formdata.append("mobilePhone", this.user.Phone)
                            formdata.append("organization", this.user.CompanyName)

                            var requestOptions = {
                                method: 'POST',
                                body: formdata,
                                headers: myHeaders,
                                redirect: 'follow'
                            };
                            fetch("https://digital-h-1-uat.certainteed.com/OktaAPI/user/createuser", {
                              method: 'POST',
                              body: formdata,
                              headers: myHeaders,
                              redirect: 'follow'
                          })
                                .then(response => response.text())
                                .then(result => {
                                    result = result.substring(result.indexOf("(") + 1, result.lastIndexOf(")"));
                                    result = JSON.parse(result);

                                    if (result[1] == "OK") {

                                        var data = JSON.parse(result[0]);
                                        var UserId = data.id;
                                        this.user.oktauserid=data.id;
                                        var requestOptions = {
                                            method: 'POST',
                                            headers: myHeaders,
                                            redirect: 'follow'
                                        };

                                        fetch("https://digital-h-1-uat.certainteed.com/OktaAPI/user/Activateuser?UserId=" + UserId, {
                                          method: 'POST',
                                          headers: myHeaders,
                                          redirect: 'follow'
                                      })
                                            .then(response => response.text())
                                            .then(result => {
                                                result = result.substring(result.indexOf("(") + 1, result.lastIndexOf(")"));
                                                result = JSON.parse(result);
                                              var  result1 = JSON.parse(result[0]);
                                                var activationToken = result1.activationToken;

                                                this.ordersService.createUser(this.user).subscribe(
                                                  (response) => {
                                                    console.log('User created successfully:', response);
                                                    console.log('Selected Security Level:', this.user.SecurityLevel);
                                                   
                                                    this.showSuccessAlert = true;
                                                  },
                                                  (error) => {
                                                    console.error('Error creating user:', error);
                                                  }
                                                );
                                                        this.UserService.Send(this.user.FirstName,this.user.LastName,this.user.EmailAddress,activationToken,this.user.oktauserid,"https:%2F%2Fdigital-team-805wb.certainteed.com%2FProductcustomization").subscribe((data: any) => {
                                                          debugger;
                                                             console.log(data);
                                                         });
                                                       
                                                        //localStorage.setItem("JWTToken", result);
                                                        //result = JSON.parse(result);
                                                   




                                            })



                                    }
                                })
                        }
          else
          {
          var formdata = new FormData();
          formdata.append("FirstName", this.user.FirstName)
          formdata.append("LastName", this.user.LastName)
          formdata.append("Email", this.user.EmailAddress)
          formdata.append("groupids", this.groupid)
          formdata.append("mobilePhone", this.user.Phone)
          formdata.append("organization", this.user.CompanyName)
formdata.append("login",Userid)
                            var requestOptions = {
                                method: 'POST',
                                body: formdata,
                                headers: myHeaders,
                                redirect: 'follow'
                            };
                            fetch("https://digital-h-1-uat.certainteed.com/OktaAPI/user/createusersupply", {
                              method: 'POST',
                              body: formdata,
                              headers: myHeaders,
                              redirect: 'follow'
                          })
                                .then(response => response.text())
                                .then(result => {
                                    result = result.substring(result.indexOf("(") + 1, result.lastIndexOf(")"));
                                    result = JSON.parse(result);
                                    if (result[1] == "OK") {

                                        var data = JSON.parse(result[0]);
                                        var UserId = data.id;
                                    var   activationToken = result;
                                                       
                                                        var formdata = new FormData();
                                                        formdata.append("FirstName", this.user.FirstName)
                                                        formdata.append("LastName", this.user.LastName)
                                                        formdata.append("Email", this.user.EmailAddress)
                                                        formdata.append("Token", "testtoken")
                                                       formdata.append("AppName", "Diamond Deck")
formdata.append("WebsiteURL", "https://digital-team-805wb.certainteed.com/Productcustomization/")
                                                        var requestOptions = {
                                                            method: 'POST',
                                                            headers: myHeaders,
                                                            body: formdata,
                                                            redirect: 'follow'
                                                        };

                                                        fetch("https://digital-h-1-uat.certainteed.com/OktaAPI/User/Sendmail", {
                                                          method: 'POST',
                                                          headers: myHeaders,
                                                          body: formdata,
                                                          redirect: 'follow'
                                                      })
                                                            .then(response => {

                                                               // alert("user created successfully");
                                                               
//window.location = "https://digital-team-805wb.certainteed.com/diamonddeck/CreateUser.asp?created=true";
                                                            })
                                                            .then(result => {})
                                                            .catch(error => console.log('error', error));
                                                        //localStorage.setItem("JWTToken", result);
                                                        //result = JSON.parse(result);



                                    }
                                })
          }

                    } else {
        
         var data = JSON.parse(result[0]);
                        var UserId = data[0].id;
        var formdata = new FormData();
            formdata.append("GroupID", this.groupid);
    formdata.append("Userid", UserId);

    
var found=false;
            fetch("https://digital-h-1-uat.certainteed.com/OktaAPI/user/CheckUserGroup", {
              method: 'POST',
           
              body: formdata,
              redirect: 'follow'
          })
                .then(response => response.text())
                .then(result => {
      //result = result.substring(result.indexOf("(") + 1, result.lastIndexOf(")"));
                    result = JSON.parse(result);
                    this.user.oktauserid=UserId;
                    this.ordersService.createUser(this.user).subscribe(
                      (response) => {
                        console.log('User created successfully:', response);
                        console.log('Selected Security Level:', this.user.SecurityLevel);
                       
                        this.showSuccessAlert = true;
                        this.router.navigate(['/User']);
                      },
                      (error) => {
                        console.error('Error creating user:', error);
                      }
                    );
        if(result!="")
        {
         result = JSON.parse(result);
        
         if(result=="true")
         {
          this.showSuccessAlert = true;
        //  window.location = "https://digital-team-805wb.certainteed.com/diamonddeck/CreateUser.asp?created=false";
         return;
         }
         }
         else
         {
               var formdata = new FormData();
                        formdata.append("groupid", this.groupid)
                        formdata.append("userid", UserId)


                        var requestOptions = {
                            method: 'POST',
                            body: formdata,
                            headers: myHeaders,
                            redirect: 'follow'
                        };

                        fetch("https://digital-h-1-uat.certainteed.com/OktaAPI/user/AddUserGroup", {
                          method: 'POST',
                          body: formdata,
                          headers: myHeaders,
                          redirect: 'follow'
                      })
                            .then(response => response.text())
                            .then(result => {

 
                                var formdata = new FormData();
                                formdata.append("FirstName", this.user.FirstName)
                                formdata.append("LastName", this.user.LastName)
                                formdata.append("Email", this.user.EmailAddress)
                                formdata.append("Token", "test")


                                var requestOptions = {
                                    method: 'POST',
                                    body: formdata,
                                    headers: myHeaders,
                                    redirect: 'follow'
                                };

                                fetch("https://digital-h-1-uat.certainteed.com/OktaAPI/User/Sendmail", {
                                  method: 'POST',
                                  body: formdata,
                                  headers: myHeaders,
                                  redirect: 'follow'
                              })
                                    .then(response => {
                                      this.showSuccessAlert=true;
                                      this.router.navigate(['/User']);
                                        //alert("user created successfully");

                                     //   window.location = "https://digital-team-805wb.certainteed.com/diamonddeck/CreateUser.asp?created=true";
                                    })


                            })
         }
        console.log(result);
        });
        
        
        

                       
                  

                    }
                })
                .catch(error => console.log('error', error));

        } else {

          // window.location = "https://digital-team-805wb.certainteed.com/diamonddeck/CreateUser.asp?created=false";
        }
    })
    .catch(error => {
       // window.location = "https://digital-team-805wb.certainteed.com/diamonddeck/CreateUser.asp?created=false";

    });


//result = JSON.parse(result);
});
  }
  
  createUser() {

    const UserId=this.route.snapshot.params['id'];
if(UserId!=undefined)
{
  debugger;
  console.log('User to be created:', this.user);
  this.user.Userid=this.route.snapshot.params['id'];

  this.ordersService.UpdateUser(this.user).subscribe(
    (response) => {
      this.UpdateoktaUser();
      console.log('User created successfully:', response);
      console.log('Selected Security Level:', this.user.SecurityLevel);
      this.showUpdateAlert = true;
      this.router.navigate(['/User']);
    },
    (error) => {
      console.error('Error creating user:', error);
    }
  );
  }
  else{
    console.log('User to be created:', this.user);
    this.ordersService.CheckUser(this.user).subscribe(
      async (response) => {
      console.log(response);
      if(response.length==0)
        {
          await this.CreateOktaUser();
 
   }
   
    else
    {
      
      this.showErrorAlert= true;
      
    }
      });

  }
  }


  ngOnInit() {
debugger;
this.loadallSites();
this.loadAllSecurityLevels();
const UserId=this.route.snapshot.params['id'];
if(UserId!=undefined)
{
this.ordersService.GetUserswithId(UserId).subscribe((data: any) => {
  console.log(data);
  this.user.EmailAddress=data[0].EmailAddress;
  this.user.SiteId=data[0].SiteId;
		console.log(data[0].SiteId);
    console.log(this.user.SiteId);
		
		this.user.FirstName=data[0].FirstName;
		this.user.LastName=data[0].LastName;
		
		this.user.CompanyName=data[0].CompanyName;
		this.user.Address=data[0].Address;
		this.user.City=data[0].City;
		this.user.State=data[0].State;
		this.user.ZipCode=data[0].ZipCode;
		this.user.Country=data[0].Country;
		this.user.Phone=data[0].Phone;
		this.user.securityLevel=data[0].SecurityLevel;
    this.selected=data[0].SecurityLevel;
    this.user.SGID=data[0].SGID;
    this.user.oktauserid=data[0].oktauserid
    $("#create")[0].innerHTML="Update User";
    //$("#update").css("display","block");
    //$("#create").css("display","none");

});

}
   
  }

  loadallSites() {
    this.ordersService.getAllSites(this.UserID).subscribe(
      (response: any[]) => {
        this.allSites = response;
        // console.log(this.allSites);
      },
      (error) => {
        console.error('Error fetching all sites', error);
      }
    )
  }

  loadAllSecurityLevels() {
    this.ordersService.getAllSecurityLevels().subscribe(
      (response: any[]) => {
        this.securityLevels = response;
        // console.log(this.securityLevels)
      },
      (error) => {
        console.error('Error fetching security Levels', error);
      }
    );
  }


}
