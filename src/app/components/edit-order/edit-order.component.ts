import { DatePipe, Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, HostListener } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { error } from 'jquery';
import { ArtworkService } from 'src/app/services/artwork.service';
import { OrdersService } from 'src/app/services/orders.service';
import { ProofService } from 'src/app/services/proof.service';

export class Product {
  proxyID: any;
  ProductCode!: string;
  ProductDescription!: string;
  ProductQty!: string;
}


@Component({
  selector: 'app-edit-order',
  templateUrl: './edit-order.component.html',
  styleUrls: ['./edit-order.component.css']
})
export class EditOrderComponent {

  orderID!: string; // order ID from the URL 
  UserID!: any;
  customerEmails: any[] = [];
  allSites: any[] = [];

  currentOrderType!: string;

  myArtwork: any;
  myProof: any;

  userIPAdress!: string;
  browserName!: string;

  FirstName!:string;
  LastName!:string;

  orderProductDetails: any[] = [];

  products: Product[] = [];

  showSuccessAlert: boolean = false;
  

  orderDetails: any;
  selectedSite: any;
  orderStatus: any;
  orderStatusID: any;

  selectedCustomer: any[] = [];

  order: any = {
    Siteid: '',
    CTOrderNumber: '',
    CTPONumber: '',
    EnteredBy: '',
    ArtworkID: '',
    ProofID: '',
    OrderType: '',
    OrderBy: '',
    CTShipToInformation_Name: '',
    CTShipToInformation_CompanyName: '',
    CTShipToInformation_AccountNumber: '',
    CTShipToInformation_Address: '',
    CTShipToInformation_City: '',
    CTShipToInformation_State: '',
    CTShipToInformation_ZipCode: '',
    CTShipToInformation_Country: '',
    CTShipToInformation_Phone: '',
    StatusId: '',

    CTShipToInformation_EmailAddress: '',
    ApprovedByClient: '',
    OrderID: '',
    RegionNumber: '',
    RepeatOrderComment: '',
    TerritoryNumber: ''
  };

  productDetails: any[] = [
    {
      proxyID: '',
      OrderID: '',
      ProductCode: '',
      ProductDescription: '',
      ProductQty: '',
      ReferenceNo: ''
    },
    {
      proxyID: '',
      OrderID: '',
      ProductCode: '',
      ProductDescription: '',
      ProductQty: '',
      ReferenceNo: ''
    },
    {
      proxyID: '',
      OrderID: '',
      ProductCode: '',
      ProductDescription: '',
      ProductQty: '',
      ReferenceNo: ''
    },
  ];

  // Initialize newProductDetails with empty objects
  newProductDetails: any[] = [
    {
      OrderID: '',
      ProductCode: '',
      ProductDescription: '',
      ProductQty: '',
      ReferenceNo: ''
    },
    {
      OrderID: '',
      ProductCode: '',
      ProductDescription: '',
      ProductQty: '',
      ReferenceNo: ''
    },
    {
      OrderID: '',
      ProductCode: '',
      ProductDescription: '',
      ProductQty: '',
      ReferenceNo: ''
    },
    {
      OrderID: '',
      ProductCode: '',
      ProductDescription: '',
      ProductQty: '',
      ReferenceNo: ''
    }
  ];

  customers : any = {
    userid: '',
    orderID: '',
    orderByAccountNumber: '',
    shipToAccountNumber: '',
    fullName: '',
    emailAddress: '',
    dateAdded: '',
    dateLastUsed: '',
    enteredBy: '',
    isPrimary: ''
  }

  constructor(
    private ordersService: OrdersService,
    private artworkService: ArtworkService,
    private proofService: ProofService,
    private router: Router, 
    private route: ActivatedRoute,
    private datePipe: DatePipe,
    private http: HttpClient,
    private _location: Location
  ) { 
    this.UserID = sessionStorage.getItem("UserID") ?? ''; // getting the UserID from session storage
  }

  goBack() {
    this._location.back();
  }

  ngOnInit(): void {
    // this.orderID = this.route.snapshot.params['OrderID'];
    // this.FirstName = sessionStorage.getItem("FirstName") ?? '';
    // this.LastName = sessionStorage.getItem("LastName") ?? '';
    
    // this.getIPInfo();
    // this.getOrderDetails();
    // this.getSelectedOrderSite();
    // this.getCurrentOrderStatus();
    // this.getFullNameAndEmailAddressByOrderID();

    // this.loadAllOrderSatus();
    // this.loadallSites();
    // this.loadCustomerEmails();

    // console.log('mah typeee: ', this.currentOrderType);
  }
  
  ngAfterViewInit() {
    this.orderID = this.route.snapshot.params['OrderID'];
    this.FirstName = sessionStorage.getItem("FirstName") ?? '';
    this.LastName = sessionStorage.getItem("LastName") ?? '';
    
    this.getIPInfo();
    this.getOrderDetails();
    this.getSelectedOrderSite();
    this.getCurrentOrderStatus();
    this.getFullNameAndEmailAddressByOrderID();

    this.loadAllOrderSatus();
    this.loadallSites();
    this.loadCustomerEmails();
    this.getOrderDetailsByOrderID();

  }

  loadAllOrderSatus() {
    this.ordersService.getAllOrderStatus().subscribe(
      (orderResponse: any) => {
        // console.log(orderResponse[0].StatusId);
        this.order.StatusId = orderResponse[0].StatusId;
      },
      (error) => {
        console.error('Error loading order status', error);
      }
    )
  }

  loadallSites() {
    this.ordersService.getAllSites(this.UserID).subscribe(
      (response: any[]) => { 
        this.allSites = response;
      },
      (error) => {
        console.error('Error fetching all sites', error);
      }
    )
  }

  // loading customer emails in the dropdown
  loadCustomerEmails() {
    this.ordersService.getCustomerEmails(this.order.SiteId).subscribe(
      (data: any[]) => {
        this.customerEmails = data;
      },
      (error) => {
        console.error('Error fetching customer emails', error);
      }
    );
  }

  createNewOrderDetails() {
    debugger;
    this.ordersService.getOrderDetails(this.orderID).subscribe(
      (orderDetails: any) => {
        debugger;

        const orderID = orderDetails[0].OrderID;
        // console.log(this.newProductDetails);
        this.newProductDetails.forEach((product: any) => {
          product.OrderID = orderID;
        });
        
        // Send the request to create product details
        this.createProductDetails(orderID);  

      },
      (orderError) => {
        console.error('Error creating order:', orderError);
      }
    );
  }

  createProductDetails(orderID: any) {
    this.newProductDetails.forEach((product: any) => { // Iterate over productDetails array
      
      if (
        product.ProductCode.trim() !== '' &&
        product.ProductDescription.trim() !== '' &&
        product.ProductQty.trim() !== ''
      ) {
        const productToSend = {
          OrderID: this.orderID,
          ProductCode: product.ProductCode,
          ProductDescription: product.ProductDescription,
          ProductQty: product.ProductQty,
          ReferenceNo: product.ReferenceNo
        };
    
        this.ordersService.createProductDetails(productToSend).subscribe(
          (ProductPesponse) => {
            console.log('Product detail added successfully:', ProductPesponse);
          },
          (error) => {
            console.error('Error creating product detail:', error);
          }
        );
      }
    });
  }


  getOrderDetails() {
    this.ordersService.getOrderDetails(this.orderID).subscribe(
      (orderDetails: any) => {
        console.log(orderDetails);
        
        this.order.OrderID = this.route.snapshot.params['OrderID'];
        this.currentOrderType = orderDetails[0].OrderType;

        this.order.orderID = orderDetails[0].OrderID;
        this.order.userid = orderDetails[0].Userid;
        this.order.Siteid = orderDetails[0].Siteid;
        this.order.CTOrderNumber = orderDetails[0].CTOrderNumber
        this.order.CTPONumber = orderDetails[0].CTPONumber

        this.myArtwork = orderDetails[0].ArtworkID
        this.myProof = orderDetails[0].ProofID
        
        this.order.EnteredBy = orderDetails[0].EnteredBy
        this.order.OrderType = orderDetails[0].OrderType
        this.order.OrderBy = orderDetails[0].OrderBy
        this.order.CTShipToInformation_Name = orderDetails[0].CTShipToInformation_Name
        this.order.CTShipToInformation_CompanyName = orderDetails[0].CTShipToInformation_CompanyName
        this.order.CTShipToInformation_AccountNumber = orderDetails[0].CTShipToInformation_AccountNumber
        this.order.CTShipToInformation_Address = orderDetails[0].CTShipToInformation_Address
        this.order.CTShipToInformation_City = orderDetails[0].CTShipToInformation_City
        this.order.CTShipToInformation_State = orderDetails[0].CTShipToInformation_State
        this.order.CTShipToInformation_ZipCode =  orderDetails[0].CTShipToInformation_ZipCode
        this.order.CTShipToInformation_Country = orderDetails[0].CTShipToInformation_Country
        this.order.CTShipToInformation_Phone = orderDetails[0].CTShipToInformation_Phone
        this.order.CTShipToInformation_EmailAddress = orderDetails[0].CTShipToInformation_EmailAddress
        // this.order.ApprovedByClient = orderDetails[0].ApprovedByClient
        // this.order.RegionNumber = orderDetails[0].RegionNumber
        // this.order.RepeatOrderComment = orderDetails[0].RepeatOrderComment
        // this.order.TerritoryNumber = orderDetails[0].TerritoryNumber
        
      },
      (error) => {
        console.log('error fetching order details', error);
      }
    )
  }

  getSelectedOrderSite() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getSelectedOrderSite(orderId).subscribe(
      (response) => {
        // console.log(response);
        this.selectedSite = response;
      },
      (error) => {
        console.log('error fetching order details', error);
      }
    )
  }

  getCurrentOrderStatus() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getCurrentOrderStatus(orderId).subscribe(
      (response) => {
        console.log(response[0].StatusId);
        this.orderStatus = response;
        this.orderStatusID = response[0].StatusId;

      },
      (error) => {
        console.log('erorrrrr', error);
      }
    )
  }

  getOrderDetailsByOrderID() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getGetOrderDetailsByOrderID(orderId).subscribe(
      (res: any[]) => {
        res.sort((a, b) => a.ProductCode - b.ProductCode); // Sort products by ProductCode
        this.orderProductDetails = res;
        console.log(this.orderProductDetails);
      },
      (error) => {
          console.error('Error fetching order details:', error);
      }
    );
  }

  // new dynamic method
  updateOrderDetails() {
    debugger;
    const productDetails = this.orderProductDetails;
    console.log('Product details for update:', productDetails);
    this.ordersService.updateProductDetails(productDetails).subscribe(
      (data: any) => {
        console.log('Order details updated successfully', data);
      },
      (error) => {
        console.log('Unable to update order details', error);
      }
    );
  }
  
  getFullNameAndEmailAddressByOrderID() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getFullNameAndEmailAddressByOrderID(orderId).subscribe(
      (response: any[]) => {
        // console.log(response);
        this.selectedCustomer = response;
      },
      (error) => {
        console.log('errrrrorrrrr', error);
      }
    )
  }

  redirectToUploadArtwork(OrderID: string) {
    // Navigate to '/uploadArtwork' route with parameters
    this.router.navigate([ '/uploadArtwork', OrderID ]);
  }

  updateSelectedCustomer() {
    const selectedUserId = this.order.CTShipToInformation_EmailAddress;

    const index = selectedUserId - 1; // Assuming UserID starts from 1
    // Check if the index is valid
    if (index < 0 || index >= this.customerEmails.length) {
      console.error('Invalid index or selected user ID.');
      return;
    }

    // Access the email address using the index
    const selectedEmailAddress = this.customerEmails[index].EmailAddress;
    
    this.customers.userid = selectedUserId;
    this.customers.emailAddress = selectedEmailAddress;


  }

  loadCustomerEmailsAndAssignCustomers(orderID: any) {
    this.ordersService.getCustomerEmails(this.order.SiteId).subscribe(
      (data: any[]) => {
        debugger;
        console.log(data);
        this.customerEmails = data;

        // selected email address
        const selectedEmailAddress = this.order.CTShipToInformation_EmailAddress;
        console.log('selected email address: ', selectedEmailAddress);

        const selectedCustomer = this.customerEmails.find(email => email.EmailAddress === selectedEmailAddress);
        console.log('sss: ', selectedCustomer);
        
        if(selectedCustomer) {

          const customers = {
            fullName: selectedCustomer.FirstName + " " + selectedCustomer.LastName,
            enteredBy: selectedCustomer.FirstName,
            userid: selectedCustomer.UserID,
            emailAddress: encodeURIComponent(selectedCustomer.EmailAddress),
            orderID: orderID,
            DateAdded: '',
            DateLastUsed: '',
            IsPrimary: '',
            OrderByAccountNumber: '',
            ShipToAccountNumber: ''
          };

          this.assignCustomers(customers);

          this.browserName = (navigator as any).userAgentData.brands[0].brand
        const logData = {
          UserID: this.UserID,
          IPAddress: this.userIPAdress, 
          OrderID: this.orderID,
          ActionType: 'customer assigned',
          ActionDescription: `${selectedEmailAddress} was assigned to this order`,
          BrowserInfo: this.browserName, 
          Comments: '---', 
          Status: '' 
        };
      
        // Call your service method to insert log
        this.ordersService.insertLog(logData).subscribe(
          (response) => {
            console.log('Log inserted successfully:', response);
          },
          (error) => {
            console.error('Error inserting log:', error);
          }
        );
        } else {
          console.error('Selected customer not found!');
        }
      },
      (error) => {
        console.error('Error fetching customer emails', error);
      }
    );
  }

  assignCustomers(customers: any) {
    this.ordersService.assignCustomers(customers).subscribe(
      (customerResponse) => {
        console.log("Customers has been assigned !!!", customerResponse);
      },
      (customerError) => {
        console.log("Customers NOT ASSIGNED !!!", customerError);
      }
    );
  }

  updateOrder() {
    // debugger;
    this.order.OrderID = this.route.snapshot.params['OrderID'];
    this.order.OrderType = this.currentOrderType;
    this.order.StatusId = this.orderStatusID;
    // this.order.ArtworkID = 

    this.ordersService.updateOrder(this.order).subscribe(
      (response) => {
        console.log('Order updated successfully', response);
        this.updateOrderDetails();
        this.createNewOrderDetails();
        this.loadCustomerEmailsAndAssignCustomers(this.order.orderID );

        this.browserName = (navigator as any).userAgentData.brands[0].brand
        const logData = {
          UserID: this.UserID,
          IPAddress: this.userIPAdress, 
          OrderID: this.orderID,
          ActionType: 'Order Updated',
          ActionDescription: `order was updated by ${this.FirstName}${this.LastName}`,
          BrowserInfo: this.browserName, 
          Comments: '---', 
          Status: '' 
        };
      
        // Call your service method to insert log
        this.ordersService.insertLog(logData).subscribe(
          (response) => {
            console.log('Log inserted successfully:', response);
          },
          (error) => {
            console.error('Error inserting log:', error);
          }
        );

        // Redirect to '/allOrders' upon successful order creation
        const navigationExtras: NavigationExtras = {
          queryParams: { updateSuccess: 'true' },
          fragment: 'top' // Scroll to top when navigating
        };
        this.router.navigate(['/allOrders'], navigationExtras);

      },

      
      (error) => {
        console.log(error);
      }
    )
  }

  downloadArtwork(OrderID: string) {
    debugger;
    this.artworkService.getArtworkToDownload(OrderID).subscribe(
      (res) => {
        const artworkDownloadOrderID = res[0].OrderID; 
        const artworkDownloadFileName = res[0].FileName;
  
        this.artworkService.downloadArtworkFile(artworkDownloadOrderID, artworkDownloadFileName)
          .subscribe(
            (fileRes) => {
              const blob = new Blob([fileRes], { type: 'application/octet-stream' });
              const url = window.URL.createObjectURL(blob);
              
              const link = document.createElement('a');
              link.href = url;
              link.download = artworkDownloadFileName; 
              document.body.appendChild(link);
              
              link.click();
              
              window.URL.revokeObjectURL(url);
              document.body.removeChild(link);
              
              console.log("File downloaded successfully");
            },
            (error) => {
              console.log("Failed to download file", error);
            }
          );
      },
      (error) => {
        console.log('Failed to get artwork details', error);
      }
    );
  }

  downloadProof(OrderID: string) {
    debugger;
    this.proofService.getProofToDownload(OrderID).subscribe(
      (res) => {
        const proofDownloadOrderID = res[0].OrderID; 
        const proofDownloadFileName = res[0].FileName;
  
        this.proofService.downloadProofFile(proofDownloadOrderID, proofDownloadFileName)
          .subscribe(
            (fileRes) => {
              const blob = new Blob([fileRes], { type: 'application/octet-stream' });
              const url = window.URL.createObjectURL(blob);
              
              const link = document.createElement('a');
              link.href = url;
              link.download = proofDownloadFileName; 
              document.body.appendChild(link);
              
              link.click();
              
              window.URL.revokeObjectURL(url);
              document.body.removeChild(link);
              
              console.log("File downloaded successfully");
            },
            (error) => {
              console.log("Failed to download file", error);
            }
          );
      },
      (error) => {
        console.log('Failed to get artwork details', error);
      }
    );
  }

  getIPInfo() {
    this.http.get<any>('https://ipinfo.io').subscribe(
      (response) => {
        this.userIPAdress = response.ip;
      },
      (error) => {
        console.log('Error fetching IP info', error);
      }
    );
  }

}