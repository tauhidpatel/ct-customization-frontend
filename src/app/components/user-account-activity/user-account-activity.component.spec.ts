import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAccountActivityComponent } from './user-account-activity.component';

describe('UserAccountActivityComponent', () => {
  let component: UserAccountActivityComponent;
  let fixture: ComponentFixture<UserAccountActivityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserAccountActivityComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserAccountActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
