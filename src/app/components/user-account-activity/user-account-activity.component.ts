import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { OrdersService } from 'src/app/services/orders.service';

declare var $: any;

@Component({
  selector: 'app-user-account-activity',
  templateUrl: './user-account-activity.component.html',
  styleUrls: ['./user-account-activity.component.css']
})
export class UserAccountActivityComponent {

  accountHistory: any[] = [];

  UserID: any;

  constructor( private ordersService: OrdersService, private _location: Location ) { 
    this.UserID = sessionStorage.getItem("UserID") ?? '';
  }

  goBack() {
    this._location.back();
  }
  // ngOnInit(): void {
  //   this.getAccountChangeHistory();
  // }

  ngAfterViewInit(): void {
    // this.initDataTables();
    this.getAccountChangeHistory();
  }

  getAccountChangeHistory() {
    this.ordersService.getAccountChangeHistory(this.UserID).subscribe(
      (response) => {
        this.accountHistory = response;
        this.initDataTables();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  private initDataTables(): void {
    $(document).ready(function () {
      $('#accountDataTable').DataTable({
        columnDefs: [{ type: 'date', targets: [0] }],
        order: [[0, 'desc']]
      });
    });
  }

  // private initDataTables(): void {
  //   $('#accountDataTable').DataTable().destroy();
  //   $(document).ready(function () {
  //     $('#example').DataTable({
  //       columnDefs: [{ type: 'date', targets: [0] }],
  //       order: [[0, 'desc']]
  //     });
  //   });
  // }

}
