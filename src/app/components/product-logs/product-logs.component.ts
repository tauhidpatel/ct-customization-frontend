import { Component } from '@angular/core';
import { OrdersService } from 'src/app/services/orders.service';
import { Chart, registerables } from 'chart.js';
import { Location } from '@angular/common';

Chart.register(...registerables);

@Component({
  selector: 'app-product-logs',
  templateUrl: './product-logs.component.html',
  styleUrls: ['./product-logs.component.css']
})
export class ProductLogsComponent {

  productLogs: any[] = [];

  UserID: any;

  constructor( private ordersService: OrdersService, private _location: Location ) { 
    this.UserID = sessionStorage.getItem("UserID") ?? '';
  }

  goBack() {
    this._location.back();
  }

  ngOnInit(): void {
    this.getProductLogs();
  }

  getProductLogs() {
    this.ordersService.getProductLogs(this.UserID).subscribe(
      (response: any) => {
        // console.log(response);
        this.productLogs = response[0];
        this.renderChart();
      },
      (error) => {
        console.log(error);
      }
    )
  }

  renderChart() {
    const labels = Object.keys(this.productLogs);
    const counts = Object.values(this.productLogs);

    const myChart = new Chart('barchart', {
      type: 'bar',
      data: {
        labels: labels, 
        datasets: [{
          label: 'Products',
          data: counts,
          borderWidth: 1,
        }]
      },
      options: {
        scales: {
          x: {
            ticks: {
              font: {
                size: 10
              }
            }
          },
          y: {
            beginAtZero: true
          }
        }
      }
    });
  }

}
