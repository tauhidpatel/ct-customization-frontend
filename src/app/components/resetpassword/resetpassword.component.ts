import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent {

  user: any = {
    NewPassword: '',
    ConfirmPassword: ''
    }
    UserId: string | undefined;
    Token:string | undefined;
    showSuccessAlert: boolean = false;
    showErrorAlert1: boolean = false;
    showErrorAlert2: boolean = false;
    showErrorAlert3: boolean = false;
  
    constructor(private UserService:UserService, private router: Router,private route:ActivatedRoute) {
     // this.UserID = sessionStorage.getItem("UserID") ?? '';
     }
  
     ChangePassword() {
      this.UserId = this.route.snapshot.params['UserId'];
      if(this.user.NewPassword=='' || this.user.ConfirmPassword=='')
        {
  this.showErrorAlert1=true;
        }
        else if(this.user.NewPassword!= this.user.ConfirmPassword)
          {
            this.showErrorAlert2=true;
          }
          else
          {
            debugger;
            $.ajax({
              type: "POST",
              url: "https://digital-h-1-uat.certainteed.com/OktaAPI/user/SetPassword",
              data: {"User": this.UserId ,"Password": this.user.ConfirmPassword},
              contentType: "application/json; charset=utf-8",
              dataType: "jsonp",
              success: function (response) {
    if(response[1]=="BadRequest")
    {
      $("#cover-spin").hide();
            this.showErrorAlert3=true;
    }
    else{
      $("#cover-spin").hide();
  $(".alert-success").css("display", "block");
  this.showSuccessAlert = true;
    }
    }
    });

    }  
     }
  }
  
