import { DatePipe, Location } from '@angular/common';
import { Component } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ProofService } from 'src/app/services/proof.service';
import { MailService } from 'src/app/services/mail.service';
import { HttpClient } from '@angular/common/http';
import { OrdersService } from 'src/app/services/orders.service';
import { ArtworkService } from 'src/app/services/artwork.service';

@Component({
  selector: 'app-review-proof',
  templateUrl: './review-proof.component.html',
  styleUrls: ['./review-proof.component.css']
})
export class ReviewProofComponent {

  UserID: string;
  orderID: any;
  ProofID: any;
  str!: string;

  OrderName: any
  
  // variables to track checkbox states
  checkbox1Checked: boolean = false;
  checkbox2Checked: boolean = false;

  pmsColors: any;
  artworkFileName: any;
  proofFileName: any;

  userIPAdress!: string;
  browserName!: string;

  FirstName!: string;
  LastName!: string;
  EmailAddress: any;

  selectedSite: any;

  constructor(
    private proofService: ProofService, 
    private ordersService: OrdersService,
    private ArtworkService: ArtworkService,
    private datePipe: DatePipe,
    private MailService :MailService,  
    private route: ActivatedRoute,
    private router: Router,  
    private http: HttpClient,
    private _location: Location 
  ) 
  { 
    this.UserID = sessionStorage.getItem("UserID") ?? '';
    this.FirstName = sessionStorage.getItem("FirstName") ?? '';
    this.LastName = sessionStorage.getItem("LastName") ?? '';
    this.EmailAddress = sessionStorage.getItem("EmailAddress") ?? '';
  }

  goBack() {
    this._location.back();
  }

  ngOnInit(): void {
    this.getIPInfo();
    this.getSelectedOrderSite();
    this.getOrderDetails();
    
    this.orderID = this.route.snapshot.params['OrderID'];
    this.ProofID = this.route.snapshot.params['ProofID'];
  } 

  ngAfterViewInit() {
    this.getPMSColors();
  }

  getPMSColors() {
    this.ordersService.getPMSColor(this.orderID).subscribe(
      (res) => {
        this.artworkFileName = res[0].FileName;
        this.proofFileName  = res[0].ProofFileName;
        if (res[0].PMSColors.length > 0 && res[0].PMSColors) {
          this.pmsColors = res[0].PMSColors;
        } else {
          this.pmsColors = 'N/A';
        }
        this.pmsColors = res[0].PMSColors;
      },
      (error) => {
        console.log(error);
      }
    )
  }

  ProofApprove() {
    // if both checkboxes are checked
    if (this.checkbox1Checked && this.checkbox2Checked) {
      this.proofService.ProofApprove( this.orderID, this.UserID, this.userIPAdress, this.ProofID ).subscribe(
        (Response) => {
          debugger;
          console.log(Response);
  
          if(Response === 'Success') {
            this.ProofApproveMail(this.OrderName,'1') 
  
            this.browserName = (navigator as any).userAgentData.brands[0].brand
            const logData = {
              UserID: this.UserID,
              IPAddress: this.userIPAdress, 
              OrderID: this.orderID,
              ActionType: 'Artwork Approved',
              ActionDescription: `Artwork was approved by ${this.EmailAddress}`,
              BrowserInfo: this.browserName, 
              Comments: '---', 
              Status: '' 
            };
          
            // Call your service method to insert log
            this.ordersService.insertLog(logData).subscribe(
              (response) => {
                console.log('Log inserted successfully:', response);
              },
              (error) => {
                console.error('Error inserting log:', error);
              }
            );
  
            this.ordersService.updateOrderStatus(this.orderID, 4).subscribe(
              (res) => { 
                console.log('order status updated successfully',res);
              },
              (error) => {
                console.log('unable to update order status', error);
              }
            )
  
            // Redirect to '/allOrders' upon successful order creation
            const navigationExtras: NavigationExtras = {
              queryParams: { proofAccepted: 'true' },
              fragment: 'top' // Scroll to top when navigating
            };
            this.router.navigate(['/allOrders'], navigationExtras);
          }
        });
    } else {
      // Alert user to check both checkboxes
      alert('Please tick both the checkboxes to proceed with the approval.');
    }
  }

  ProofReject() {
    // Check if rejection reason is provided
    if (this.str && this.str.trim().length > 0) {
      this.proofService.ProofReject(this.orderID, this.UserID, this.userIPAdress, this.ProofID, this.str).subscribe(
        (Response) => {
          console.log(Response);
  
          if(Response === 'Success') {
            this.ProofRejectMail(this.orderID,'1');
  
            this.browserName = (navigator as any).userAgentData.brands[0].brand
            const logData = {
              UserID: this.UserID,
              IPAddress: this.userIPAdress, 
              OrderID: this.orderID,
              ActionType: 'Artwork Rejected',
              ActionDescription: `Artwork was rejected by ${this.EmailAddress}`,
              BrowserInfo: this.browserName, 
              Comments: '---', 
              Status: '' 
            };
          
            // Call your service method to insert log
            this.ordersService.insertLog(logData).subscribe(
              (response) => {
                console.log('Log inserted successfully:', response);
              },
              (error) => {
                console.error('Error inserting log:', error);
              }
            );
  
            this.ordersService.updateOrderStatus(this.orderID, 1).subscribe(
              (res) => { 
                console.log('order status updated successfully',res);
              },
              (error) => {
                console.log('unable to update order status', error);
              }
            )
  
            // Redirect to '/allOrders' upon successful order creation
            const navigationExtras: NavigationExtras = {
             queryParams: { showProofRejectedByVendor: 'true' },
             fragment: 'top' // Scroll to top when navigating
           };
           this.router.navigate(['/allOrders'], navigationExtras);
  
          } else {
            // Handle if response is not 'Success'
            console.error('Proof rejection failed:', Response);
          }
  
        },
        (error) => {
          console.log("proof rejected something: ", error);
        });
    } else {
        // Inform the user to provide a rejection reason
        alert('Please provide a reason for rejecting the proof.');
    }
    
  }

  ProofApproveMail(OrderNo: any,Siteid:any)
  {
    debugger;
    const Template="ProofApprovedContinueOrder";
    // const Servername="http:%2F%2Flocalhost:4200%2F";
    // const ExtraUrl="null";
    const Servername = encodeURIComponent("https://digital-team-805wb.certainteed.com");
    const ExtraUrl = encodeURIComponent("Productcustomization");

   const varExistingFileID= this.proofFileName;
  
    this.MailService.SendProofApprovedContinueOrderEmail( Template, Servername, ExtraUrl, this.OrderName, this.selectedSite, varExistingFileID ).subscribe(
      (orderResponse) => {
        console.log('Order created successfully:', orderResponse);
      });
  }

  ProofRejectMail(OrderNo: any,Siteid:any)
  {
    debugger;
    const Template="ProofRejectedByCustomer";
    // const Servername="http:%2F%2Flocalhost:4200%2F";
    // const ExtraUrl="null";

    const Servername = encodeURIComponent("https://digital-team-805wb.certainteed.com");
    const ExtraUrl = encodeURIComponent("Productcustomization");

   const varExistingFileID= this.proofFileName;
   const REJECTREASON=this.str;
  
    this.MailService.SendProofRejectedByCustomerEmail( Template, Servername, ExtraUrl, this.OrderName, this.selectedSite, varExistingFileID, REJECTREASON ).subscribe(
      (orderResponse) => {
        console.log('Order created successfully:', orderResponse);
      });
  }

  downloadArtwork() {
    debugger;
    this.ArtworkService.getArtworkToDownload(this.orderID).subscribe(
      (res) => {
        const artworkDownloadOrderID = res[0].OrderID; 
        const artworkDownloadFileName = res[0].FileName;
  
        this.ArtworkService.downloadArtworkFile(artworkDownloadOrderID, artworkDownloadFileName)
          .subscribe(
            (fileRes) => {
              const blob = new Blob([fileRes], { type: 'application/octet-stream' });
              const url = window.URL.createObjectURL(blob);
              
              const link = document.createElement('a');
              link.href = url;
              link.download = artworkDownloadFileName; 
              document.body.appendChild(link);
              
              link.click();
              
              window.URL.revokeObjectURL(url);
              document.body.removeChild(link);
              
              console.log("File downloaded successfully");
            },
            (error) => {
              console.log("Failed to download file", error);
            }
          );
      },
      (error) => {
        console.log('Failed to get artwork details', error);
      }
    );
  }

  getOrderDetails() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getOrderDetails(orderId).subscribe(
      (res) => { 
        // console.log(res);
        this.OrderName = res[0].CTOrderNumber;
      },
      (error) => {
        console.log(error);
      }
    )
  }

  getIPInfo() {
    this.http.get<any>('https://ipinfo.io').subscribe(
      (response) => {
        // console.log(response);
        // console.log(response.ip);
        this.userIPAdress = response.ip;
      },
      (error) => {
        console.log('Error fetching IP info', error);
      }
    );
  }

  getSelectedOrderSite() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getSelectedOrderSite(orderId).subscribe(
      (response) => {
        console.log(response[0].SiteName);
        this.selectedSite = response[0].SiteName;
      },
      (error) => {
        console.log('error fetching order details', error);
      }
    )
  }

}
