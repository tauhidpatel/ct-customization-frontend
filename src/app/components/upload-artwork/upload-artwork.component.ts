import { Component } from '@angular/core';
import { ArtworkService } from 'src/app/services/artwork.service';
import { MailService } from 'src/app/services/mail.service';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { OrdersService } from 'src/app/services/orders.service';
import { HttpClient } from '@angular/common/http';
import { Router, NavigationExtras } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-upload-artwork',
  templateUrl: './upload-artwork.component.html',
  styleUrls: ['./upload-artwork.component.css']
})
export class UploadArtworkComponent {

  orderID!: string; // ORDERID FROM THE URL PARAMS
  UserID!: string; // of the one who is logged in
  FirstName!: string;
  LastName!: string;
  EmailAddress: any;

  responseArtworkID: any;

  currentOrderNumber: any;
  currentOrderID: any;

  selectedSite: any;

  artworkOrderName!: string;

  userIPAdress!: string;
  browserName!: string;

  currentDate = new Date();

  showErrorAlert: boolean = false;
  showSuccessAlert: boolean = false;
  showFileSelectAlert: boolean = false;

  LogDetails: any[] = [];

  artworks: any = {
    orderId: "",
    artworkID: "",
    userId: "",
    fileName: "",
    origFileName: "",
    dateAdded: "",
    pmsColors: ""
  }

  constructor(
    private artworkService: ArtworkService,
    private ordersService: OrdersService, 
    private MailService :MailService,
    private route: ActivatedRoute,
    private router: Router,
    private datePipe: DatePipe,
    private http: HttpClient,
    private _location: Location
  ) {
    this.UserID = sessionStorage.getItem("UserID") ?? '';
    this.FirstName = sessionStorage.getItem("FirstName") ?? '';
    this.LastName = sessionStorage.getItem("LastName") ?? '';
    this.EmailAddress = sessionStorage.getItem("EmailAddress") ?? '';
   }

   goBack() {
    this._location.back();
  }

  ngOnInit(): void {
    this.orderID = this.route.snapshot.params['OrderID'];
    this.getIPInfo();
    this.getOrderDetails();
    this.getSelectedOrderSite();
  } 

  uploadArtwork(): void {
    debugger;
    this.artworks.orderId = this.orderID;
    this.artworks.pmsColors = this.artworks.pmsColors;
    this.artworkService.uploadArtwork(this.artworks).subscribe(
      (artworkResponse) => {
        console.log("Artwork uploaded successfully!!");
        console.log('Artwork Details', artworkResponse);
        console.log('Artwork ID', artworkResponse[0].artworkID); 
        this.responseArtworkID = artworkResponse[0].artworkID; 
        console.log('ArtworkS OBJECT', this.artworks);

        debugger;
        
        this.browserName = (navigator as any).userAgentData.brands[0].brand
        const logData = {
          UserID: this.UserID,
          IPAddress: this.userIPAdress, 
          OrderID: this.orderID,
          ActionType: 'Upload Artwork',
          ActionDescription: `Artwork was uploaded by ${this.EmailAddress}`,
          BrowserInfo: this.browserName, 
          Comments: '---', 
          Status: '' 
        };
      
        // Call your service method to insert log
        this.ordersService.insertLog(logData).subscribe(
          (response) => {
            console.log('Log inserted successfully:', response);
          },
          (error) => {
            console.error('Error inserting log:', error);
          }
        );

        this.ordersService.updateOrderStatus(this.orderID, 3).subscribe(
          (res) => { 
            console.log('order status updated successfully',res);
          },
          (error) => {
            console.log('unable to update order status', error);
          }
        )
        
        this.SendUploadArtworkEmail( this.artworkOrderName, this.selectedSite, this.artworks.fileName, this.artworks.pmsColors, this.orderID );
        
        // Redirect to '/allOrders' upon successful order creation
        const navigationExtras: NavigationExtras = {
          queryParams: { successArtwork: 'true' },
          fragment: 'top' // Scroll to top when navigating
        };

        
        this.router.navigate(['/allOrders'], navigationExtras);

      },
      (artworkError) => {
        console.log("Unable to upload Artwok Data", artworkError);
      }
    );
  }

  SendUploadArtworkEmail(OrderNo: any,Siteid:any,varExistingFileID:any,PMSCOLORS:any, ArtworkID: any)
  {
    debugger;
    const Template="LogoUploadedSendToVendor";
    // const Servername="http:%2F%2Flocalhost:4200%2F";
    // const ExtraUrl="null";
    const Servername = encodeURIComponent("https://digital-team-805wb.certainteed.com");
    const ExtraUrl = encodeURIComponent("Productcustomization");
   
  
    this.MailService.SendUploadArtworkEmail(Template,Servername,ExtraUrl,OrderNo, this.selectedSite, varExistingFileID,PMSCOLORS, ArtworkID).subscribe(
      (orderResponse) => {
        console.log('Order created successfully:', orderResponse);
      });
  }

  uploadFile(): void {
    debugger;
    const fileInput = document.getElementById('uploadNewFile') as HTMLInputElement;
    const file = fileInput.files?.[0];
    if (file) {
      const fileName = file.name;
      const fileExtension = fileName.split('.').pop()?.toLowerCase();
      if (fileExtension === 'ai' || fileExtension === 'eps') {
        // File type is supported, proceed with form submission or other action
        this.artworks.fileName = fileName;
        this.artworks.userId = this.UserID;
        this.artworks.dateAdded = this.datePipe.transform(this.currentDate, 'yyyy-MM-dd HH:mm:ss'); 

        const pmsColors = this.artworks.pmsColors;

        this.artworkService.uploadArtworkFile(this.orderID, this.UserID, pmsColors, file).subscribe(
          (fileResponse) => { console.log('file insrted in backend success') },
          (error) => { console.log('unable to insert file in the backend', error) }

        )
        
        this.uploadArtwork();
        
        this.showSuccessAlert = true;
        this.showErrorAlert = false;
        this.showFileSelectAlert = false;
      } else {
        
        this.showErrorAlert = true;
        this.showSuccessAlert = false;
        this.showFileSelectAlert = false;
      }
    } else {
      this.showSuccessAlert = false;
      this.showErrorAlert = false;
      this.showFileSelectAlert = true;
    }
  }

  getIPInfo() {
    this.http.get<any>('https://ipinfo.io').subscribe(
      (response) => {
        this.userIPAdress = response.ip;
      },
      (error) => {
        console.log('Error fetching IP info', error);
      }
    );
  }

  getSelectedOrderSite() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getSelectedOrderSite(orderId).subscribe(
      (response) => {
        console.log(response[0].SiteName);
        this.selectedSite = response[0].SiteName;
      },
      (error) => {
        console.log('error fetching order details', error);
      }
    )
  }

  getOrderDetails() {
    this.ordersService.getOrderDetails(this.orderID).subscribe(
      (res) => { 
        // console.log(res);
        this.artworkOrderName = res[0].CTOrderNumber;
      },
      (error) => {
        console.log(error);
      }
    )
  }

}
