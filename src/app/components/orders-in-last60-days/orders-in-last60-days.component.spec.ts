import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrdersInLast60DaysComponent } from './orders-in-last60-days.component';

describe('OrdersInLast60DaysComponent', () => {
  let component: OrdersInLast60DaysComponent;
  let fixture: ComponentFixture<OrdersInLast60DaysComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrdersInLast60DaysComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OrdersInLast60DaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
