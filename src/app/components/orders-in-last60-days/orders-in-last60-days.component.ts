import { Component } from '@angular/core';
import { OrdersService } from 'src/app/services/orders.service';
import { Chart, registerables } from 'chart.js';
import { Location } from '@angular/common';

Chart.register(...registerables);

@Component({
  selector: 'app-orders-in-last60-days',
  templateUrl: './orders-in-last60-days.component.html',
  styleUrls: ['./orders-in-last60-days.component.css']
})
export class OrdersInLast60DaysComponent {

  ordersInLast60DaysData: { OrderDate: string, OrderCount: number }[] = [];

  UserID!: any;

  noDataAvailable = false;
  noDataMessage = 'No data is available';

  constructor( private ordersService: OrdersService, private _location: Location ) {
    this.UserID = sessionStorage.getItem("UserID") ?? '';
   }

  goBack() {
    this._location.back();
  }

  ngOnInit(): void {
    this.getOrdersInLast60Days();
  }

  getOrdersInLast60Days() {
    this.ordersService.getOrdersInLast60Days(this.UserID).subscribe(
      (response) => {
        if (response && response.length > 0) {
          this.ordersInLast60DaysData = response;
          this.noDataAvailable = false;
          this.renderChart();
        } else {
          console.log('No data available.');
          this.ordersInLast60DaysData = []; 
          this.noDataAvailable = true; 
          this.noDataMessage = 'No orders found in the last 60 days.';
        }
      },
      (error) => {
        console.log(error);
      }
    )
  }

  renderChart() {
    // const ctx = document.getElementById('piechart');
    const dates = this.ordersInLast60DaysData.map(item => {
      const date = new Date(item.OrderDate);
      // Format the date as desired, for example: "Mar 05, 2024"
      return `${date.toLocaleString('default', { month: 'short' })} ${date.getDate()}, ${date.getFullYear()}`;
    });
    const orderCounts = this.ordersInLast60DaysData.map(item => item.OrderCount);

    const myChart = new Chart('barchart', {
      type: 'bar',
      data: {
        labels: dates, // Convert month number to month name
        datasets: [{
          label: 'Orders in last 60 Days',
          data: orderCounts,
          borderWidth: 1,
          // backgroundColor: '#005EB8',
        }]
      },
      options: {
        scales: {
          x: {
            ticks: {
              font: {
                size: 10
              }
            }
          },
          y: {
            beginAtZero: true
          }
        }
      }
    });
  }

}
