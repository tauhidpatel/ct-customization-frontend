import { Component, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrdersService } from 'src/app/services/orders.service';
declare var $: any;
declare var bootstrap: any;

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent {

  Users: any[] = [];

  constructor(private ordersService: OrdersService,private router:Router, private route: ActivatedRoute, private elementRef: ElementRef) { }

  private initDataTables(): void {
    $(document).ready(function () {
      $('#example').DataTable({
        // paging: false,
        // searching: false,
        // info: false
        columnDefs: [ { type: 'date', 'targets': [0] } ],
        order: [[ 0, 'desc' ]]
      });
    });
  }

  EditUser(UserId:any) {
    debugger;
    this.router.navigate([ '/createUser', UserId ]); 
  }

  ngOnInit() {
    this.ordersService.getAllUsers().subscribe((data: any) => {
      console.log(data);
      this.Users = data;

      // Extract UserID for each user and fetch security level
      

      // Sort orders array by order date in descending order
      this.Users.sort((a, b) => new Date(b.CTOrderDate).getTime() - new Date(a.CTOrderDate).getTime());

      this.ordersService.totalOrders = this.Users.length;

      this.initDataTables();
    });
  }

  getSecurityLevelByUserId(userId: number) {
    this.ordersService.getSecurityLevelByUserId(userId).subscribe(
      (res: any[]) => { 
        const roles = res.map(item => item.Role); 
        console.log(roles); // Log all 'Role' values
        const userIndex = this.Users.findIndex(user => user.UserID === userId);
        console.log(userIndex);
        if (userIndex !== -1) {
          this.Users[userIndex].Roles = roles;
          console.log(this.Users[userIndex].Roles)
        }
      }
      )
  };


}

  

