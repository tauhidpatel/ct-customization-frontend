import { Component } from '@angular/core';
import { OrdersService } from 'src/app/services/orders.service';
import { Chart, registerables } from 'chart.js';
import { Location } from '@angular/common';

Chart.register(...registerables);

@Component({
  selector: 'app-annually-orders',
  templateUrl: './annually-orders.component.html',
  styleUrls: ['./annually-orders.component.css']
})
export class AnnuallyOrdersComponent {

  annuallyOrderData: { Year: number, OrderCount: number }[] = [];

  UserID: any

  constructor( private ordersService: OrdersService, private _location: Location  ) {
    this.UserID = sessionStorage.getItem("UserID") ?? '';
   }

  goBack() {
    this._location.back();
  } 

  ngOnInit(): void {
    this.getAnnuallyOrderDetails();
  }

  getAnnuallyOrderDetails() {
    this.ordersService.getAnnuallyOrderDetails(this.UserID).subscribe(
      (response) => {
        // console.log(response);
        this.annuallyOrderData = response;
        this.renderChart();
      },
      (error) => {
        console.log(error);
      }
    )
  }

  renderChart() {
    const years = this.annuallyOrderData.filter(item => item.Year !== null).map(item => item.Year);
    const orderCounts = this.annuallyOrderData.map(item => item.OrderCount);

    const myChart = new Chart('annuallyChart', {
      type: 'bar',
      data: {
        labels: years.map(year => year.toString()), // Use years directly as labels
        datasets: [{
          label: 'Orders By Year',
          data: orderCounts,
          borderWidth: 1,
          // backgroundColor: '#005EB8',
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });
  }


}
