import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnuallyOrdersComponent } from './annually-orders.component';

describe('AnnuallyOrdersComponent', () => {
  let component: AnnuallyOrdersComponent;
  let fixture: ComponentFixture<AnnuallyOrdersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnnuallyOrdersComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AnnuallyOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
