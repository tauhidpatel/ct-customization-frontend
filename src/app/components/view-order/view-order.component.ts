import { DatePipe, Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArtworkService } from 'src/app/services/artwork.service';
import { OrdersService } from 'src/app/services/orders.service';
import { ProofService } from 'src/app/services/proof.service';

@Component({
  selector: 'app-view-order',
  templateUrl: './view-order.component.html',
  styleUrls: ['./view-order.component.css']
})
export class ViewOrderComponent {

  latestArtwork: any;

  orderDetails: any;
  orderProductDetails: any[] = [];
  selectedSite: any;
  orderStatus: any;


  selectedCustomer: any[] = [];
  LogDetails: any[] = [];

  constructor(
    private ordersService: OrdersService, 
    private artworkService: ArtworkService,
    private proofService: ProofService,
    private route: ActivatedRoute,
    private router: Router,
    private datePipe: DatePipe,
    private http: HttpClient,
    private _location: Location  
  ) { }

  goBack() {
    this._location.back();
    console.log( 'goBack()...' );
  }

  ngOnInit(): void {

    this.getOrderDetails();
    this.getSelectedOrderSite();
    this.getCurrentOrderStatus();
    this.getOrderDetailsByOrderID();
    this.getFullNameAndEmailAddressByOrderID();
    this.getNewOrderLog();
    this.getIPInfo();

  }

  formatDate(date: string): string {
    return this.datePipe.transform(date, 'yyyy-MM-dd') ?? '';
  }

  getOrderDetails() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getOrderDetails(orderId).subscribe(
      (response) => {
        console.log(response[0].RepeatOrderComment);
        this.orderDetails = response;
      },
      (error) => {
        console.log('error fetching order details', error);
      }
    )
  }

  getSelectedOrderSite() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getSelectedOrderSite(orderId).subscribe(
      (response) => {
        this.selectedSite = response;
      },
      (error) => {
        console.log('error fetching order details', error);
      }
    )
  }

  getCurrentOrderStatus() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getCurrentOrderStatus(orderId).subscribe(
      (response) => {
        this.orderStatus = response;
      },
      (error) => {
        console.log('erorrrrr', error);
      }
    )
  }
  
  getOrderDetailsByOrderID() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getGetOrderDetailsByOrderID(orderId).subscribe(
      (res: any[]) => {
          console.log(res);
          res.sort((a, b) => a.ProductCode - b.ProductCode); // Sort products by ProductCode
          this.orderProductDetails = res;
      },
      (error) => {
          console.error('Error fetching order details:', error);
      }
    );
  
    // this.ordersService.getGetOrderDetailsByOrderID(orderId).subscribe(
    //   (res: any[]) => {
    //     console.log(res);
    //     res.sort((a, b) => a.ProductCode - b.ProductCode);

    //     for (let i = 0; i < Math.min(res.length, 3); i++) {
    //       const product = res[i];
    //       if (i === 0) {
    //         this.orderProductsOne = product.ProductCode;
    //         this.orderDescriptionOne = product.ProductDescription;
    //         this.orderQuantityOne = product.ProductQty;
    //       } else if (i === 1) {
    //         this.orderProductsTwo = product.ProductCode;
    //         this.orderDescriptionTwo = product.ProductDescription;
    //         this.orderQuantityTwo = product.ProductQty;
    //       } else if (i === 2) {
    //         this.orderProductsThree = product.ProductCode;
    //         this.orderDescriptionThree = product.ProductDescription;
    //         this.orderQuantityThree = product.ProductQty;
    //       } else if ( i === 3 ) {
    //         this.orderProductsFour = product.ProductCode;
    //         this.orderDescriptionFour = product.ProductDescription;
    //         this.orderQuantityFour = product.ProductQty;
    //       } else if ( i === 4 ) {
    //         this.orderProductsFive = product.ProductCode;
    //         this.orderDescriptionFive = product.ProductDescription;
    //         this.orderQuantityFive = product.ProductQty;
    //       } else if ( i === 5 ) {
    //         this.orderProductsSix = product.ProductCode;
    //         this.orderDescriptionSix = product.ProductDescription;
    //         this.orderQuantitySix = product.ProductQty;
    //       }
    //     }
      // },
      // (error) => {
      //   console.log(error);
      // }
  }

  getFullNameAndEmailAddressByOrderID() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getFullNameAndEmailAddressByOrderID(orderId).subscribe(
      (response: any[]) => {
        console.log(response);
        this.selectedCustomer = response;
      },
      (error) => {
        console.log('errrrrorrrrr', error);
      }
    )
  }

  redirectToUploadArtwork(OrderID: string) {
    // Navigate to '/uploadArtwork' route with parameters
    this.router.navigate([ '/uploadArtwork', OrderID ]);
  }

  getNewOrderLog() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getNewOrderLog(orderId).subscribe(
      (logResponse) => {
        console.log('logggggggg: ', logResponse);
        this.LogDetails = logResponse;
      },
      (error) => {
        console.log('no details for new order', error);
      }
    )
  }

  getIPInfo() {
    this.http.get<any>('https://ipinfo.io').subscribe(
      (response) => {
        console.log(response);
        console.log(response.ip);
      },
      (error) => {
        console.log('Error fetching IP info', error);
      }
    );
  }

  downloadArtwork(OrderID: string) {
    debugger;
    this.artworkService.getArtworkToDownload(OrderID).subscribe(
      (res) => {
        const artworkDownloadOrderID = res[0].OrderID; 
        const artworkDownloadFileName = res[0].FileName;
  
        this.artworkService.downloadArtworkFile(artworkDownloadOrderID, artworkDownloadFileName)
          .subscribe(
            (fileRes) => {
              const blob = new Blob([fileRes], { type: 'application/octet-stream' });
              const url = window.URL.createObjectURL(blob);
              
              const link = document.createElement('a');
              link.href = url;
              link.download = artworkDownloadFileName; 
              document.body.appendChild(link);
              
              link.click();
              
              window.URL.revokeObjectURL(url);
              document.body.removeChild(link);
              
              console.log("File downloaded successfully");
            },
            (error) => {
              console.log("Failed to download file", error);
            }
          );
      },
      (error) => {
        console.log('Failed to get artwork details', error);
      }
    );
  }

  downloadProof(OrderID: string) {
    debugger;
    this.proofService.getProofToDownload(OrderID).subscribe(
      (res) => {
        const proofDownloadOrderID = res[0].OrderID; 
        const proofDownloadFileName = res[0].FileName;
  
        this.proofService.downloadProofFile(proofDownloadOrderID, proofDownloadFileName)
          .subscribe(
            (fileRes) => {
              const blob = new Blob([fileRes], { type: 'application/octet-stream' });
              const url = window.URL.createObjectURL(blob);
              
              const link = document.createElement('a');
              link.href = url;
              link.download = proofDownloadFileName; 
              document.body.appendChild(link);
              
              link.click();
              
              window.URL.revokeObjectURL(url);
              document.body.removeChild(link);
              
              console.log("File downloaded successfully");
            },
            (error) => {
              console.log("Failed to download file", error);
            }
          );
      },
      (error) => {
        console.log('Failed to get artwork details', error);
      }
    );
  }
  
  
}
