import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { OrdersService } from 'src/app/services/orders.service';
declare var $: any;

@Component({
  selector: 'app-user-orders-activity',
  templateUrl: './user-orders-activity.component.html',
  styleUrls: ['./user-orders-activity.component.css']
})
export class UserOrdersActivityComponent {

  LogDetails: any[] = [];

  UserID: any;

  constructor( private ordersService: OrdersService, private router: Router, private _location: Location ) {
    this.UserID = sessionStorage.getItem("UserID") ?? '';
   }

  goBack() {
    this._location.back();
  }

  ngAfterViewInit(): void {
    this.getAccountChangeHistory();
  }

  getAccountChangeHistory() {
    this.ordersService.getOrdersHistorywithOrderID(this.UserID).subscribe(
      (response) => {
        console.log(response);
        this.LogDetails = response;
        this.initDataTables();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  navigateToViewOrder(OrderID: string) {
      this.router.navigate(['/viewOrder', OrderID]);
  }

  private initDataTables(): void {
    $(document).ready(function () {
      $('#ordersDataTable').DataTable({
        columnDefs: [{ type: 'date', targets: [0] }],
        order: [[0, 'desc']]
      });
    });
  }

}
