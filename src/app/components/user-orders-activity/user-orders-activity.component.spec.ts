import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserOrdersActivityComponent } from './user-orders-activity.component';

describe('UserOrdersActivityComponent', () => {
  let component: UserOrdersActivityComponent;
  let fixture: ComponentFixture<UserOrdersActivityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserOrdersActivityComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserOrdersActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
