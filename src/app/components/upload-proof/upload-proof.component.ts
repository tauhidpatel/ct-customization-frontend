import { DatePipe, Location } from '@angular/common';
import { Component } from '@angular/core';
import { ProofService } from 'src/app/services/proof.service';
import { MailService } from 'src/app/services/mail.service';
import { ActivatedRoute } from '@angular/router';
import { OrdersService } from 'src/app/services/orders.service';
import { HttpClient } from '@angular/common/http';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-upload-proof',
  templateUrl: './upload-proof.component.html',
  styleUrls: ['./upload-proof.component.css']
})
export class UploadProofComponent {

  orderID!: string; // ORDERID FROM THE URL PARAMS
  UserID!: string; // of the one who is logged in

  FirstName!: string;
  LastName!: string;
  EmailAddress: any;

  selectedSite: any;

  proofOrderName!: string;

  userIPAdress!: string;
  browserName!: string;

  currentDate = new Date();

  showErrorAlert: boolean = false;
  showSuccessAlert: boolean = false;
  showFileSelectAlert: boolean = false;

  proofs: any = {
    orderId: "",
    ProofID: '',
    userId: '',
    fileName: '',
    origFileName: '',
    dateAdded: '',
    customerComments: '',
    customerStatus: '',
    pmsColors: '',
  }

  constructor(
    private proofService: ProofService, 
    private ordersService: OrdersService, 
    private datePipe: DatePipe,
    private MailService :MailService, 
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    private _location: Location
  ) { 
    this.UserID = sessionStorage.getItem("UserID") ?? '';
    this.FirstName = sessionStorage.getItem("FirstName") ?? '';
    this.LastName = sessionStorage.getItem("LastName") ?? '';
    this.EmailAddress = sessionStorage.getItem("EmailAddress") ?? '';
  }

  goBack() {
    this._location.back();
  }

  ngOnInit(): void {
    this.orderID = this.route.snapshot.params['OrderID'];
    this.getIPInfo();
    this.getOrderDetails();
    this.getSelectedOrderSite();
  } 

  uploadProof(): void {
    debugger;
    this.proofs.orderId = this.orderID;
    this.proofService.uploadProof(this.proofs).subscribe(
      (proofResponse) => {
        console.log("Proof uploaded successfully!!");
        console.log('Proof Details', proofResponse);
        console.log('Proof ID', proofResponse.ProofID);
        console.log('PROOF OBJECT', this.proofs);

        debugger;

        this.browserName = (navigator as any).userAgentData.brands[0].brand
        const logData = {
          UserID: this.UserID,
          IPAddress: this.userIPAdress, 
          OrderID: this.orderID,
          ActionType: 'Upload Proof',
          ActionDescription: `Proof was uploaded by ${this.EmailAddress}`,
          BrowserInfo: this.browserName, 
          Comments: '---', 
          Status: '' 
        };
      
        // Call your service method to insert log
        this.ordersService.insertLog(logData).subscribe(
          (response) => {
            console.log('Log inserted successfully:', response);
          },
          (error) => {
            console.error('Error inserting log:', error);
          }
        );

        this.ordersService.updateOrderStatus(this.orderID, 2).subscribe(
          (res) => { 
            console.log('order status updated successfully',res);
          },
          (error) => {
            console.log('unable to update order status', error);
          }
        );

        // Redirect to '/allOrders' upon successful order creation
        const navigationExtras: NavigationExtras = {
          queryParams: { successProof: 'true' },
          fragment: 'top' // Scroll to top when navigating
        };
        this.router.navigate(['/allOrders'], navigationExtras);

        // mail trigger
      },
      (proofError) => {
        console.log("Unable to upload Proof Data", proofError);
      }
    )
  } 

  uploadFile(): void {
    debugger;
    const fileInput = document.getElementById('uploadNewFile') as HTMLInputElement;
    console.log(fileInput);
    const file = fileInput.files?.[0];
    if (file) {
      const fileName = file.name;
      const fileExtension = fileName.split('.').pop()?.toLowerCase();
      if ( fileExtension === 'pdf' ) {
        // File type is supported, proceed with form submission or other action
        this.proofs.fileName = fileName;
        this.proofs.userId = this.UserID;
        this.proofs.dateAdded = this.datePipe.transform(this.currentDate, 'yyyy-MM-dd HH:mm:ss'); 

        this.proofService.uploadProofFile(this.orderID, this.UserID, file).subscribe(
          (fileResponse) => { console.log('file insrted in backend success', fileResponse) },
          (error) => { console.log('unable to insert file in the backend', error) }
        );

        // For example, you can submit the form here
        this.uploadProof();

        const encodedFirstName = encodeURIComponent(this.FirstName);
        const encodedLastName = encodeURIComponent(this.LastName);
        const encodedFullName = `${encodedFirstName}${encodedLastName}`;
        
        this.SendUploadProofEmail(this.proofOrderName, this.selectedSite, this.UserID, encodedFullName);
        
        this.showSuccessAlert = true;
        this.showErrorAlert = false;
        this.showFileSelectAlert = false;
      } else {
        // alert('Unsupported file type. Please upload a file with .ai or .eps extension.');
        this.showErrorAlert = true;
        this.showSuccessAlert = false;
        this.showFileSelectAlert = false;
      }
    } else {
      this.showSuccessAlert = false;
      this.showErrorAlert = false;
      this.showFileSelectAlert = true;
    }
  }

  getSelectedOrderSite() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getSelectedOrderSite(orderId).subscribe(
      (response) => {
        this.selectedSite = response[0].SiteName;
      },
      (error) => {
        console.log('error fetching order details', error);
      }
    )
  }

  getIPInfo() {
    this.http.get<any>('https://ipinfo.io').subscribe(
      (response) => {
        this.userIPAdress = response.ip;
      },
      (error) => {
        console.log('Error fetching IP info', error);
      }
    );
  }

  getOrderDetails() {
    this.ordersService.getOrderDetails(this.orderID).subscribe(
      (res) => { 
        // console.log(res[0].CTOrderNumber);
        this.proofOrderName = res[0].CTOrderNumber;
      },
      (error) => {
        console.log(error);
      }
    )
  }

  SendUploadProofEmail(OrderNo: any,Siteid:any,varUserId:any,Fullname:any)
  {
    debugger;
    const Template="ProofReadyForReview";
    // const Servername="http:%2F%2Flocalhost:4200%2F";
    // const ExtraUrl="null";

    const Servername = encodeURIComponent("https://digital-team-805wb.certainteed.com");
    const ExtraUrl = encodeURIComponent("Productcustomization");
   
  
    this.MailService.SendUploadProofEmail(Template, Servername,ExtraUrl, this.proofOrderName, this.selectedSite, varUserId, Fullname).subscribe(
      (orderResponse) => {
        console.log('Order created successfully:', orderResponse);
      });
  }
}
