import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { OrdersService } from 'src/app/services/orders.service';
import { SsoService } from 'src/app/services/sso.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent {

  UserID!: any; // getting this from session storage
  showUpdateAlert: boolean = false;

  userName!:string;
  userIPAdress!: string;
  browserName!: string;

  user: any = {
    Userid: "",
    FirstName: "",
    LastName: "",
    CompanyName: "",
    Address: "",
    City: "",
    State: "",
    Country: "",
    ZipCode: "",
    Phone: "",

    // not passed
    DateCreated: '',
    DateUpdated: '',
    EmailAddress: '',
    SGID: '',
    SecurityLevel: '',
    Status: '',
    oktauserid: ''

  }
  
  constructor(
    private http: HttpClient,
    private ordersService: OrdersService,
    private _location: Location 
  ) {
    this.UserID = sessionStorage.getItem("UserID") ?? '';
  }

  goBack() {
    this._location.back();
  }

  ngOnInit(): void {
    // console.log(this.UserID);
    this.ordersService.getUserInfoByUserID(this.UserID).subscribe(
      (response) => {
        console.log(response);
        this.user.FirstName = response[0].FirstName;
        this.user.LastName = response[0].LastName;
        this.user.CompanyName = response[0].CompanyName;
        this.user.Address = response[0].Address;
        this.user.City = response[0].City;
        this.user.State = response[0].State;
        this.user.Country = response[0].Country;
        this.user.ZipCode = response[0].ZipCode;
        this.user.Phone = response[0].Phone;

        this.userName = this.user.FirstName;
      },
      (error) => {
        console.log('errrrrr', error);
      }
    );

    this.getIPInfo();
  }

  updateUser() {
    debugger;
    console.log('User to be updated:', this.user);
    this.user.Userid= this.UserID;
    
    this.ordersService.updateUserInfoByUserID(this.user).subscribe(
      (response) => {
        console.log('User updated successfully:', response);
        this.showUpdateAlert = true;

        debugger;

        this.browserName = (navigator as any).userAgentData.brands[0].brand;

        const logData = {
          UserID: this.UserID,
          IPAddress: this.userIPAdress, // retrieve the IP address if needed
          OrderID: '',
          ActionType: 'Profile Updated',
          ActionDescription: `profile was updated by ${this.userName}`,
          BrowserInfo: this.browserName, // You may retrieve browser info if needed
          Comments: '---', // User comments for new order
          Status: '' // You may add status if needed
        };
      
        // Call your service method to insert log
        this.ordersService.insertLog(logData).subscribe(
          (response) => {
            console.log('Log inserted successfully:', response);
          },
          (error) => {
            console.error('Error inserting log:', error);
          }
        );
      },
      (error) => {
        console.error('Error creating user:', error);
      }
    );
  }

  getIPInfo() {
    this.http.get<any>('https://ipinfo.io').subscribe(
      (response) => {
        // console.log(response);
        console.log(response.ip);
        this.userIPAdress = response.ip;
      },
      (error) => {
        console.log('Error fetching IP info', error);
      }
    );
  }
  
}
