import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { OrdersService } from 'src/app/services/orders.service';
import { Location } from '@angular/common';
declare var $: any;

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent {

  showSuperRoles: boolean = true;
  Name!: string;
  SecurityLevel!: string;
  Country!: string;
  SiteName!: string;
  UserID!: string;
  

  orders: any[] = [];
  allOrders: any[] = [];
  proccessedOrders: any[] = [];
  totalOrders: number = 0;
  completedOrders: number = 0;
  proccessedCount: number = 0;

  quickAction: any[] = [];
  quickActionEndUser: any[] = [];

  supportLanguages = ['en', 'fr', 'es'];
  
  // SiteName: string;

  constructor(
    private translateService: TranslateService, 
    private ordersService: OrdersService,
    private http:HttpClient,
    private router: Router,
    private _location: Location  
  ) {
    
    this.SecurityLevel = sessionStorage.getItem("SecurityLevel") ?? '';
    this.UserID = sessionStorage.getItem("UserID") ?? '';
    this.Country = sessionStorage.getItem("Country") ?? '';
    this.SiteName = sessionStorage.getItem("SiteName") ?? ''

    

    this.translateService.addLangs(this.supportLanguages);
    this.translateService.setDefaultLang('en')
  }

  goBack() {
    this._location.back();
  }

  ngOnInit(): void {
    this.ordersService.getAllOrders(this.SecurityLevel, this.UserID, this.Country , this.SiteName).subscribe((data: any) => {
      // console.log(data);
      this.orders = data;
      this.totalOrders = this.orders.length;
      console.log(this.orders);
debugger;
     // const StatusIds = this.orders.map(order => order.StatusId);
     // const proxyIdsWithStatusFive = StatusIds.filter(id => id === 5);
      var completedOrders= this.orders.filter((e: { StatusId: number; }) => e.StatusId == 5);
      this.completedOrders = completedOrders.length;
      var proccessedCount = this.orders.filter((e: { StatusId: number; }) => e.StatusId != 5);
      this.proccessedCount = proccessedCount.length;
      // Fetch completed orders only if there are orders with status 5
      // if (proxyIdsWithStatusFive.length > 0) {
      //   this.ordersService.getCompletedOrders(proxyIdsWithStatusFive[0]).subscribe(
      //     (completedOrders) => {
      //       this.completedOrders = completedOrders.length;
      //       // console.log(completedOrders.length);
      //       // const completeCount = this.completedOrders.length;
      //     }
      // )}

      // const proxyIdsWithStatusFour = StatusIds.filter(id => id === 4);
      // if (proxyIdsWithStatusFour.length > 0) {
      //   this.ordersService.getCompletedOrders(proxyIdsWithStatusFour[0]).subscribe(
      //     (completedOrders) => {
      //       this.proccessedCount = completedOrders.length;
      //       // console.log(completedOrders.length);
      //       // const completeCount = this.completedOrders.length;
      //     }
      // )}

    });

    // this.GetQuickActionOrders();
  }
 
  ngAfterViewInit(): void {
  //   if(this.SecurityLevel=="3")
  //     {
  //   this.GetQuickActionOrders();
  //     }
  // else  if(this.SecurityLevel=="4")
  //     {
  //   this.GetQuickActionEndUserOrders();
  //     }
  //     else
  //     {
        this.GetQuickActionOrders();
        this.GetQuickActionEndUserOrders();
      // }
      
  }

  GetQuickActionOrders() {
    // this.ordersService.getQuickActionOrders(this.SiteName).subscribe(
    //   (res) => {
    //     this.initDataTables();
    //   },
    //   (error) => {
    //     console.log(error);
    //   }
    // );
    this.ordersService.getAllOrders( this.SecurityLevel, this.UserID, this.Country, this.SiteName ).subscribe(
      (data: any) => {
        console.log(data);
        this.quickAction  = data;

      // Sort orders array by order date in descending order
      this.orders.sort((a, b) => new Date(b.CTOrderDate).getTime() - new Date(a.CTOrderDate).getTime());

      this.initDataTables();
    },
      (error) => {
        console.log(error);
    });
  }

  GetQuickActionEndUserOrders() {
    this.ordersService.getQuickActionEndUserOrders(this.UserID).subscribe(
      (res) => {
        console.log(res);
        this.quickActionEndUser = res
        this.initDataTables();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  initDataTables(): void {
    $(document).ready(function () {
      $('#example').DataTable();
    });
  }

  // Navigate to the upload artwork page with the order ID
  uploadArtwork(OrderID: string) {
    this.router.navigate([ '/uploadArtwork', OrderID ]);
  }

  navigateToreviewProof(OrderID: string,ProofID:string) {
    this.router.navigate([ '/reviewProof', OrderID,ProofID ])
  }

  goToOrder(OrderID: string) {
    this.router.navigate(['/allOrders', OrderID]);
  }

}
