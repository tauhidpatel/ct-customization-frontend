import { Component } from '@angular/core';
import { OrdersService } from '../../services/orders.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ArtworkService } from '../../services/artwork.service';
import { ProofService } from '../../services/proof.service';

@Component({
  selector: 'app-print-order',
  templateUrl: './print-order.component.html',
  styleUrls: ['./print-order.component.css']
})
export class PrintOrderComponent {
  UserID: string;
  orderID: any;
  orders: any[] = [];
  OrderDetails: any[] = [];
  LogDetails: any;
  currentDate:string | undefined;
  currentTime:string | undefined;
  year!: string;
  artworkURL!: string;
proofURL!: string;
orderStatus: any;

  constructor(
    private ordersService: OrdersService, 
    private artworkService: ArtworkService, 
    private proofService: ProofService,
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient
  ) { 
    this.UserID = sessionStorage.getItem("UserID") ?? '';
  }

  ngOnInit(): void {
    $(".sticky-header").css("display","none");
    $(".footer").css("display","none");

    this.getCurrentOrderStatus();

    var date = new Date();
 this.currentDate = date.toISOString().slice(0,10);
 this.currentTime = date.getHours() + ':' + date.getMinutes();
  this.year = date.getFullYear().toString();

    this.orderID = this.route.snapshot.params['OrderID'];
    this.ordersService.getOrderDetails(this.orderID).subscribe(
      (orderDetails: any) => {
        console.log(orderDetails);
        this.orders=orderDetails;
        if(this.orders[0].ArtworkID=="" || this.orders[0].ArtworkID==null)
          {
            this.artworkURL="(Pending upload by client)"
          }
          else
          {
            this.downloadArtwork(this.orderID)
          }
          if(this.orders[0].ProofID=="" || this.orders[0].ProofID==null)
            {
              this.proofURL="(Pending upload by vendor)";
            }
            else
            {
              this.downloadProof(this.orderID);
            }
        this.ordersService.getGetOrderDetailsByOrderID(this.orderID).subscribe(
          (res: any[]) => {
            console.log(res);
            this.OrderDetails=res;
            this.ordersService.getNewOrderLog(this.orderID).subscribe(
              (logResponse) => {
                console.log('logggggggg: ', logResponse);
                this.LogDetails = logResponse;
           
                window.setTimeout(() => window.print(), 1000);
              },
              (error) => {
                console.log('no details for new order', error);
              }
            )
          });
      });

  } 

  downloadArtwork(OrderID: string) {
    debugger;
    this.artworkService.getArtworkToDownload(OrderID).subscribe(
      (res) => {
        const artworkDownloadOrderID = res[0].OrderID; 
        const artworkDownloadFileName = res[0].FileName;
  
        this.artworkService.downloadArtworkFile(artworkDownloadOrderID, artworkDownloadFileName)
          .subscribe(
            (fileRes) => {
              const blob = new Blob([fileRes], { type: 'application/octet-stream' });
              const url = window.URL.createObjectURL(blob);
              
              const link = document.createElement('a');
              link.href = url;
              link.download = artworkDownloadFileName; 
           
              this.artworkURL=link.toString();
             
              
              console.log("File downloaded successfully");
            },
            (error) => {
              console.log("Failed to download file", error);
            }
          );
      },
      (error) => {
        console.log('Failed to get artwork details', error);
      }
    );
  }
  downloadProof(OrderID: string) {
    debugger;
    this.proofService.getProofToDownload(OrderID).subscribe(
      (res) => {
        const proofDownloadOrderID = res[0].OrderID; 
        const proofDownloadFileName = res[0].FileName;
  
        this.proofService.downloadProofFile(proofDownloadOrderID, proofDownloadFileName)
          .subscribe(
            (fileRes) => {
              const blob = new Blob([fileRes], { type: 'application/octet-stream' });
              const url = window.URL.createObjectURL(blob);
              
              const link = document.createElement('a');
              link.href = url;
              link.download = proofDownloadFileName; 
             
              this.proofURL=link.toString();
            
              
              console.log("File downloaded successfully");
            },
            (error) => {
              console.log("Failed to download file", error);
            }
          );
      },
      (error) => {
        console.log('Failed to get artwork details', error);
      }
    );
  }

  getCurrentOrderStatus() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getCurrentOrderStatus(orderId).subscribe(
      (response) => {
        this.orderStatus = response;
      },
      (error) => {
        console.log('erorrrrr', error);
      }
    )
  }
  

}
