import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ArtworkService } from 'src/app/services/artwork.service';
import { OrdersService } from 'src/app/services/orders.service';
import { ProofService } from 'src/app/services/proof.service';
declare var $: any;
declare var bootstrap: any;

@Component({
  selector: 'app-processed-orders',
  templateUrl: './processed-orders.component.html',
  styleUrls: ['./processed-orders.component.css']
})
export class ProcessedOrdersComponent {

  allOrders: any[] = [];
  completedOrders: any[] = [];
  UserID!: string;
  SecurityLevel!: string;
  SiteName!: string;
  Country!: string;

  constructor(
    private ordersService: OrdersService,
    private artworkService: ArtworkService,
    private proofService: ProofService,
    private router: Router
  ) 
  {  
    this.SecurityLevel = sessionStorage.getItem("SecurityLevel") ?? '';
    this.UserID = sessionStorage.getItem("UserID") ?? '';
    this.Country = sessionStorage.getItem("Country") ?? '';
    this.SiteName = sessionStorage.getItem("SiteName") ?? ''
}

  ngOnInit(): void {
    this.fetchCompletedOrders();
  }

  fetchCompletedOrders() {

    this.ordersService.getAllOrders(this.SecurityLevel,this.UserID, this.Country ,this.SiteName).subscribe(
      (res) => { 
        console.log(res);
        this.allOrders = res;
        this.completedOrders = this.allOrders.filter((e: { StatusId: number; }) => e.StatusId != 5);
        if (this.completedOrders.length > 0) {
          this.initDataTables();
        }
        else {
          console.log('No orders with status 5 found.');
        }
  
      },
      (error) => { console.log(error) }
    );

  }
  

  ngAfterViewInit(): void {
    const tooltips = document.querySelectorAll('.tt');
    tooltips.forEach(t => {
      new bootstrap.Tooltip(t);
    });
  }

  private initDataTables(): void {
    $(document).ready(function () {
      $('#example').DataTable({
        // paging: false,
        // searching: false,
        // info: false
        columnDefs: [ { type: 'date', 'targets': [0] } ],
        order: [[ 0, 'desc' ]]
      });
    });
  }

  

  // Navigate to view order with the OrderID
  navigateToViewOrder(OrderID: string) {
    this.router.navigate(['/viewOrder', OrderID]);
  }

  printOrder(OrderID: string) {
    // this.router.navigate(['/printOrder', OrderID]);
    const url = '/printOrder/' + OrderID;
    window.open(url, '_blank');
  }
  
  downloadArtwork(OrderID: string) {
    debugger;
    this.artworkService.getArtworkToDownload(OrderID).subscribe(
      (res) => {
        const artworkDownloadOrderID = res[0].OrderID; 
        const artworkDownloadFileName = res[0].FileName;
  
        this.artworkService.downloadArtworkFile(artworkDownloadOrderID, artworkDownloadFileName)
          .subscribe(
            (fileRes) => {
              const blob = new Blob([fileRes], { type: 'application/octet-stream' });
              const url = window.URL.createObjectURL(blob);
              
              const link = document.createElement('a');
              link.href = url;
              link.download = artworkDownloadFileName; 
              document.body.appendChild(link);
              
              link.click();
              
              window.URL.revokeObjectURL(url);
              document.body.removeChild(link);
              
              console.log("File downloaded successfully");
            },
            (error) => {
              console.log("Failed to download file", error);
            }
          );
      },
      (error) => {
        console.log('Failed to get artwork details', error);
      }
    );
  }

  downloadProof(OrderID: string) {
    debugger;
    this.proofService.getProofToDownload(OrderID).subscribe(
      (res) => {
        const proofDownloadOrderID = res[0].OrderID; 
        const proofDownloadFileName = res[0].FileName;
  
        this.proofService.downloadProofFile(proofDownloadOrderID, proofDownloadFileName)
          .subscribe(
            (fileRes) => {
              const blob = new Blob([fileRes], { type: 'application/octet-stream' });
              const url = window.URL.createObjectURL(blob);
              
              const link = document.createElement('a');
              link.href = url;
              link.download = proofDownloadFileName; 
              document.body.appendChild(link);
              
              link.click();
              
              window.URL.revokeObjectURL(url);
              document.body.removeChild(link);
              
              console.log("File downloaded successfully");
            },
            (error) => {
              console.log("Failed to download file", error);
            }
          );
      },
      (error) => {
        console.log('Failed to get artwork details', error);
      }
    );
  }

  // Navigate to edit order with the OrderID
  navigateToUpdateOrderStatus(OrderID: string) {
    this.router.navigate(['/updateOrderStatus', OrderID]);
  }

}
