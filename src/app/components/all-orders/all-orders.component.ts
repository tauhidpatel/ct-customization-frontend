import { Component, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ArtworkService } from 'src/app/services/artwork.service';
import { OrdersService } from 'src/app/services/orders.service';
import { ProofService } from 'src/app/services/proof.service';
import { Location } from '@angular/common';
declare var $: any;
declare var bootstrap: any;

@Component({
  selector: 'app-all-orders',
  templateUrl: './all-orders.component.html',
  styleUrls: ['./all-orders.component.css']
})
export class AllOrdersComponent {

  orderID: any; //FROM URL PARAMS
  showConfirmation: boolean = false;
  orderNumber!: string;
  orderIdToDelete: string = '';

  showArtworkUploadSuccessAlert: boolean = false;
  showProofUploadSuccessAlert: boolean = false;
  showSuccessAlert: boolean = false;
  showDeleteAlert: boolean = false;
  showArtworkRejectedByVendor: boolean = false;
  showProofRejectedByVendor: boolean = false;
  proofAccepted: boolean = false;
  showOrderCompleted: boolean = false;
  updateSuccess: boolean = false;
  artworkRejected: boolean = false;
  successAdminResetStatus: boolean = false;
  repeatOrderSuccess: boolean = false;

  artworkDownloadOrderID = '';
  artworkDownloadFileName = '';

  userId: any;
  SGID!: string;
  Name!: string;
  Country!: string;
  SiteName!:string;
  UserID!: string;
  orderStatus!: string;
  SecurityLevel !:any;
  orders: any[] = [];
  
  MultipleSites=false;

  constructor(
    private ordersService: OrdersService,
    private artworkService: ArtworkService, 
    private proofService: ProofService,
    private route: ActivatedRoute, 
    private elementRef: ElementRef, 
    private router: Router,
    private _location: Location 
  ) 
    {
      
    this.SecurityLevel = sessionStorage.getItem("SecurityLevel") ?? '';
    this.UserID = sessionStorage.getItem("UserID") ?? '';
    this.Country = sessionStorage.getItem("Country") ?? '';
    this.SiteName = sessionStorage.getItem("SiteName") ?? ''
  
  }

  goBack() {
    this._location.back();
  }

  isDownloadable(status: string): boolean {
    // List of statuses where the button should be visible
    const visibleStatuses = [
        'Artwork is being reviewed by vendor',
        'Proof is being reviewed by customer',
        'Order Being Processed',
        'Complete'
    ];
    return visibleStatuses.includes(status);
}


  ngOnInit() {
  
    this.orderID = this.route.snapshot.params['OrderID'];
    if( !this.orderID ) {
      this.ordersService.getAllOrders( this.SecurityLevel, this.UserID, this.Country, this.SiteName ).subscribe(
        (data: any) => {
        // console.log('all orders: ', data);
        this.orders = data;
  
        // Sort orders array by order date in descending order
        this.orders.sort((a, b) => new Date(b.CTOrderDate).getTime() - new Date(a.CTOrderDate).getTime());
  
        this.ordersService.totalOrders = this.orders.length;
  
        this.initDataTables();
      },
        (error) => {
          console.log(error);
      });
    } 
    else {
      this.ordersService.getOrderDetails(this.orderID).subscribe(
        (data: any) => {
          // console.log('all orders: ', data);
          this.orders = data;

          // Sort orders array by order date in descending order
        this.orders.sort((a, b) => new Date(b.CTOrderDate).getTime() - new Date(a.CTOrderDate).getTime());
  
        this.ordersService.totalOrders = this.orders.length;
  
        this.initDataTables();
        }, (error) => {
          console.log(error);
        });
    }
    

    this.route.queryParams.subscribe(params => {
      if (params['success'] === 'true') {
          this.showSuccessAlert = true;
          this.showArtworkUploadSuccessAlert = false;
          this.showProofUploadSuccessAlert = false;
          this.showArtworkRejectedByVendor = false;
          this.showProofRejectedByVendor = false;
          this.proofAccepted = false;
          this.showOrderCompleted = false;
          this.updateSuccess = false;
          this.successAdminResetStatus = false;
          // Scroll to the top of the page
          this.scrollToTop();
        //  window.location.reload();
      }
    });

    this.route.queryParams.subscribe(params => {
      if (params['successArtwork'] === 'true') {
          this.showArtworkUploadSuccessAlert = true;

          this.showSuccessAlert = false;
          this.showProofUploadSuccessAlert = false;
          this.showArtworkRejectedByVendor = false;
          this.showProofRejectedByVendor = false;
          this.proofAccepted = false;
          this.showOrderCompleted = false;
          this.updateSuccess = false;
          this.successAdminResetStatus = false;

          // Scroll to the top of the page
          this.scrollToTop();
      }
    });
    
    this.route.queryParams.subscribe(params => {
      if (params['successProof'] === 'true') {
          this.showProofUploadSuccessAlert = true;

          this.showSuccessAlert = false;
          this.showArtworkUploadSuccessAlert = false;
          this.showArtworkRejectedByVendor = false;
          this.showProofRejectedByVendor = false;
          this.proofAccepted = false;
          this.showOrderCompleted = false;
          this.updateSuccess = false;
          this.successAdminResetStatus = false;

          // Scroll to the top of the page
          this.scrollToTop();
      }
    });

    this.route.queryParams.subscribe(params => {
      if (params['ArtworkRejectedByVendor'] === 'true') {
          this.showArtworkRejectedByVendor = true;

          this.showSuccessAlert = false;
          this.showArtworkUploadSuccessAlert = false;
          this.showProofUploadSuccessAlert = false;
          this.showProofRejectedByVendor = false;
          this.proofAccepted = false;
          this.showOrderCompleted = false;
          this.updateSuccess = false;
          this.successAdminResetStatus = false;

          // Scroll to the top of the page
          this.scrollToTop();
      }
    });

    this.route.queryParams.subscribe(params => {
      if (params['showProofRejectedByVendor'] === 'true') {
          this.showProofRejectedByVendor = true;

          this.showSuccessAlert = false;
          this.showArtworkUploadSuccessAlert = false;
          this.showProofUploadSuccessAlert = false;
          this.showArtworkRejectedByVendor = false;
          this.proofAccepted = false;
          this.showOrderCompleted = false;
          this.updateSuccess = false;
          this.successAdminResetStatus = false;

          // Scroll to the top of the page
          this.scrollToTop();
      }
    });

    this.route.queryParams.subscribe(params => {
      if (params['proofAccepted'] === 'true') {
          this.proofAccepted = true;

          this.showSuccessAlert = false;
          this.showArtworkUploadSuccessAlert = false;
          this.showProofUploadSuccessAlert = false;
          this.showArtworkRejectedByVendor = false;
          this.showProofRejectedByVendor = false;
          this.showOrderCompleted = false;
          this.updateSuccess = false;
          this.successAdminResetStatus = false;

          // Scroll to the top of the page
          this.scrollToTop();
      }
    });

    this.route.queryParams.subscribe(params => {
      if (params['showOrderCompleted'] === 'true') {
          this.showOrderCompleted = true;

          this.showSuccessAlert = false;
          this.showArtworkUploadSuccessAlert = false;
          this.showProofUploadSuccessAlert = false;
          this.showArtworkRejectedByVendor = false;
          this.showProofRejectedByVendor = false;
          this.proofAccepted = false;
          this.updateSuccess = false;
          this.successAdminResetStatus = false;

          // Scroll to the top of the page
          this.scrollToTop();
      }
    });
    
    this.route.queryParams.subscribe(params => {
      if (params['updateSuccess'] === 'true') {
          this.updateSuccess = true;

          this.showSuccessAlert = false;
          this.showArtworkUploadSuccessAlert = false;
          this.showProofUploadSuccessAlert = false;
          this.showArtworkRejectedByVendor = false;
          this.showProofRejectedByVendor = false;
          this.proofAccepted = false;
          this.showOrderCompleted = false;
          this.successAdminResetStatus = false;

          // Scroll to the top of the page
          this.scrollToTop();
      }
    });

    this.route.queryParams.subscribe(params => {
      if (params['successAdminResetStatus'] === 'true') {
          this.successAdminResetStatus = true;

          this.showSuccessAlert = false;
          this.showArtworkUploadSuccessAlert = false;
          this.showProofUploadSuccessAlert = false;
          this.showArtworkRejectedByVendor = false;
          this.showProofRejectedByVendor = false;
          this.proofAccepted = false;
          this.showOrderCompleted = false;
          this.updateSuccess = false;

          // Scroll to the top of the page
          this.scrollToTop();
      }
    });

    this.route.queryParams.subscribe(params => {
      if (params['repeatOrderSuccess'] === 'true') {
          this.repeatOrderSuccess = true;

          this.successAdminResetStatus = false;
          this.showSuccessAlert = false;
          this.showArtworkUploadSuccessAlert = false;
          this.showProofUploadSuccessAlert = false;
          this.showArtworkRejectedByVendor = false;
          this.showProofRejectedByVendor = false;
          this.proofAccepted = false;
          this.showOrderCompleted = false;
          this.updateSuccess = false;

          // Scroll to the top of the page
          this.scrollToTop();
      }
    });



  }

  confirmDeleteOrder(orderId: string, CTOrderNumber: string): void {
    this.orderIdToDelete = orderId;
    console.log('orderIdToDelete:', this.orderIdToDelete);
    this.orderNumber = CTOrderNumber;
    // this.showConfirmation = true; 
  }

  deleteOrder(): void {
    if (!this.orderIdToDelete) {
      console.error('No OrderID to delete');
      return;
    }

    this.showSuccessAlert = false;
    this.showArtworkUploadSuccessAlert = false;
    this.showProofUploadSuccessAlert = false;
    this.showOrderCompleted = false;
    this.updateSuccess = false;
    this.successAdminResetStatus = false;

    this.ordersService.deleteOrder(this.orderIdToDelete).subscribe(
      () => {
        console.log('Order deleted successfully');
        this.showDeleteAlert = true; // Show delete success message
        this.closeModal(); // Close the modal
        this.scrollToTop(); // Scroll to the top of the page

        this.orders = this.orders.filter(order => order.OrderID !== this.orderIdToDelete);

        // Destroy the existing DataTable
        const table = $('#example').DataTable();
        if (table) {
          table.destroy();
        }
        setTimeout(() => {
          this.initDataTables(); // Reinitialize DataTable
        }, 100);
      },
      (error) => {
        console.error('Failed to delete order', error);
      }
    );
  }

  closeModal(): void {
    const modalElement = document.getElementById('exampleModal');
    if (modalElement) {
      modalElement.classList.remove('show');
      modalElement.setAttribute('aria-hidden', 'true');
      modalElement.style.display = 'none';
      const modalBackdrop = document.getElementsByClassName('modal-backdrop')[0];
      if (modalBackdrop) {
        document.body.removeChild(modalBackdrop);
      }
    }
  }

  ngAfterViewInit(): void {
    const tooltips = document.querySelectorAll('.tt');
    tooltips.forEach(t => {
      new bootstrap.Tooltip(t);
    });
  }

  private scrollToTop(): void {
    // Scroll to the top of the page
    this.elementRef.nativeElement.ownerDocument.documentElement.scrollTop = 0;
  }

  private initDataTables(): void {
    // Use Angular lifecycle hook to initialize DataTables after the view has been initialized.
    // This ensures that the table element exists in the DOM.
    // Note: Avoid using jQuery in Angular wherever possible. If DataTables is available as an Angular library, consider using that instead.
    setTimeout(() => {
      $('#example').DataTable({
        // paging: false,
        // searching: false,
        // info: false
        columnDefs: [{ type: 'date', targets: [0] }],
        order: [[0, 'desc']]
      });
    });
  }

  // Navigate to the reset order status page with the order ID
  navigateToResetOrderStatus(OrderID: string) {
    // this.ordersService.setOrderId(OrderID);
    this.router.navigate(['/resetOrderStatus', OrderID]);
  }
  
  navigateToreviewProof(OrderID: string,ProofID:string) {
    this.router.navigate([ '/reviewProof', OrderID,ProofID ])
  }
  navigateToUploadProof(OrderID: string) {
    this.router.navigate([ '/uploadProof', OrderID ])
  }

  // download the recent proof file
  downloadProof(OrderID: string) {
    debugger;
    this.proofService.getProofToDownload(OrderID).subscribe(
      (res) => {
        const proofDownloadOrderID = res[0].OrderID; 
        const proofDownloadFileName = encodeURIComponent(res[0].FileName);
  
        this.proofService.downloadProofFile(proofDownloadOrderID, proofDownloadFileName)
          .subscribe(
            (fileRes) => {
              const blob = new Blob([fileRes], { type: 'application/octet-stream' });
              const url = window.URL.createObjectURL(blob);
              
              const link = document.createElement('a');
              link.href = url;
              link.download = proofDownloadFileName; 
              document.body.appendChild(link);
              
              link.click();
              
              window.URL.revokeObjectURL(url);
              document.body.removeChild(link);
              
              console.log("File downloaded successfully");
            },
            (error) => {
              console.log("Failed to download file", error);
            }
          );
      },
      (error) => {
        console.log('Failed to get artwork details', error);
      }
    );
  }
  
  // Navigate to the upload artwork page with the order ID
  uploadArtwork(OrderID: string) {
    this.router.navigate([ '/uploadArtwork', OrderID ]);
  }
  ReviewArtwork(OrderID: string,ArtworkID:string) {
    this.router.navigate([ '/reviewArtwork', OrderID,ArtworkID ]);
  }

  // download the recent artwork file
  downloadArtwork(OrderID: string) {
    debugger;
    this.artworkService.getArtworkToDownload(OrderID).subscribe(
      (res) => {
        const artworkDownloadOrderID = res[0].OrderID; 
        const artworkDownloadFileName = encodeURIComponent(res[0].FileName); 
  
        this.artworkService.downloadArtworkFile(artworkDownloadOrderID, artworkDownloadFileName)
          .subscribe(
            (fileRes) => {
              const blob = new Blob([fileRes], { type: 'application/octet-stream' });
              const url = window.URL.createObjectURL(blob);
              
              const link = document.createElement('a');
              link.href = url;
              link.download = artworkDownloadFileName; 
              document.body.appendChild(link);
              
              link.click();
              
              window.URL.revokeObjectURL(url);
              document.body.removeChild(link);
              
              console.log("File downloaded successfully");
            },
            (error) => {
              console.log("Failed to download file", error);
            }
          );
      },
      (error) => {
        console.log('Failed to get artwork details', error);
      }
    );
  }

  // Navigate to view order with the OrderID
  navigateToViewOrder(OrderID: string) {
    this.router.navigate(['/viewOrder', OrderID]);
  }

  printOrder(OrderID: string) {
    // this.router.navigate(['/printOrder', OrderID]);
    const url = '/Productcustomization/printOrder/' + OrderID;
    window.open(url, '_blank');
  }

  // Navigate to edit order with the OrderID
  navigateToRepeatOrder(OrderID: string) {
   
   this.router.navigate(['/repeatOrder', OrderID]);
  }

  // Navigate to edit order with the OrderID
  navigateToEditOrder(OrderID: string) {
    this.router.navigate(['/editOrder', OrderID]);
  }

  // Navigate to edit order with the OrderID
  navigateToUpdateOrderStatus(OrderID: string) {
    this.router.navigate(['/updateOrderStatus', OrderID]);
  }

  ngOnDestroy(): void {
    this.destroyDataTables();
  }

  private destroyDataTables(): void {
    const table = $('#example').DataTable();
    if (table) {
      table.destroy();
    }
  }

  closeAlert() {
    this.showSuccessAlert = false;
  }
  

}
