import { DatePipe, Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { MailService } from 'src/app/services/mail.service';
import { OrdersService } from 'src/app/services/orders.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-create-new-order',
  templateUrl: './create-new-order.component.html',
  styleUrls: ['./create-new-order.component.css']
})
export class CreateNewOrderComponent {
[x: string]: any;

  UserID!: string; // session storage variable
  FirstName!: string; // from session storage
  LastName!: string; // from session storage
  EmailAddress!: string; // from session storage

  currentDate: Date = new Date();

  // showSuccessAlert: boolean = false;
  userIPAdress!: string;
  browserName!: string;

  errorMessage: string = '';

  currentOrderNumber: any;
  currentOrderID: any;
  currentCTOrderNumber: any;

  todayDate!: string;
  FullName!:string;

  allCountries: any[] = [];
  allStates: any[] = [];
  customerEmails: any[] = [];
  allSites: any[] = [];

  order: any = {
    Siteid: '',
    CTOrderNumber: '',
    CTPONumber: '',
    EnteredBy: this.FirstName + this.LastName,
    ArtworkID: '',
    ProofID: '',
    OrderBy: '',
    OrderType: 'New Order',
    CTShipToInformation_Name: '',
    CTShipToInformation_CompanyName: '',
    CTShipToInformation_AccountNumber: '',
    CTShipToInformation_Address: '',
    CTShipToInformation_City: '',
    CTShipToInformation_State: '',
    CTShipToInformation_ZipCode: '',
    CTShipToInformation_Country: '',
    CTShipToInformation_Phone: '',
    StatusId: 1,

    // this one is for dropdown
    CTShipToInformation_EmailAddress: '',

    ApprovedByClient: '',
    userid: this.UserID, // get this from the session storage
    orderID: '',
    RegionNumber: '',
    RepeatOrderComment: 'NULL',
    TerritoryNumber: ''
  };

  products: any[] = [
    {
      OrderID: '', 
      ProductCode: '',
      ProductDescription: '',
      ProductQty: '',
      ReferenceNo: ''
    }
  ];

  productDetails: any[] = [
    {
      OrderID: '',
      ProductCode: '',
      ProductDescription: '',
      ProductQty: '',
      ReferenceNo: ''
    },
    {
      OrderID: '',
      ProductCode: '',
      ProductDescription: '',
      ProductQty: '',
      ReferenceNo: ''
    },
    {
      OrderID: '',
      ProductCode: '',
      ProductDescription: '',
      ProductQty: '',
      ReferenceNo: ''
    },
    {
      OrderID: '',
      ProductCode: '',
      ProductDescription: '',
      ProductQty: '',
      ReferenceNo: ''
    },
    {
      OrderID: '',
      ProductCode: '',
      ProductDescription: '',
      ProductQty: '',
      ReferenceNo: ''
    }
  ];

  customers : any = {
    userid: '',
    orderID: '',
    orderByAccountNumber: '',
    shipToAccountNumber: '',
    fullName: '',
    emailAddress: '',
    dateAdded: '',
    dateLastUsed: '',
    enteredBy: '',
    isPrimary: ''
  }
  
  constructor(
    private ordersService: OrdersService,
    private MailService :MailService, 
    private datePipe: DatePipe, 
    private router: Router,
    private http: HttpClient,
    private _location: Location  
  ) { 
    this.UserID = sessionStorage.getItem("UserID") ?? ''; // getting the UserID from session storage
    this.FirstName = sessionStorage.getItem("FirstName") ?? '';
    this.LastName = sessionStorage.getItem("LastName") ?? '';
    this.EmailAddress = sessionStorage.getItem("EmailAddress") ?? '';
  }

  goBack() {
    this._location.back();
  }

   createOrder() {
    debugger;
    this.order.userid = this.UserID; 

    this.loadAllOrderSatus(); // Load order status first

    this.ordersService.createNewOrder(this.order).subscribe(
      (orderResponse) => {
        debugger;
        console.log('Order created successfully:', orderResponse);

        // Extract the OrderID from the response
        const orderID = orderResponse[0].orderID;
        this.currentCTOrderNumber = orderResponse[0].ctOrderNumber;

        this.currentOrderID = orderResponse[0].orderID;

        // Assign the OrderID to each product detail
        this.productDetails.forEach((product: any) => {
        product.OrderID = orderID;
        });

        // Send the request to create product details
        this.createProductDetails(orderID);  

        // Load customer emails and assign customers after creating the order
        this.loadCustomerEmailsAndAssignCustomers(orderID);

        this.browserName = (navigator as any).userAgentData.brands[0].brand;

        //inserting a log for the created order
        const logData = {
          UserID: this.UserID,
          IPAddress: this.userIPAdress,
          OrderID: orderID,
          ActionType: 'New Order',
          ActionDescription: `New order created was created by ${this.EmailAddress}`,
          BrowserInfo: this.browserName,
          Comments: '---', 
          Status: '' 
        };
      
        // Call your service method to insert log
        this.ordersService.insertLog(logData).subscribe(
          (response) => {
            console.log('Log inserted successfully:', response);
          },
          (error) => {
            console.error('Error inserting log:', error);
          }
        );
        
        // Redirect to '/allOrders' upon successful order creation
        const navigationExtras: NavigationExtras = {
          queryParams: { success: 'true' },
          fragment: 'top' // Scroll to top when navigating
        };
        this.router.navigate(['/allOrders'], navigationExtras);

      },
      (orderError) => {
        console.error('Error creating order:', orderError);
      }
    );
  }

  checkCTOrderNumber() {
    if (this.order.CTOrderNumber) {
      this.ordersService.checkCTOrderNumberAvailability(this.order.CTOrderNumber).subscribe(
        (response) => { 
          console.log(response.message);
          this.errorMessage = response.message;
          // If CTOrderNumber is available, reset the error message
          if (response.message === 'This Order Number is available.') {
            this.errorMessage = '';
          }
        },
        (error) => {
          console.error(error); 
        }
      );
    } else {
      this.errorMessage = '';
    }
  }
  onOptionsSelected(value:string){
    this.loadCustomerEmails(value);
}
NewOrderMail(FullName:any,OrderNo: any,Siteid:any,ToEmail:any)
{
  debugger;
  const Template="NewOrder";
  // const Servername="http:%2F%2Flocalhost:4200%2F";
  // const Servername="https%3A%2F%2Fdigital-team-805wb.certainteed.com";
  // const ExtraUrl="Productcustomization";
  const Servername = encodeURIComponent("https://digital-team-805wb.certainteed.com");
  const ExtraUrl = encodeURIComponent("Productcustomization");
  // const ExtraUrl="null";
 

  this.MailService.NewOrderMail(Template,Servername,ExtraUrl,FullName, OrderNo, Siteid,ToEmail).subscribe(
    (orderResponse) => {
      console.log('Order created successfully:', orderResponse);
    });
}

  createProductDetails(orderID: any) {
    this.productDetails.forEach((product: any) => { // Iterate over productDetails array

      // Check if all required fields have values
    if (
      product.ProductCode.trim() !== '' &&
      product.ProductDescription.trim() !== '' &&
      product.ProductQty.trim() !== ''
    ) { 
      const productToSend = {
        OrderID: orderID,
        ProductCode: product.ProductCode,
        ProductDescription: product.ProductDescription,
        ProductQty: product.ProductQty,
        ReferenceNo: product.ReferenceNo
      };
  
      this.ordersService.createProductDetails(productToSend).subscribe(
        (ProductPesponse) => {
          console.log('Product detail added successfully:', ProductPesponse);
        },
        (error) => {
          console.error('Error creating product detail:', error);
        }
      );
     }
    });
  }

  ngOnInit(): void {

    this.FirstName = sessionStorage.getItem("FirstName") ?? '';
    this.LastName = sessionStorage.getItem("LastName") ?? '';
    this.order.EnteredBy = `${this.FirstName} ${this.LastName}`;
    this.todayDate = this.datePipe.transform(new Date(), 'dd-MM-yyyy') ?? '';
    
    this.getAllCountries();
    this.getAllStates();
    this.loadAllOrderSatus();
    this.loadallSites();
   //this.loadCustomerEmails();
    this.getIPInfo();
    // this.getOrderDetails();
  
  }

  loadAllOrderSatus() {
    this.ordersService.getAllOrderStatus().subscribe(
      (orderResponse: any) => {
        console.log(orderResponse)
        // this.order.StatusId = orderResponse[0].StatusId;
      },
      (error) => {
        console.error('Error loading order status', error);
      }
    )
  }

  loadallSites() {
    this.ordersService.getAllSites(this.UserID).subscribe(
      (response: any[]) => { 
        this.allSites = response;
        // console.log(this.allSites);
      },
      (error) => {
        console.error('Error fetching all sites', error);
      }
    )
  }

  // loading customer emails in the dropdown
  loadCustomerEmails(value:any) {
    this.ordersService.getCustomerEmails(value).subscribe(
      (data: any[]) => {
        this.customerEmails = data;
      },
      (error) => {
        console.error('Error fetching customer emails', error);
      }
    );
  }

  loadCustomerEmailsAndAssignCustomers(orderID: any) {
    this.ordersService.getCustomerEmails(this.order.Siteid).subscribe(
      (data: any[]) => {
        debugger;
        console.log(data);
        this.customerEmails = data;

        // selected email address
        const selectedEmailAddress = this.order.CTShipToInformation_EmailAddress;
        console.log('selected email address: ', selectedEmailAddress);

        const selectedCustomer = this.customerEmails.find(email => email.EmailAddress === selectedEmailAddress);
        console.log('sss: ', selectedCustomer);
        
        if(selectedCustomer) {

          const customers = {
            fullName: selectedCustomer.FirstName + " " + selectedCustomer.LastName,
            enteredBy: selectedCustomer.FirstName,
            userid: selectedCustomer.UserID,
            emailAddress: encodeURIComponent(selectedCustomer.EmailAddress),
            orderID: orderID,
            DateAdded: '',
            DateLastUsed: '',
            IsPrimary: '',
            OrderByAccountNumber: '',
            ShipToAccountNumber: ''
          };
          this.assignCustomers(customers);

          const fullName = selectedCustomer.FirstName + " " + selectedCustomer.LastName;
          const selectedSiteId = this.order.Siteid;
          const index1 = selectedSiteId - 1;
          const Siteid = this.allSites[index1].SiteName;
          const toEmail = selectedCustomer.EmailAddress;

          this.NewOrderMail(fullName, this.currentCTOrderNumber, Siteid, toEmail);
        } else {
          console.error('Selected customer not found!');
        }
        
        
      
        // this.FullName=this.customerEmails[index].FirstName+" "+this.customerEmails[index].LastName;
        // this.customers.enteredBy = this.UserID;
        // this.customers.userid = selectedUserId;
        // this.customers.emailAddress = selectedEmailAddress;
        // this.customers.orderID = orderID;
        // debugger;
        // // this.assignCustomers(this.customers);
        // const selectedSiteId = this.order.Siteid;

        // const index1 = selectedSiteId - 1
        // const Siteid = this.allSites[index1].SiteName
        // const ToEmail=this.customerEmails[index].EmailAddress;
        // this.NewOrderMail(this.FullName,orderID,Siteid,ToEmail) 
        
      },
      (error) => {
        console.error('Error fetching customer emails', error);
      }
    );
  }

  assignCustomers(customers: any) {
    this.ordersService.assignCustomers(customers).subscribe(
      (customerResponse) => {
        debugger;
        console.log("Customers assigned !!!", customerResponse);
      },
      (customerError) => {
        console.log("Customers NOT ASSIGNED !!!", customerError);
      }
    );
  }

  getOrderDetails() {
    this.ordersService.getOrderDetails(this.currentOrderID).subscribe(
      (res) => {
        this.currentOrderNumber = res[0].CTOrderNumber;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getIPInfo() {
    this.http.get<any>('https://ipinfo.io').subscribe(
      (response) => {
        // console.log(response);
        // console.log(response.ip);
        this.userIPAdress = response.ip;
      },
      (error) => {
        console.log('Error fetching IP info', error);
      }
    );
  }

  getAllCountries() {
    this.ordersService.getAllCountries().subscribe(
      (res) => {
        this.allCountries = res;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getAllStates() {
    this.ordersService.getAllStates().subscribe(
      (res) => {
        this.allStates = res;
      },
      (error) => {
        console.log(error);
      }
    );
  }
  

}
