import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { SsoService } from 'src/app/services/sso.service';
import { environment } from'../../../environments/environment';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent {

  showWelcome: any = false;
  showIcons: boolean = true;
  showSuperRoles: boolean = false;
  
  loginSuccessMessage!: string;
  SGID: string | undefined;
  URl: any;
  pathPrefix: any;

  constructor(
    private http: HttpClient,
    private ssoService: SsoService, 
    private router: Router, 
    private route: ActivatedRoute,
    private _global: GlobalService) 
  { 
    this.URl = _global.URL;
    this.pathPrefix = _global.pathPrefix;
  }
  
  navigateToSsoPage() {
    console.log(environment.Api);
    // debugger;
    // Redirect to the SSO login page
    // window.location.href = "https://uat.websso.saint-gobain.com/cas/login" + "?service=" + "http://localhost:54100/" + "api/sso1/";
    //  window.location.href = "https://uat.websso.saint-gobain.com/cas/login"+ "?service=" + "https://localhost:7049/" + "api/Login/sso1/";
   window.location.href = "https://uat.websso.saint-gobain.com/cas/login"+ "?service=" + environment.Api + "/api/Login/sso1/";
  }

}
