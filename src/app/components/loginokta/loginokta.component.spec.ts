import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginoktaComponent } from './loginokta.component';

describe('LoginoktaComponent', () => {
  let component: LoginoktaComponent;
  let fixture: ComponentFixture<LoginoktaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginoktaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LoginoktaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
