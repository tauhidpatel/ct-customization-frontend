import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Okta } from 'src/app/shared/okta/okta.service';

@Component({
  selector: 'app-loginokta',
  templateUrl: './loginokta.component.html',
  styleUrls: ['./loginokta.component.css']
})
export class LoginoktaComponent implements OnInit {
  title = 'app';
  user: any;
  oktaSignIn;
  private route: ActivatedRoute = new ActivatedRoute;

  constructor(private okta: Okta,private router: Router,private zone: NgZone,private http: HttpClient,private changeDetectorRef: ChangeDetectorRef) {
   // console.log(environment.language);
   // environment.language="en";
    //console.log(environment.language);
    this.oktaSignIn = okta.getWidget();
    debugger;
  }

  showLogin(): void {
    this.oktaSignIn?.remove();
    console.log('in login');
    this.oktaSignIn.renderEl({el: '#okta-login-container'}, (response:any) => {
      console.log('in auth');
      if (response.status === 'SUCCESS') {
        this.oktaSignIn.authClient.tokenManager.add('idToken', response.tokens.idToken);
        this.oktaSignIn.authClient.tokenManager.add('accessToken', response.tokens.accessToken);
        this.user = response.tokens.idToken.claims.email;
       // alert(this.user);
     //   console.log(response);
        sessionStorage.setItem("userId",response.tokens.idToken.claims.sub);
     //  alert(sessionStorage.getItem("userId")?.toString());
        this.oktaSignIn.remove();
        this.changeDetectorRef.detectChanges();
        this.zone.run(() => {
          this.router.navigate(['/login']); 
      });
       
      }
     
    });
  }

  async ngOnInit(): Promise<void> {
   
  }
  async ngAfterViewInit() {
    $(".arrow2").css("display","none");
    $(".arrow1").css("display","none");
  //  this.showLogin();
    // ngOnInit doesn't work for this :(
    // https://github.com/okta/okta-signin-widget/issues/278
    try {
      // this.oktaSignIn.authClient.session.get(response => {
      //   console.log(response);
      //   if (response.status !== "INACTIVE") {
      //     this.user = response.login;
      //     this.changeDetectorRef.detectChanges();
      //   } else {
      //     console.log("response.status was inactive:", response.status);
      //     this.showLogin();
      //   }
      // });
      var response = await this.oktaSignIn.authClient.session.get();
     // console.log(response);
      if (response.status !== "INACTIVE") {
        sessionStorage.setItem("userId",response.userId);
        this.router.navigate(['/login']); 
      }
      else
      {
        this.showLogin();
      }
    } catch (error) {
    //  console.log(error);
    }
    // show login screen anyway...
   // this.showLogin();
  
   
  }
  logout(): void {
    this.oktaSignIn.signOut(() => {
      this.user = undefined;
      this.showLogin();
    });
  }
}
