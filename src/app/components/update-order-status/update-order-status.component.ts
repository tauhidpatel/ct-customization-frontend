import { Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { OrdersService } from 'src/app/services/orders.service';

@Component({
  selector: 'app-update-order-status',
  templateUrl: './update-order-status.component.html',
  styleUrls: ['./update-order-status.component.css']
})
export class UpdateOrderStatusComponent {

  OrderName!: string;

  userIPAdress!: string;
  browserName!: string;

  UserID!:string; // from session storage
  ORDERID!:string; // from URL PARAMS

  updateProductCode: boolean = false;
  updateShipDate: boolean = false;
  updateCompletionDate: boolean = false;
  row_color = "#eeeeee";
  loggedIn="#ffffff";
  orders: any[] = [];
  ActualShipDate:any;
  AnticipatedShipDate:any
  
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private ordersService: OrdersService, 
    private router: Router,
    private _location: Location
  ) 
  {
    this.UserID = sessionStorage.getItem("UserID") ?? '';
  }

  goBack() {
    this._location.back();
  }

  ngOnInit()
  {
    this.getIPInfo();
    this.getOrderDetails();
    this.ORDERID = this.route.snapshot.params['OrderID'];
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getGetOrderDetailsByOrderID(orderId).subscribe(
      (res: any[]) => {
        // console.log(res);
      this.orders=res;
       

       

      },
      (error) => {
        console.log(error);
      });
  }

  Submit()
  {
    debugger;
    if(this.updateShipDate==true)
      {
        const orderId = this.route.snapshot.params['OrderID'];
        this.ordersService.UpdateAnticipatedShipDate(orderId,this.AnticipatedShipDate).subscribe(
         (response) => {
          console.log('shipping update response: ', response);

          this.browserName = (navigator as any).userAgentData.brands[0].brand
              const logData = {
                UserID: this.UserID,
                IPAddress: this.userIPAdress, 
                OrderID: this.ORDERID,
                ActionType: 'Update Shipping',
                ActionDescription: 'Anticipated Ship Date was updated',
                BrowserInfo: this.browserName, 
                Comments: '---', 
                Status: '' 
              };
          
              // Call your service method to insert log
              this.ordersService.insertLog(logData).subscribe(
                (response) => {
                  console.log('Log inserted successfully:', response);
                },
                (error) => {
                  console.error('Error inserting log:', error);
                }
              );

          });
      }
      if(this.updateCompletionDate==true)
        {
          const orderId = this.route.snapshot.params['OrderID'];
          this.ordersService.CompleteOrder(orderId,this.ActualShipDate).subscribe(
            (response) => {
              console.log('complete order: ', response);

              this.browserName = (navigator as any).userAgentData.brands[0].brand
              const logData = {
                UserID: this.UserID,
                IPAddress: this.userIPAdress, 
                OrderID: this.ORDERID,
                ActionType: 'Update Shipping',
                ActionDescription: 'Shipping Date was updated as COMPLETED',
                BrowserInfo: this.browserName, 
                Comments: '---', 
                Status: '' 
              };
          
              // Call your service method to insert log
              this.ordersService.insertLog(logData).subscribe(
                (response) => {
                  console.log('Log inserted successfully:', response);
                },
                (error) => {
                  console.error('Error inserting log:', error);
                }
              );

            });
        }

    if(this.updateShipDate==true)
      {
        if(this.updateProductCode==true)
          {
            this.orders.forEach((product: any) => {
              console.log(product);
              this.ordersService.UpdateOrderDetailsByOrderID(product.NewProductCode,product.ProxyID,product.ProxyID).subscribe(
                (response) => {
                  console.log(response);
                });
            });
          }
          else
          {
            this.orders.forEach((product: any) => {
              console.log(product);
              this.ordersService.UpdateOrderDetailsByOrderID(null,product.ProxyID,product.ProxyID).subscribe(
                (response) => {
                  console.log(response);
                });
            });
          }
      }

      const orderId = this.route.snapshot.params['OrderID'];
      this.ordersService.updateOrderStatus(orderId, 5).subscribe(
        (res) => { 
          console.log('order status updated successfully',res);
        },
        (error) => {
          console.log('unable to update order status', error);
        }
      )

      // Redirect to '/allOrders' upon successful order creation
      const navigationExtras: NavigationExtras = {
        queryParams: { showOrderCompleted: 'true' },
        fragment: 'top' // Scroll to top when navigating
      };
      this.router.navigate(['/allOrders'], navigationExtras);
  }

  getOrderDetails() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getOrderDetails(orderId).subscribe(
      (res) => { 
        // console.log(res);
        this.OrderName = res[0].CTOrderNumber;
      },
      (error) => {
        console.log(error);
      }
    )
  }

  getIPInfo() {
    this.http.get<any>('https://ipinfo.io').subscribe(
      (response) => {
        this.userIPAdress = response.ip;
      },
      (error) => {
        console.log('Error fetching IP info', error);
      }
    );
  }

}
