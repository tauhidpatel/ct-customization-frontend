import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-authorize',
  templateUrl: './authorize.component.html',
  styleUrls: ['./authorize.component.css']
})
export class AuthorizeComponent {

  pathPrefix:any;
  URL:any;

  constructor( private route: ActivatedRoute,private _global: GlobalService) { 
    this.URL=_global.URL;
    this.pathPrefix=_global.pathPrefix;
      }

  ngOnInit() {
  
    this.route.queryParams
    .subscribe(params => {

       console.log(params);
      
      sessionStorage.setItem("SGID",params["SGID"]);
      sessionStorage.setItem("UserID",params["UserID"]);
      sessionStorage.setItem("FirstName",params["FirstName"]);
      sessionStorage.setItem("LastName",params["LastName"]);
      sessionStorage.setItem("Email",params["Email"]);
      sessionStorage.setItem("CompanyName",params["CompanyName"]);
      sessionStorage.setItem("Country", params["Country"]);
      sessionStorage.setItem("SecurityLevel",params["SecurityLevel"]);
      sessionStorage.setItem("SiteName",params["SiteName"]);
      sessionStorage.setItem("SiteId",params["SiteId"]);

        // window.location.href = "http://localhost:4200/welcome";
     window.location.href = "https://digital-team-805wb.certainteed.com/Productcustomization/welcome";
      //https://digital-team-805wb.certainteed.com/Productcustomization/welcome
     });
    }

}