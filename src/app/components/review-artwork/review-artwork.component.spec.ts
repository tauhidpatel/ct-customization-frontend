import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviewArtworkComponent } from './review-artwork.component';

describe('ReviewArtworkComponent', () => {
  let component: ReviewArtworkComponent;
  let fixture: ComponentFixture<ReviewArtworkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviewArtworkComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReviewArtworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
