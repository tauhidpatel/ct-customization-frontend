import { DatePipe, Location } from '@angular/common';
import { Component } from '@angular/core';
import { MailService } from 'src/app/services/mail.service';
import { ArtworkService } from 'src/app/services/artwork.service';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { OrdersService } from 'src/app/services/orders.service';
@Component({
  selector: 'app-review-artwork',
  templateUrl: './review-artwork.component.html',
  styleUrls: ['./review-artwork.component.css']
})
export class ReviewArtworkComponent {

  UserID: string;
  orderID: any;
  str!: string;

  pmsColors: any;
  artworkFileName: any;

  userIPAdress!: string;
  browserName!: string;

  selectedSite: any;

  FirstName: string;
  LastName: string;
  EmailAddress: string;
  
  ArtworkID: any;

  OrderName!: string;

  constructor(private ordersService: OrdersService,private ArtworkService: ArtworkService, private datePipe: DatePipe,private MailService :MailService,   private route: ActivatedRoute,private http: HttpClient,private router: Router, private _location: Location  ) { 
    this.UserID = sessionStorage.getItem("UserID") ?? '';
    this.FirstName = sessionStorage.getItem("FirstName") ?? '';
    this.LastName = sessionStorage.getItem("LastName") ?? '';
    this.EmailAddress = sessionStorage.getItem("EmailAddress") ?? '';
  }

  goBack() {
    this._location.back();
  }

  ngOnInit(): void {
    
    this.getOrderDetails();
    this.getIPInfo();
    this.getSelectedOrderSite();

    
    this.orderID = this.route.snapshot.params['OrderID'];
    this.ArtworkID = this.route.snapshot.params['ArtworkID'];
  } 

  ngAfterViewInit() {
    this.getPMSColors();
  }

  getPMSColors() {
    this.ordersService.getPMSColor(this.orderID).subscribe(
      (res) => {
        console.log(res[0].PMSColors.length)
        this.artworkFileName = res[0].FileName;
        if (res[0].PMSColors.length > 0 && res[0].PMSColors) {
          this.pmsColors = res[0].PMSColors;
        } else {
          this.pmsColors = 'N/A';
        }
        this.pmsColors = res[0].PMSColors;
      },
      (error) => {
        console.log(error);
      }
    )
  }

  ArtworkReject() {
    if (this.str && this.str.trim().length > 0)  {
      debugger;
    this.ArtworkService.ArtworkReject(this.orderID, this.UserID, this.userIPAdress, this.ArtworkID, this.str).subscribe(
      (Response) => {
        debugger;
        // console.log(Response);

        if(Response === 'Success') {
          this.ArtworkRejectMail(this.OrderName,'1') 

          this.browserName = (navigator as any).userAgentData.brands[0].brand
          const logData = {
            UserID: this.UserID,
            IPAddress: this.userIPAdress, 
            OrderID: this.orderID,
            ActionType: 'Artwork Rejected',
            ActionDescription: `Artwork was rejected by ${this.EmailAddress}`,
            BrowserInfo: this.browserName, 
            Comments: '---', 
            Status: '' 
          };
        
          // Call your service method to insert log
          this.ordersService.insertLog(logData).subscribe(
            (response) => {
              console.log('Log inserted successfully:', response);
            },
            (error) => {
              console.error('Error inserting log:', error);
            }
          );

          // Redirect to '/allOrders' upon successful artwork rejection
          const navigationExtras: NavigationExtras = {
            queryParams: { ArtworkRejectedByVendor: 'true' },
            fragment: 'top' // Scroll to top when navigating
          };
          this.router.navigate(['/allOrders'], navigationExtras);
        }
      });
    } else {
      // Inform the user to provide a rejection reason
      alert('Please provide a reason for rejecting the artwork.');
    }
    
  }

  ArtworkRejectMail(OrderNo: any,Siteid:any)
  {
    debugger;
    const Template="ArtworkRejectedByVendor";
    // const Servername="http:%2F%2Flocalhost:4200%2F";
    // const ExtraUrl="null";

    const Servername = encodeURIComponent("https://digital-team-805wb.certainteed.com");
    const ExtraUrl = encodeURIComponent("Productcustomization");

   const varExistingFileID=this.artworkFileName;
   const REJECTREASON=this.str;
  
    this.MailService.SendArtworkRejectedByVendorEmail( Template, Servername, ExtraUrl, this.OrderName, this.selectedSite, this.UserID, this.FirstName, varExistingFileID, REJECTREASON ).subscribe(
      (orderResponse) => {
        console.log('Order created successfully:', orderResponse);
      });
  }

  navigateToUploadProof() {
    this.router.navigate([ '/uploadProof', this.orderID ])
  }

  downloadArtwork() {
    debugger;
    this.ArtworkService.getArtworkToDownload(this.orderID).subscribe(
      (res) => {
        const artworkDownloadOrderID = res[0].OrderID; 
        const artworkDownloadFileName = res[0].FileName;
  
        this.ArtworkService.downloadArtworkFile(artworkDownloadOrderID, artworkDownloadFileName)
          .subscribe(
            (fileRes) => {
              const blob = new Blob([fileRes], { type: 'application/octet-stream' });
              const url = window.URL.createObjectURL(blob);
              
              const link = document.createElement('a');
              link.href = url;
              link.download = artworkDownloadFileName; 
              document.body.appendChild(link);
              
              link.click();
              
              window.URL.revokeObjectURL(url);
              document.body.removeChild(link);
              
              console.log("File downloaded successfully");
            },
            (error) => {
              console.log("Failed to download file", error);
            }
          );
      },
      (error) => {
        console.log('Failed to get artwork details', error);
      }
    );
  }
  
  getIPInfo() {
    this.http.get<any>('https://ipinfo.io').subscribe(
      (response) => {
        // console.log(response);
        // console.log(response.ip);
        this.userIPAdress = response.ip;
      },
      (error) => {
        console.log('Error fetching IP info', error);
      }
    );
  }

  getOrderDetails() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getOrderDetails(orderId).subscribe(
      (res) => { 
        // console.log(res);
        this.OrderName = res[0].CTOrderNumber;
      },
      (error) => {
        console.log(error);
      }
    )
  }

  getSelectedOrderSite() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getSelectedOrderSite(orderId).subscribe(
      (response) => {
        console.log(response[0].SiteName);
        this.selectedSite = response[0].SiteName;
      },
      (error) => {
        console.log('error fetching order details', error);
      }
    )
  }

}
