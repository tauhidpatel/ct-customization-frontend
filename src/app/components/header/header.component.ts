import { Component, ElementRef, HostListener, Inject, LOCALE_ID, ViewChild } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subject, takeUntil, interval, Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Okta } from 'src/app/shared/okta/okta.service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';
declare var bootstrap: any;
import * as $ from "jquery";
import { HttpClient } from '@angular/common/http';
import { OrdersService } from 'src/app/services/orders.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  todaysTimeAndDate$!: Observable<string>;
  // formattedDate$: Observable<string>;

  @ViewChild('dropdown') dropdown!: ElementRef;
  isProfileDropdownOpen: boolean = false;

  UserID!: string;
  userIPAdress!: string;
  browserName!: string;

  FirstName!: string;
  SecurityLevel!: string;
  oktaEmail!: string;
  userRole:any;
  showWelcome: any;
  showLogoutButton: boolean = true;
  showNavItems: boolean = true;
  showIcons: boolean = true;
  showProfile: boolean = true;
  showHamburger: boolean = true;
  oktaSignIn: any;
  private destroy$ = new Subject<void>();

  // FLAG MENU VARIABLES
  isDropdownOpen: boolean = false;
  mySubscription;
  myDate = new Date();
  isMenuOpened: boolean = false;
  locale: any;
  flag: any;
  langauge: any;

  navList: any [] = [
    {
      "name": "HOME",
      "Url": "/welcome",
      "iconPath": "https://digital-team-805wb.certainteed.com/Productcustomization/assets/images/hamburger-home.png"
    },
    {
      "name": "CREATE NEW ORDER",
      "Url": "/createNewOrder",
      "iconPath": "https://digital-team-805wb.certainteed.com/Productcustomization/assets/images/hamburger-create.png"
    },
    {
      "name": "REPEAT ORDER",
      "Url": "/repeatOrder",
      "iconPath": "https://digital-team-805wb.certainteed.com/Productcustomization/assets/images/hamburger-repeat.png"
    },
    {
      "name": "USERS",
      "Url": "/User",
      "iconPath": "https://digital-team-805wb.certainteed.com/Productcustomization/assets/images/hamburger-users.png"
    },
    {
      "name": "REPORTS",
      "Url": "/reports",
      "iconPath": "https://digital-team-805wb.certainteed.com/Productcustomization/assets/images/hamburger-reports.png"
    },
   
  ]

  @HostListener('document:click', ['$event'])
  clickout(event: any) {
    var target = event.target;
    if(target.parentElement.classList[1]=='utility-drop__trigger') {
      if($(".dropdown--country").hasClass("open"))
      {
        $(".dropdown--country").removeClass("open");
      }
      else
      {
      $(".dropdown--country").addClass("open");
      }
    } else {
      $(".dropdown--country").removeClass("open");
    }

    // Check if the clicked element is part of the dropdown trigger
    if (target.classList.contains('utility-drop__trigger') || target.closest('.utility-drop__trigger')) {
      this.toggleDropdown(event);
    } else {
      this.closeDropdown();
    }

    // Check if the clicked element is part of the dropdown trigger
    // if (target.classList.contains('utility-drop__trigger')) {
    //   this.toggleDropdown();
    // } else {
    //   this.closeDropdown();
    // }
  }

  ngOnInit(): void {
    // console.log('my firstname is: ', this.FirstName);
    this.todaysTimeAndDate$ = interval(1000).pipe(
      startWith(0),
      map(() => {
        const currentDate = new Date();
        return this.datePipe.transform(currentDate, 'M/d/yyyy') ?? '';
      })
    );

    this.getIPInfo();
    this.userRole=sessionStorage.getItem("SecurityLevel")?.toString();
    //  alert(this.userRole);
      if(this.userRole=="End User" && this.userRole=="Vendor")
      {
      var index = this.navList.findIndex(el => el.name === 'CREATE NEW ORDER');
  if (index > -1) {
    this.navList.splice(index, 1);
  }
  index = this.navList.findIndex(el => el.name === 'REPEAT ORDER');
  if (index > -1) {
    this.navList.splice(index, 1);
  }
  index = this.navList.findIndex(el => el.name === 'USERS');
  if (index > -1) {
    this.navList.splice(index, 1);
  }
  
  }
    this.flag = sessionStorage.getItem('flag');
    this.langauge=sessionStorage.getItem('lang');  
    if(sessionStorage.getItem('flag')) {
      if(sessionStorage.getItem('flag')=='cn') {
        $("#flag").removeClass($("#flag")[0].classList[1]);
        $("#flag").addClass('flag-icon-cn');
      }
      else if(sessionStorage.getItem('flag')=='us') {
        $("#flag").removeClass($("#flag")[0].classList[1]);
        $("#flag").addClass('flag-icon-us');
      }
    }

    this.router.events
      .pipe(takeUntil(this.destroy$))
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          this.showIcons = this.shouldShowIcons();
          this.showLogoutButton = this.shouldShowLogoutButton();
          this.showNavItems = this.shouldShowNavItems();
          this.showWelcome = this.shouldShowWelcome();
          this.showProfile = this.shouldProfileIcons();
          this.showHamburger = this.shouldShowHamburger();
        }
      });

      // HAMBURGER MENU STARTS HERE
      const hamburger = document.querySelector(".hamburger")!;
      const navMenu = document.querySelector(".nav-menu")!;

      hamburger.addEventListener("click", () => {
        hamburger.classList.toggle("active");
        navMenu.classList.toggle("active");
      })

      document.querySelectorAll(".nav-link").forEach(n => {
        n.addEventListener("click", () => {
          hamburger.classList.remove("active");
          navMenu.classList.remove("active");
        });
      });
      
      document.querySelectorAll(".nav-menu li").forEach(item => {
        item.addEventListener("click", () => {
          hamburger.classList.remove("active");
          navMenu.classList.remove("active");
        });
      });
      // HAMBURGER MENU ENDS HERE

      // DROPDOWN CODE
      document.addEventListener("click", e => {
        const target = e.target as Element; // or HTMLElement
        const isDropdownButton = target.matches("[data-dropdown-button]");
        if (!isDropdownButton && target.closest("[data-dropdown]") != null) return;
      
        let currentDropdown: HTMLElement | null = null;
        if (isDropdownButton) {
          currentDropdown = target.closest("[data-dropdown]") as HTMLElement;
          if (currentDropdown) {
            currentDropdown.classList.toggle("active");
          }
        }
      
        document.querySelectorAll("[data-dropdown].active").forEach(dropdown => {
          if (dropdown === currentDropdown) return;
          dropdown.classList.remove("active");
        });
      });      

      
  }

  ngAfterViewInit(): void {
    const tooltips = document.querySelectorAll('.tt');
    tooltips.forEach(t => {
      new bootstrap.Tooltip(t);
    });
  }

  constructor(
    private router: Router, 
    private okta: Okta,
    private http: HttpClient,
    private ordersService: OrdersService,
    private translate: TranslateService,
    private eRef: ElementRef,
    private datePipe: DatePipe,
    @Inject(LOCALE_ID) locale: string
    ) {
      // const currentDate = new Date();
      // this.todaysTimeAndDate = this.datePipe.transform(currentDate, 'dd MMM, yyyy : h:mm a') ?? '';

      this.router.events.subscribe((event) => {
        if (event instanceof NavigationEnd) {
          // Update showHamburger property based on current route
          this.showHamburger = this.shouldShowHamburger();
        }
      });
 
      this.FirstName = sessionStorage.getItem("FirstName") ?? '';
      this.SecurityLevel = sessionStorage.getItem("SecurityLevel") ?? '';
      this.oktaEmail = sessionStorage.getItem("oktaEmail") ?? '';
      this.UserID = sessionStorage.getItem("UserID") ?? ''; 

      // Extract the first part before the dot
      if (this.oktaEmail) {
        const emailParts = this.oktaEmail.split('@');
        this.oktaEmail = emailParts[0].split('.')[0];
      }

      // debugger;
      if (sessionStorage.getItem('lang')) {
        console.log('getting old lang');
        console.log(sessionStorage.getItem('lang'));
        this.locale = sessionStorage.getItem('lang');
      } else {
        this.locale = navigator.language;
    
        if(this.locale=='en-US')
        {
          environment.language="en";
          this.flag="us";
          this.langauge=environment.language;
          sessionStorage.setItem('lang', "en");
        sessionStorage.setItem('flag', "us");
        }
        else  if(this.locale=='es-419')
        {
          environment.language="es";
          this.flag="us";
          this.langauge=environment.language;
          sessionStorage.setItem('lang', "es");
          sessionStorage.setItem('flag', "us");
        }
        else  if(this.locale=='es')
        {
          environment.language="es";
          this.flag="us";
          this.langauge=environment.language;
          sessionStorage.setItem('lang', "es");
          sessionStorage.setItem('flag', "us");
        }
        else  if(this.locale=='es-mx')
        {
          environment.language="es";
          this.flag="us";
          this.langauge=environment.language;
          sessionStorage.setItem('lang', "es");
          sessionStorage.setItem('flag', "us");
        }
        else  if(this.locale=='es-us')
        {
          environment.language="es";
          this.flag="us";
          this.langauge=environment.language;
          sessionStorage.setItem('lang', "es");
          sessionStorage.setItem('flag', "us");
        }
        else  if(this.locale=='fr-CA')
        {
        
          environment.language="fr";
          this.flag="cn";
          this.langauge=environment.language;
          sessionStorage.setItem('lang', "fr");
          sessionStorage.setItem('flag', "cn");
         // document.title = "Centre client | CertainTeed";
        }
        else  if(this.locale=='fr-fr')
        {
        
          environment.language="fr";
          this.flag="cn";
          this.langauge=environment.language;
          sessionStorage.setItem('lang', "fr");
          sessionStorage.setItem('flag', "cn");
         // document.title = "Centre client | CertainTeed";
        }
        else  if(this.locale=='en-CA')
        {
          environment.language="en";
          this.flag="cn";
          this.langauge=environment.language;
          sessionStorage.setItem('lang', "en");
          sessionStorage.setItem('flag', "cn");
        }
        else
        {
          environment.language="en";
          this.flag="us";
          this.langauge=environment.language;
          sessionStorage.setItem('lang', "en");
          sessionStorage.setItem('flag', "us");
        }
        // alert(this.flag);
      }
      this.translate.use(sessionStorage.getItem("lang") ?? '');
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.mySubscription = this.router.events.subscribe((event) => {
          if (event instanceof NavigationEnd) {
      // Trick the Router into believing it's last link wasn't previously loaded
          this.router.navigated = false;
          }
        }); 

    }

    toggleProfileDropdown(event: MouseEvent) {
      event.preventDefault();
      this.isProfileDropdownOpen = !this.isProfileDropdownOpen;
      if (this.isProfileDropdownOpen) {
        document.addEventListener('click', this.closeDropdownOutside);
      } else {
        document.removeEventListener('click', this.closeDropdownOutside);
      }
    }
  
    closeDropdownOutside = (event: MouseEvent) => {
      if (!this.dropdown.nativeElement.contains(event.target)) {
        this.isProfileDropdownOpen = false;
        document.removeEventListener('click', this.closeDropdownOutside);
      }
    }

    closeDropdownInsideProfile() {
      this.isProfileDropdownOpen = false;
    }
    

    toggleDropdown(event: Event) {
      event.stopPropagation(); // Prevent the click event from reaching the document click handler
      this.isDropdownOpen = !this.isDropdownOpen;
    }

    closeDropdown() {
      this.isDropdownOpen = false;
    }

    useLanguage(language: string,flag:string) {
      sessionStorage.setItem('lang', language);
      sessionStorage.setItem('flag', flag);
      this.flag=flag;
      this.langauge=language;
      if(flag=='cn')
      {
        $("#flag").removeClass($("#flag")[0].classList[1]);
        $("#flag").addClass('flag-icon-cn');
      }
     else if(flag=='us')
      {
        $("#flag").removeClass($("#flag")[0].classList[1]);
        $("#flag").addClass('flag-icon-us');
      }
    
    
      this.translate.use(language);
      document.querySelector('html')?.setAttribute('lang', language);
      environment.language=language;
         if(language=='fr')
      {
         document.title = "Centre client | CertainTeed";
      }
      window.location.reload();
     // const currentUrl = this.router.url;
     // this.router.navigate([currentUrl])
      debugger;
    }

    open(i: any) {
      const triggerElement = $(".utility-drop__trigger")[i];
      if (triggerElement) {
        const parentElement = triggerElement.parentElement as HTMLElement;
        if (parentElement) {
          parentElement.classList.toggle("open");
        }
      }
    }

    ngOnDestroy() {
      if (this.mySubscription) {
       this.mySubscription.unsubscribe();
      }
    }

    showNavItem(navItem: any): boolean {
      const userSecurityLevel = this.SecurityLevel;
      // console.log(userSecurityLevel);
      
      if (userSecurityLevel === '1' || userSecurityLevel === '2' || userSecurityLevel === '5') {
          // Show all navigation items to users with security level 1, 2, and 5
          // return navItem.name === "HOME" && navItem.name === "CREATE NEW ORDER" && navItem.name === "REPEAT ORDER" && navItem.name === "USERS" && navItem.name === "REPORTS";
          // return navItem.name;
          return true;
      } else if (userSecurityLevel === '3' || userSecurityLevel === '4') {
          // Only show the "HOME" navigation item to users with security level 3 and 4
          return navItem.name === "HOME" || navItem.name === "REPORTS" ;
      } else {
          // Hide all navigation items for other security levels
          return false;
      }
    }  
    
  shouldShowWelcome(): any {
    const currentRoute = this.router.url;
    const routesWithoutWelcome = ['landing', 'login'];

    return !routesWithoutWelcome.some(route => currentRoute.endsWith(route));
  }  

  shouldShowLogoutButton(): boolean {
    const currentRoute = this.router.url;
    const routesWithoutLogoutButton = ['landing', '/login'];
  
    return !routesWithoutLogoutButton.some(route => currentRoute.endsWith(route));
  }

  shouldShowNavItems(): boolean {
    const currentRoute = this.router.url;
    const routesWithoutNavItems = [ 'landing', 'login' ];

    return !routesWithoutNavItems.some(route => currentRoute.endsWith(route));
  }

  shouldShowIcons(): boolean {
    const currentRoute = this.router.url;
    const restrictedRoutes = [ '/login', '/landing' ];
  
    return !restrictedRoutes.some(route => currentRoute.endsWith(route));
  }

  shouldProfileIcons(): boolean {
    const currentRoute = this.router.url;
    const restrictedRoutes = [ '/login', '/landing' ];
  
    return !restrictedRoutes.some(route => currentRoute.endsWith(route));
  }

  shouldShowHamburger(): boolean {
    const currentRoute = this.router.url;
    const restrictedRoutes = [ '/login', '/landing', '/authorize' ];
  
    return !restrictedRoutes.some(route => currentRoute.endsWith(route));
  }

  toggleMenu() {
    this.showHamburger = !this.showHamburger;
  }

  @HostListener('document:click', ['$event'])
  onDocumentClick(event: MouseEvent) {
    // Close menu when clicking outside the menu
    const target = event.target as HTMLElement;
    if (!target.closest('.navbar_menu')) {
      this.showHamburger = false;
    }
  }

  logout(): void {
    this.oktaSignIn = this.okta.getWidget();

    this.oktaSignIn.authClient.signOut({
      clearTokensBeforeRedirect: true,
      //  postLogoutRedirectUri: 'http://localhost:4200'
      postLogoutRedirectUri: 'https://digital-team-805wb.certainteed.com/Productcustomization'
    })

    sessionStorage.removeItem('SGID');
    sessionStorage.clear();
    localStorage.clear();

    this.browserName = (navigator as any).userAgentData.brands[0].brand;
    debugger;
    const logData = {
      UserID: this.UserID,
      IPAddress: this.userIPAdress,
      OrderID: '',
      ActionType: 'Logout',
      ActionDescription: `${this.FirstName} has logged out successfully'`,
      BrowserInfo: this.browserName, 
      Comments: '---', // User comments for new order
      Status: '' // You may add status if needed
    };
    
    this.ordersService.insertLog(logData).subscribe(
      (response) => {
        console.log('Log inserted successfully:', response);
      },
      (error) => {
        console.error('Error inserting log:', error);
      }
    );
  }

  getIPInfo() {
    this.http.get<any>('https://ipinfo.io').subscribe(
      (response) => {
        // console.log(response);
        // console.log(response.ip);
        this.userIPAdress = response.ip;
      },
      (error) => {
        console.log('Error fetching IP info', error);
      }
    );
  }


}
