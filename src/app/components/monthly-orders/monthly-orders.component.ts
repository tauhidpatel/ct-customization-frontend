import { Component } from '@angular/core';
import { OrdersService } from 'src/app/services/orders.service';
import { Chart, registerables } from 'chart.js';
import { Location } from '@angular/common';

Chart.register(...registerables);

@Component({
  selector: 'app-monthly-orders',
  templateUrl: './monthly-orders.component.html',
  styleUrls: ['./monthly-orders.component.css']
})
export class MonthlyOrdersComponent {

  monthlyOrderData: { Month: number, OrderCount: number }[] = [];

  UserID!: any;

  constructor( private ordersService: OrdersService, private _location: Location ) { 
    this.UserID = sessionStorage.getItem("UserID") ?? '';
  }

  goBack() {
    this._location.back();
  }

  ngOnInit(): void {
    this.getMonthlyOrderDetails();
  }

  getMonthlyOrderDetails() {
    this.ordersService.getMonthlyOrderDetails(this.UserID).subscribe(
      (response) => {
        console.log(response);
        this.monthlyOrderData = response;
        this.renderChart();
      },
      (error) => {
        console.log(error);
      }
    )
  }

  renderChart() {
    // Destroy existing chart if it exists
    const months = this.monthlyOrderData.map(item => item.Month);
    const orderCounts = this.monthlyOrderData.map(item => item.OrderCount);

    const myChart = new Chart('monthlyChart', {
      type: 'bar',
      data: {
        labels: months.map(month => this.getMonthName(month)), // Convert month number to month name
        datasets: [{
          label: 'Orders By Month',
          data: orderCounts,
          borderWidth: 1,
          // backgroundColor: '#005EB8',
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });
  }

  getMonthName(monthNumber: number): string {
    const monthNames = [
      "January", "February", "March", "April", "May", "June", 
      "July", "August", "September", "October", "November", "December"
    ];

    return monthNames[monthNumber - 1];
  }


}
