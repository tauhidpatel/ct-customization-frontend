import { DatePipe, Location } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { MailService } from 'src/app/services/mail.service';
import { OrdersService } from 'src/app/services/orders.service';

@Component({
  selector: 'app-repeat-order',
  templateUrl: './repeat-order.component.html',
  styleUrls: ['./repeat-order.component.css']
})
export class RepeatOrderComponent {

  selectedOrder: any;
  dropdownOrderDetails: any[] = [];
  addedProdDetails: any[] = [];
  addingIntoProductDetails: any;

  userIPAdress!: string;
  browserName!: string;
  EmailAddress: any;

  errorMessage: string = '';

  orderProductDetails: any[] = [];

  selectedDropdownOrderID!: any;
  selectedDropdownOrderNumer!: any;

  repeatOrderID: any;
  
  orderID!: string; // order ID from the URL 
  UserID!: any; // getting this from session storage
  customerEmails: any[] = [];
  allSites: any[] = [];
  allOrdersInfo: any[] = [];

  showSuccessAlert: boolean = false;
  
  orderDetails: any[] = [];
  selectedSite: any;
  orderStatus: any;

  selectedCustomer: any[] = [];

  order: any = {
    Siteid: '',
    CTOrderNumber: '',
    CTPONumber: '',
    EnteredBy: '',
    ArtworkID: '',
    ProofID: '',
    OrderType: '',
    OrderBy: '',
    CTShipToInformation_Name: '',
    CTShipToInformation_CompanyName: '',
    CTShipToInformation_AccountNumber: '',
    CTShipToInformation_Address: '',
    CTShipToInformation_City: '',
    CTShipToInformation_State: '',
    CTShipToInformation_ZipCode: '',
    CTShipToInformation_Country: '',
    CTShipToInformation_Phone: '',

    CTShipToInformation_EmailAddress: '',
    ApprovedByClient: '',
    orderID: '',
    RegionNumber: '',
    RepeatOrderComment: '',
    TerritoryNumber: ''
  };

  productDetails: any[] = [
    {
      proxyID: '',
      OrderID: '',
      ProductCode: '',
      ProductDescription: '',
      ProductQty: '',
      ReferenceNo: ''
    },
    {
      proxyID: '',
      OrderID: '',
      ProductCode: '',
      ProductDescription: '',
      ProductQty: '',
      ReferenceNo: ''
    },
    {
      proxyID: '',
      OrderID: '',
      ProductCode: '',
      ProductDescription: '',
      ProductQty: '',
      ReferenceNo: ''
    },
  ];

  // Initialize newProductDetails with empty objects
  newProductDetails: any[] = [
    {
      OrderID: '',
      ProductCode: '',
      ProductDescription: '',
      ProductQty: '',
      ReferenceNo: ''
    },
    {
      OrderID: '',
      ProductCode: '',
      ProductDescription: '',
      ProductQty: '',
      ReferenceNo: ''
    },
    {
      OrderID: '',
      ProductCode: '',
      ProductDescription: '',
      ProductQty: '',
      ReferenceNo: ''
    },
    {
      OrderID: '',
      ProductCode: '',
      ProductDescription: '',
      ProductQty: '',
      ReferenceNo: ''
    }
  ];

  customers : any = {
    userid: '',
    orderID: '',
    orderByAccountNumber: '',
    shipToAccountNumber: '',
    fullName: '',
    emailAddress: '',
    dateAdded: '',
    dateLastUsed: '',
    enteredBy: '',
    isPrimary: ''
  }

  constructor(
    private ordersService: OrdersService,
    private MailService :MailService, 
    private router: Router, 
    private route: ActivatedRoute,
    private datePipe: DatePipe,
    private http: HttpClient,
    private _location: Location
  ) { 
    this.UserID = sessionStorage.getItem("UserID") ?? ''; // getting the UserID from session storage
    this.EmailAddress = sessionStorage.getItem("EmailAddress") ?? '';
  }

  goBack() {
    this._location.back();
  }

  ngOnInit(): void {
    debugger;
    this.orderID = this.route.snapshot.params['OrderID'];
    this.getIPInfo();

    if (this.orderID) {
      
      this.getOrderDetails(this.orderID);
      this.getOrderNumberFromUserID();
      this.getSelectedOrderSite();
      this.getCurrentOrderStatus();
      this.getOrderDetailsByOrderID()
      // this.getFullNameAndEmailAddressByOrderID();
      
      this.loadAllOrderSatus();
      this.loadallSites();
   //   this.loadCustomerEmails();
    } 
    else {
      this.getOrderNumberFromUserID();
     //  this.getSelectedOrderSite();
      this.loadAllOrderSatus();
      this.loadallSites();
    //  this.loadCustomerEmails();

    }
    
  }


  loadAllOrderSatus() {
    this.ordersService.getAllOrderStatus().subscribe(
      (orderResponse: any) => {
        // console.log(orderResponse[0].StatusId);
        this.order.StatusId = orderResponse[0].StatusId;
      },
      (error) => {
        console.error('Error loading order status', error);
      }
    )
  }

  loadallSites() {
    this.ordersService.getAllSites(this.UserID).subscribe(
      (response: any[]) => { 
        this.allSites = response;
      },
      (error) => {
        console.error('Error fetching all sites', error);
      }
    )
  }

  // loading customer emails in the dropdown
  loadCustomerEmails(SiteId:any) {
    debugger;
    this.ordersService.getCustomerEmails(SiteId).subscribe(
      (data: any[]) => {
        this.customerEmails = data;
      },
      (error) => {
        console.error('Error fetching customer emails', error);
      }
    );
  }

  createNewOrderDetails() {
    debugger;

    this.ordersService.getOrderDetails(this.selectedDropdownOrderID).subscribe(
      (orderDetails: any) => {
        debugger;

        const orderID = orderDetails[0].OrderID;
        
        this.newProductDetails.forEach((product: any) => {
          product.OrderID = orderID;
        });
        
        // Send the request to create product details
        this.createProductDetails(orderID);  

      },
      (orderError) => {
        console.error('Error creating order:', orderError);
      }
    );
  }

  createProductDetails(orderID: any) {
    // Combine existing and new product details
    const allProductDetails = [...this.productDetails, ...this.newProductDetails];
  
    // Iterate over the combined product details array
    allProductDetails.forEach((product: any) => {
      // Check if all required fields have values
      if (
        product.ProductCode.trim() !== '' &&
        product.ProductDescription.trim() !== '' &&
        product.ProductQty.trim() !== ''
      ) {
        const productToSend = {
          OrderID: orderID,
          ProductCode: product.ProductCode,
          ProductDescription: product.ProductDescription,
          ProductQty: product.ProductQty,
          ReferenceNo: product.ReferenceNo
        };
  
        // Call the service to create product details
        this.ordersService.createProductDetails(productToSend).subscribe(
          (ProductPesponse) => {
            console.log('Product detail added successfully:', ProductPesponse);
          },
          (error) => {
            console.error('Error creating product detail:', error);
          }
        );
      }
    });
  }
  

  // createProductDetails(orderID: any) {
  //   // Combine existing and new product details
  //   const allProductDetails = [...this.productDetails, ...this.newProductDetails];


  //   this.newProductDetails.forEach((product: any) => { // Iterate over productDetails array
      
  //     if (
  //       product.ProductCode.trim() !== '' &&
  //       product.ProductDescription.trim() !== '' &&
  //       product.ProductQty.trim() !== ''
  //     ) {
  //       const productToSend = {
  //         OrderID: this.orderID,
  //         ProductCode: product.ProductCode,
  //         ProductDescription: product.ProductDescription,
  //         ProductQty: product.ProductQty,
  //         ReferenceNo: product.ReferenceNo
  //       };
    
  //       this.ordersService.createProductDetails(productToSend).subscribe(
  //         (ProductPesponse) => {
  //           console.log('Product detail added successfully:', ProductPesponse);
  //         },
  //         (error) => {
  //           console.error('Error creating product detail:', error);
  //         }
  //       );
  //     }
  //   });
  // }

  formatDate(date: string): string {
    return this.datePipe.transform(date, 'yyyy-MM-dd') ?? '';
  }

  getOrderNumberFromUserID() {
    this.ordersService.getOrderNumberFromUserID(this.UserID).subscribe(
      (res) => {
        // console.log('Orders are: ', res);
        this.allOrdersInfo = res;
      },
      (error) => {
        console.log(error)
      }
    )
  }

  onOrderSelected() {
    debugger;
    if (!this.orderID) {
      // Handle selection from dropdown
      const selectedOrder = this.allOrdersInfo.find(order => order.CTOrderNumber === this.selectedOrder);
      // console.log('>>>>> ', selectedOrder.OrderID);
      this.selectedDropdownOrderID = selectedOrder.OrderID;
      this.selectedDropdownOrderNumer = selectedOrder.CTOrderNumber;
      if (selectedOrder) {
          // Call methods that require OrderID here or trigger an event to inform other methods about the selected OrderID
          // For example:
          // this.retrieveProductDetails();
          // this.getCurrentOrderStatus();
          // etc.
      }
  } else {
      // Handle selection based on OrderID
  }
    const dropdownOrder = this.selectedOrder;

    this.ordersService.getOrdersInfoByOrderNumber(dropdownOrder).subscribe(
      (res) => {
        // console.log('theseeeeeeeee: ', res);

        this.dropdownOrderDetails = res;
        const selectedOrderDetails = this.dropdownOrderDetails.find(order => order.CTOrderNumber === this.selectedOrder);
        
        // console.log(selectedOrderDetails);
        
        if (selectedOrderDetails) {
          this.addingIntoProductDetails = selectedOrderDetails.OrderID;
          // this.order.CTOrderNumber = selectedOrderDetails.CTOrderNumber;
          this.order.CTPONumber = selectedOrderDetails.CTPONumber;
          this.order.CTOrderDate = selectedOrderDetails.CTOrderDate; 
          this.order.OrderType = selectedOrderDetails.OrderType; 
          this.order.OrderBy = selectedOrderDetails.OrderBy; 
          this.order.shipToAccountNumber = selectedOrderDetails.shipToAccountNumber; 
          this.order.CTShipToInformation_CompanyName = selectedOrderDetails.CTShipToInformation_CompanyName; 
          this.order.CTShipToInformation_AccountNumber = selectedOrderDetails.CTShipToInformation_AccountNumber; 
          this.order.CTShipToInformation_Address = selectedOrderDetails.CTShipToInformation_Address; 
          this.order.CTShipToInformation_City = selectedOrderDetails.CTShipToInformation_City; 
          this.order.CTShipToInformation_State = selectedOrderDetails.CTShipToInformation_State; 
          this.order.CTShipToInformation_ZipCode = selectedOrderDetails.CTShipToInformation_ZipCode; 
          this.order.CTShipToInformation_Country = selectedOrderDetails.CTShipToInformation_Country; 
          this.order.CTShipToInformation_Phone = selectedOrderDetails.CTShipToInformation_Phone; 
          this.order.Siteid=selectedOrderDetails.Siteid;
          this.order.ActualShipDate=selectedOrderDetails.ActualShipDate;
          this.order.AnticipatedShipDate=selectedOrderDetails.AnticipatedShipDate;
          if(selectedOrderDetails.ArtworkID==null)
            {
              this.order.ArtworkID="";
            }
            else
            {
              this.order.ArtworkID=selectedOrderDetails.ArtworkID;
            }
          
          this.order.CTShipToInformation_Name=selectedOrderDetails.CTShipToInformation_Name;
          this.order.EnteredBy=selectedOrderDetails.EnteredBy;
       
        
          if(selectedOrderDetails.ProofID==null)
            {
              this.order.ProofID="";
            }
            else
            {
              this.order.ProofID=selectedOrderDetails.ProofID;
            }
          this.order.StatusId="2";
this.order.OrderType="Repeat order";
this.order.RepeatOrderComment="This is a repeat of order #"+selectedOrderDetails.CTOrderNumber;

          // Now that addingIntoProductDetails is set, retrieve product details
          this.retrieveProductDetails();
          this.loadCustomerEmails(this.order.Siteid)
        }
      },
      (error) => {
        console.log('error fetching details for that order number', error);
      }
    );
  }

  retrieveProductDetails() {
    if(this.addingIntoProductDetails) {

      this.ordersService.getGetOrderDetailsByOrderID(this.addingIntoProductDetails).subscribe(
        (res: any[]) => {
          res.sort((a, b) => a.ProductCode - b.ProductCode); // Sort products by ProductCode
          // console.log(this.orderProductDetails);
          // console.log('Order Product Details: ', res);
          this.orderProductDetails = res;
        },
        (error) => {
            console.error('Error fetching order details:', error);
        }
      );

      this.ordersService.getGetOrderDetailsByOrderID(this.addingIntoProductDetails).subscribe(
        (prodRes) => {
          // console.log('Product Details: ', prodRes);
          this.addedProdDetails = prodRes;
          
        }, 
        (error) => { console.log(error) }
      );
      
    }
  }

  getOrderDetailsByOrderID() {
    const orderId = this.route.snapshot.params['OrderID'];

    if(orderId) {
      this.ordersService.getGetOrderDetailsByOrderID(orderId).subscribe(
        (res: any[]) => {
          res.sort((a, b) => a.ProductCode - b.ProductCode); // Sort products by ProductCode
          this.orderProductDetails = res;
          console.log(this.orderProductDetails);
        },
        (error) => {
            console.error('Error fetching order details:', error);
        }
      );
    } 

  }

  getOrderDetails(orderID:any) {
    const orderId = this.route.snapshot.params['OrderID'];
    // this.order.CTOrderNumber = this.selectedDropdownOrderNumer;
    this.ordersService.getOrderDetails(orderID).subscribe(
      (orderDetails: any) => {
        console.log(orderDetails);
        this.order.orderID = orderDetails[0].OrderID;
        this.order.userid = orderDetails[0].Userid;
        this.order.Siteid = orderDetails[0].Siteid;
      //  this.order.CTOrderNumber = orderDetails[0].CTOrderNumber
        this.order.CTPONumber = orderDetails[0].CTPONumber
        this.order.EnteredBy = orderDetails[0].EnteredBy
        // this.order.ArtworkID = orderDetails[0].ArtworkID
        // this.order.ProofID = orderDetails[0].ProofID
        this.order.OrderType = orderDetails[0].OrderType
        this.order.OrderBy = orderDetails[0].OrderBy
        this.order.CTShipToInformation_Name = orderDetails[0].CTShipToInformation_Name
        this.order.CTShipToInformation_CompanyName = orderDetails[0].CTShipToInformation_CompanyName
        this.order.CTShipToInformation_AccountNumber = orderDetails[0].CTShipToInformation_AccountNumber
        // this.order.CTShipToInformation_Address = orderDetails[0].CTShipToInformation_Address
        this.order.CTShipToInformation_City = orderDetails[0].CTShipToInformation_City
        this.order.CTShipToInformation_State = orderDetails[0].CTShipToInformation_State
        this.order.CTShipToInformation_ZipCode =  orderDetails[0].CTShipToInformation_ZipCode
        this.order.CTShipToInformation_Country = orderDetails[0].CTShipToInformation_Country
        this.order.CTShipToInformation_Phone = orderDetails[0].CTShipToInformation_Phone
        this.order.CTShipToInformation_EmailAddress = orderDetails[0].CTShipToInformation_EmailAddress
        // this.order.ApprovedByClient = orderDetails[0].ApprovedByClient
        // this.order.RegionNumber = orderDetails[0].RegionNumber
        
        // this.order.TerritoryNumber = orderDetails[0].TerritoryNumber
        this.order.CTShipToInformation_Name=orderDetails[0].CTShipToInformation_Name;
        this.order.EnteredBy=orderDetails[0].EnteredBy;
        if(orderDetails[0].ArtworkID==null)
          {
            this.order.ArtworkID="";
          }
          else
          {
            this.order.ArtworkID=orderDetails[0].ArtworkID;
          }
          if(orderDetails[0].ProofID==null)
            {
              this.order.ProofID="";
            }
            else
            {
              this.order.ProofID=orderDetails[0].ProofID;
            }
        this.order.StatusId="2";
this.order.OrderType="Repeat order";
this.order.RepeatOrderComment="This is a repeat of order #"+orderDetails[0].CTOrderNumber;
        this.loadCustomerEmails(this.order.Siteid);
      },
      (error) => {
        console.log('error fetching order details', error);
      }
    )
  }

  getSelectedOrderSite() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getSelectedOrderSite(orderId).subscribe(
      (response) => {
        // console.log(response);
        this.selectedSite = response;
        this.order.Siteid=this.selectedSite;
    //    this.loadCustomerEmails(this.order.Siteid);
      },
      (error) => {
        console.log('error fetching order details', error);
      }
    )
  }

  getCurrentOrderStatus() {
    const orderId = this.route.snapshot.params['OrderID'];
    this.ordersService.getCurrentOrderStatus(orderId).subscribe(
      (response) => {
        // console.log(response);
        this.orderStatus = response;
      },
      (error) => {
        console.log('erorrrrr', error);
      }
    )
  }
  


  // new dynamic method
  updateOrderDetails() {
    debugger;
    const productDetails = this.orderProductDetails;
    console.log('Product details for update:', productDetails);
    this.ordersService.updateProductDetails(productDetails).subscribe(
      (data: any) => {
        console.log('Order details updated successfully', data);
      },
      (error) => {
        console.log('Unable to update order details', error);
      }
    );
  }

  updateSelectedCustomer() {
    const selectedUserId = this.order.CTShipToInformation_EmailAddress;

    const index = selectedUserId - 1; // Assuming UserID starts from 1
    // Check if the index is valid
    if (index < 0 || index >= this.customerEmails.length) {
      console.error('Invalid index or selected user ID.');
      return;
    }

    // Access the email address using the index
    const selectedEmailAddress = this.customerEmails[index].EmailAddress;
    
    this.customers.userid = selectedUserId;
    this.customers.emailAddress = selectedEmailAddress;
  }

  loadCustomerEmailsAndAssignCustomers(orderID: any,Siteid:any) {
    this.ordersService.getCustomerEmails(Siteid).subscribe(
      (data: any[]) => {
        this.customerEmails = data;

        // selected email address
        const selectedEmailAddress = this.order.CTShipToInformation_EmailAddress;
        console.log('selected email address: ', selectedEmailAddress);

        const selectedCustomer = this.customerEmails.find(email => email.EmailAddress === selectedEmailAddress);
        console.log('sss: ', selectedCustomer);
        
        if(selectedCustomer) {

          const customers = {
            fullName: selectedCustomer.FirstName + " " + selectedCustomer.LastName,
            enteredBy: selectedCustomer.FirstName,
            userid: selectedCustomer.UserID,
            emailAddress: encodeURIComponent(selectedCustomer.EmailAddress),
            orderID: orderID,
            DateAdded: '',
            DateLastUsed: '',
            IsPrimary: '',
            OrderByAccountNumber: '',
            ShipToAccountNumber: ''
          };
          this.assignCustomers(customers);
          this.RepeatOrderMail(customers.fullName, orderID, Siteid, customers.emailAddress);
        }
        
      },
      (error) => {
        console.error('Error fetching customer emails', error);
      }
    );
  }

  assignCustomers(customers: any) {
    this.ordersService.assignCustomers(customers).subscribe(
      (customerResponse) => {
        console.log("Customers has been assigned !!!", customerResponse);
      },
      (customerError) => {
        console.log("Customers NOT ASSIGNED !!!", customerError);
      }
    );
  }

  checkCTOrderNumber() {
    if (this.order.CTOrderNumber) {
      this.ordersService.checkCTOrderNumberAvailability(this.order.CTOrderNumber).subscribe(
        (response) => { 
          console.log(response.message);
          this.errorMessage = response.message;
          // If CTOrderNumber is available, reset the error message
          if (response.message === 'This Order Number is available.') {
            this.errorMessage = '';
          }
        },
        (error) => {
          console.error(error); 
        }
      );
    } else {
      this.errorMessage = '';
    }
  }

  repeatOrder() {
    debugger;
    this.order.userid = this.UserID;
   // this.order.RepeatOrderComment = this.selectedDropdownOrderNumer;
    // this.order.CTOrderNumber = this.selectedDropdownOrderNumer;

    this.ordersService.createNewOrder(this.order).subscribe(
      (orderResponse) => {
        debugger;
        const orderID = orderResponse[0].orderID;

        this.updateOrderDetails();
        this.createNewOrderDetails();

        // Concatenate existing and new product details
        const allProductDetails = [...this.orderProductDetails, ...this.newProductDetails];

        // Assign the OrderID to each product detail
        allProductDetails.forEach((product: any) => {
          product.OrderID = orderID;
        });

        // Iterate over the combined product details array
        allProductDetails.forEach((product: any) => {
          // Check if all required fields have values
          debugger;
          if (
            product.ProductCode.trim() !== '' &&
            product.ProductDescription.trim() !== '' &&
            product.ProductQty.trim() !== ''
          ) {
            const productToSend = {
              OrderID: orderID,
              ProductCode: product.ProductCode,
              ProductDescription: product.ProductDescription,
              ProductQty: product.ProductQty,
              ReferenceNo: product.ReferenceNo
            };
      
            // Call the service to create product details
            this.ordersService.createProductDetails(productToSend).subscribe(
              (ProductPesponse) => {
                console.log('Product detail added successfully:', ProductPesponse);
              },
              (error) => {
                console.error('Error creating product detail:', error);
              }
            );
          }
        });
        
        // Send the request to create product details
        // this.createProductDetails(orderID);  

        // Load customer emails and assign customers after creating the order
        this.loadCustomerEmailsAndAssignCustomers(orderID,this.order.Siteid);
    
        this.browserName = (navigator as any).userAgentData.brands[0].brand
        const logData = {
          UserID: this.UserID,
          IPAddress: this.userIPAdress, 
          OrderID: orderID,
          ActionType: 'Order Repeated',
          ActionDescription: `order was repeated by ${this.EmailAddress}`,
          BrowserInfo: this.browserName, 
          Comments: '---', 
          Status: '' 
        };
      
        this.ordersService.insertLog(logData).subscribe(
          (response) => {
            console.log('Log inserted successfully:', response);
          },
          (error) => {
            console.error('Error inserting log:', error);
          }
        );

         // Redirect to '/allOrders' upon successful order creation
         const navigationExtras: NavigationExtras = {
          queryParams: { repeatOrderSuccess: 'true' },
          fragment: 'top' // Scroll to top when navigating
        };
        this.router.navigate(['/allOrders'], navigationExtras);

      }, 
      (error) => {
        console.log(error);
    });
   
  }
  RepeatOrderMail(FullName:any,OrderNo: any,Siteid:any,ToEmail:any)
  {
    debugger;
    const Template="RepeatOrder";
    // const Servername="http:%2F%2Flocalhost:4200%2F";
    // const Servername="https%3A%2F%2Fdigital-team-805wb.certainteed.com";
    // const ExtraUrl="Productcustomization";
    const Servername = encodeURIComponent("https://digital-team-805wb.certainteed.com");
    const ExtraUrl = encodeURIComponent("Productcustomization");
    // const ExtraUrl="null";
   
  
    this.MailService.RepeatOrder(Template,Servername,ExtraUrl,FullName, OrderNo, Siteid,ToEmail).subscribe(
      (orderResponse) => {
        console.log('Order created successfully:', orderResponse);
      });
  }
  getIPInfo() {
    this.http.get<any>('https://ipinfo.io').subscribe(
      (response) => {
        // console.log(response);
        // console.log(response.ip);
        this.userIPAdress = response.ip;
      },
      (error) => {
        console.log('Error fetching IP info', error);
      }
    );
  }

}