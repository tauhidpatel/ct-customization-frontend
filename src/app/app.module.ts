import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './components/login/login.component';
import { EmailverifyComponent } from './components/emailverify/emailverify.component';
import { VerifyotpComponent } from './components/verifyotp/verifyotp.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { LandingComponent } from './components/landing/landing.component';
import { SsoComponent } from './components/sso/sso.component';
import { AuthorizeComponent } from './components/authorize/authorize.component';
import { UserprofileComponent } from './components/userprofile/userprofile.component';
import { CreateNewOrderComponent } from './components/create-new-order/create-new-order.component';
import { RepeatOrderComponent } from './components/repeat-order/repeat-order.component';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { RoleAccessComponent } from './components/role-access/role-access.component';
import { ViewOrderComponent } from './components/view-order/view-order.component';
import { ReportsComponent } from './components/reports/reports.component';
import { AllOrdersComponent } from './components/all-orders/all-orders.component';
import { UploadProofComponent } from './components/upload-proof/upload-proof.component';
import { ReviewProofComponent } from './components/review-proof/review-proof.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { UploadArtworkComponent } from './components/upload-artwork/upload-artwork.component';
import { ResetOrderStatusComponent } from './components/reset-order-status/reset-order-status.component';
import { UsersComponent } from './components/users/users.component';
import { EditOrderComponent } from './components/edit-order/edit-order.component';
import { ProcessedOrdersComponent } from './components/processed-orders/processed-orders.component';
import { UpdateOrderStatusComponent } from './components/update-order-status/update-order-status.component';
import { MonthlyOrdersComponent } from './components/monthly-orders/monthly-orders.component';
import { AnnuallyOrdersComponent } from './components/annually-orders/annually-orders.component';
import { OrdersInLast60DaysComponent } from './components/orders-in-last60-days/orders-in-last60-days.component';
import { UserAccountActivityComponent } from './components/user-account-activity/user-account-activity.component';
import { UserOrdersActivityComponent } from './components/user-orders-activity/user-orders-activity.component';
import { ProductLogsComponent } from './components/product-logs/product-logs.component';
import { ReviewArtworkComponent } from './components/review-artwork/review-artwork.component';
import { PrintOrderComponent } from './components/print-order/print-order.component';
import { ChangepasswordComponent } from './components/changepassword/changepassword.component';
import { LoginoktaComponent } from './components/loginokta/loginokta.component';
import { ForgotpasswordComponent } from './components/forgotpassword/forgotpassword.component';
import { ResetpasswordComponent } from './components/resetpassword/resetpassword.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    EmailverifyComponent,
    VerifyotpComponent,
    DashboardComponent,
    WelcomeComponent,
    LandingComponent,
    SsoComponent,
    AuthorizeComponent,
    UserprofileComponent,
    CreateNewOrderComponent,
    RepeatOrderComponent,
    CreateUserComponent,
    RoleAccessComponent,
    ViewOrderComponent,
    ReportsComponent,
    AllOrdersComponent,
    UploadProofComponent,
    ReviewProofComponent,
    UploadArtworkComponent,
    ResetOrderStatusComponent,
    UsersComponent,
    EditOrderComponent,
    ProcessedOrdersComponent,
    UpdateOrderStatusComponent,
    MonthlyOrdersComponent,
    AnnuallyOrdersComponent,
    OrdersInLast60DaysComponent,
    UserAccountActivityComponent,
    UserOrdersActivityComponent,
    ProductLogsComponent,
    ReviewArtworkComponent,
    PrintOrderComponent,
    ChangepasswordComponent,
    LoginoktaComponent,
    ForgotpasswordComponent,
    ResetpasswordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => {
          return new TranslateHttpLoader(http, './assets/i18n/', '.json');
        },
        deps: [HttpClient]  
      }
    })
  ],
  providers: [
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
